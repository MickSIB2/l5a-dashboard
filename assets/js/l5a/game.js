require ('../../css/l5a/game.css');
require('./custom');

$(document).on('click', '#envoyer-mail', function() {
    envoyer_mail($(this));
});

$(document).on('click', '#partie-mdp', function() {
    entrer_mdp_partie($(this));
});

function envoyer_mail(element) {
    var input = $('#nouveau-joueur-mail');
    var email = input.val();
    var game_id = element.attr('data-game-id');
    var gm = element.attr('data-gm');
    if (email !== '') {
        if (! isEmail(email)) {
            alert (Translator.trans('not_a_valid_mail', {'email': email}));
        } else {
            $.ajax({
                method: 'post',
                url: chemins.envoyer_mail,
                data: {
                    'mail': email,
                    'game_id': game_id,
                    'gm': gm
                },
                success: function() {
                    console.log('mail sent');
                    input.val('');
                },
                error: function () {
                    toastr["error"](Translator.trans('toast.send_mail.failure'));
                }
            })
        }
    }
}

function entrer_mdp_partie(element) {
    var input = $('#partie-mdp');
    var mdp = input.val();
    var game_id = element.attr('data-game-id');
    if (mdp !== '') {
        $.ajax({
            method: 'post',
            url: chemins.enregistrer_mdp_partie,
            data: {
                'mdp': mdp,
                'game_id': game_id
            },
            success: function() {
                console.log('mdp changé');
                input.val('');
                toastr["success"](Translator.trans('toast.change_password.success'));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.change_password.failure'));
            }
        })

    }
};

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}