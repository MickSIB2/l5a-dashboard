require('./custom');
require('datatables');
require('datatables/media/css/jquery.dataTables.min.css');

jQuery(document).ready(function () {
    dessinerVoyage(coordonnees);
});

$(document).on('click', '#editer-carte', function (event) {
    var that = $(this);
    if ($('#carte').hasClass('carte-editable')) {
        $('#carte').removeClass('carte-editable');
        that.text('Éditer le voyage');
        $('#modal-holder').html('');
    } else {
        $('#carte').addClass('carte-editable');
        that.text('Arrêter l\'édition du voyage');
    }
});

$(document).on('click', '#carte.carte-editable canvas', function (event) {
    var canvas = $('#carte canvas');
    getCursorPosition(canvas, event);
});

function getCursorPosition(canvas, event) {
    var rect = canvas[0].getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;

    var nom = '';
    var est_marqueur = 0;

    var	buttons= {
        "Valider": function() {
            nom = $('#nom-coordonnees').val();
            est_marqueur = $('#est-marqueur').prop('checked') === true ? 1 : 0;
            // Si rien à ajouter, fermer la fenêtre
            if (nom === '') {
                $(this).dialog( "close" );
            }

            $.ajax({
                url: enregistrer_coordonnees,
                method: 'post',
                data: {
                    'x': x,
                    'y': y,
                    'nom': nom,
                    'est_marqueur': est_marqueur
                },
                success: function (data) {
                    toastr["success"](nom + " a été ajouté au voyage.");
                    console.log(data.coordonnees);
                    dessinerVoyage(data.coordonnees);
                },
                error: function () {
                    toastr["error"]("Une erreur est survenue, les coordonnées n'ont pas été enregistrée.");
                }

            });
            $(this).dialog( "close" );
        },
        "Annuler": function() {
            $(this).dialog( "close" );
        }
    };

    var contenu = $('<div/>');
    contenu.append($('<label/>').attr('for', 'nom-coordonnee').text('Veuillez entrer le nom de l\'étape :'));
    var input = $('<input/>').attr({
        'id': 'nom-coordonnees',
        'value': 'Étape'
    });
    var checkbox = $('<div/>').html(
        '<input type="checkbox" id="est-marqueur"/>' +
        '<label for="est-marqueur">Est un marqueur <small>(ne sera pas relié aux autres points)</small></label>'
    );
    contenu.append(input);
    contenu.append(checkbox);

    $('#modal-holder').pup({
        titre: 'Nom de l\'étape',
        modal: true,
        position: {
            my: 'center center'
        },
        containment: false,
        boutons: buttons,
        message: contenu,
        largeur: 400,
        hauteur: 200
    });
}

function dessinerVoyage(coordonnees) {
    var canvas = $('#carte canvas');
    var font_normale = 'bold 12px Rock Salt';
    var font_marqueur = 'bold 18px Rock Salt';
    // Trier par position
    var newACoordonnees = [];
//    newACoordonnees.length = coordonnees.length;
    var aMarqueurs = [];
    $.each(coordonnees, function() {
        var oneCoordonnee = $(this);
        if (oneCoordonnee[0].est_marqueur === true || oneCoordonnee[0].est_marqueur == 1) {
            aMarqueurs.push(oneCoordonnee);
        } else {
            newACoordonnees.push(oneCoordonnee[0]);
        }

    });
    // Inverser l'ordre des coor

    // Dessiner !!!
    var ctx = canvas[0].getContext("2d");
    ctx.font = font_normale;
    ctx.fillStyle = 'white';
    ctx.strokeStyle = '#630b00';

    premier = newACoordonnees[0];
    ctx.moveTo(premier.x, premier.y);
    ecrireNom(ctx, premier.nom, premier.x, premier.y);

    newACoordonnees.splice(0,1); // Enlever la première coordonnée comme elle a déjà été ajoutée.

    $.each(newACoordonnees, function() {
        var that = $(this)[0];
        ctx.lineTo(that.x, that.y);
        ctx.stroke();
        ctx.moveTo(that.x, that.y);
        ecrireNom(ctx, that.nom, that.x, that.y)
    });

    // Dessiner les marqueurs
    $.each(aMarqueurs, function() {
        var that = $(this)[0];
        ctx.fillStyle = '#630b00';
        ctx.font = font_marqueur;
        ctx.fillText('X', that.x, that.y);
        ctx.fillStyle = 'white';
        ctx.font = font_normale;
        ecrireNom(ctx, that.nom, that.x, that.y)
    });
}

function ecrireNom(ctx, nom, x, y) {
    if (nom !== 'Étape') {
        ctx.fillText(nom, x -50, y - 20);
    }
}
