require('../../css/l5a/custom.css');

require('../app');
// import Translator from 'bazinga-translator';
// var toastr =  require('toastr');

    $(document).ready(function () {

        Custom.friendlyWaving();
        Custom.initAccordions();
        toastr.preventDuplicates = true;
        toastr.timeOut = 750;
        toastr.progressBar = true;

        Custom.initBoutons();

        Custom.initCheckboxesJets();

        ////////////////////////////////
        ////////////////////////////////

        $("textarea").each(function (e) {
            Custom.majTextareas($(this));
            $(this).keyup(function (e) {
                Custom.majTextareas($(this));
            });
        });

        $(document).on('click', '.see-more', function () {
            $(this).children(0).toggleClass('opened')
        });

        $(document).on('click', '#ouvrir-notes-perso', function () {
            Custom.ouvrirNotesPerso($(this));
        });

        $(document).on('keyup', '#notes-perso', function () {
            Custom.majNotesPersos($(this));
        });

        $(document).on('click', '.bouton-preparation-lancer', function (e) {
            e.preventDefault();
            Custom.preparerDes($(this));
        });

        /*$(document).on('click', '.editer-gm', function (event) {
            editer(event, $(this));
        });*/

        $('ul.sous-menu li').on('click', function () { // Au clic sur un élément - naviguer sur la page
            var page = $(this).children().attr('href'); // Page cible
            if (page === '#' || page === 'javascript:void(0)') {
                return;
            }
            var speed = 750; // Durée de l'animation (en ms)
            $('html, body').animate({scrollTop: $(page).offset().top}, speed); // Go
            return false;
        });

        $(document).on('click', '#ouvrir-historique-xp', function () {
            Custom.ouvrirHistoriqueXp($(this));
        });

        $(document).on('click', '.toggle-des', function () {
            Custom.toggleDes($(this));
        });

        $(document).on('click', '.afficher-listes', function () {
            Custom.afficherListes($(this));
        });

        /*$(document).on('click', '#afficher-liste-armes', function() {
            afficherListeArmes($(this).attr('data-game-id'));
        });

        $(document).on('click', '#afficher-liste-armures', function() {
            afficherListeArmures($(this).attr('data-game-id'));
        });

        $(document).on('click', '#afficher-liste-avantages', function() {
            afficherListeAvantages($(this).attr('data-game-id'));
        });

        $(document).on('click', '#afficher-liste-techniques', function() {
            afficherListeTechniques($(this).attr('data-game-id'));
        });*/

        $(document).on('click', '.supprimer-arme-gm', function () {
            Custom.supprimerArmeGM($(this));
        });

        $(document).on('click', '.supprimer-armure-gm', function () {
            Custom.supprimerArmureGM($(this));
        });

        $(document).on('click', '.supprimer-avantage-gm', function () {
            Custom.supprimerAvantageGM($(this));
        });

        $(document).on('click', '.supprimer-desavantage-gm', function () {
            Custom.supprimerDesavantageGM($(this));
        });

        $(document).on('click', '.supprimer-technique-gm', function () {
            Custom.supprimerTechniqueGM($(this));
        });

        $(document).on('click', '.afficher-liste-samurai', function () {
            $(this).next().removeClass('hidden');
        });

        $(document).on('click', '.annuler-attribuer', function () {
            $(this).parent().addClass('hidden');
        });

        $(document).on('click', '.valider-attribuer', function () {
            Custom.attribuerEquipement($(this));
        });

        $(document).on('click', '#launch-dice', function () {
            Custom.lancerDes();
        });

        $(document).on('click', '#toggle-chatbox', function () {
            var that = $(this);
            $('#chatbox').animate({height: 'toggle'});
            if (that.attr('data-state') === 'opened') {
                that.attr('data-state', 'closed');
                that.text('Open chatbox');
            } else if (that.attr('data-state') === 'closed') {
                that.attr('data-state', 'opened');
                that.text('Close chatbox');
            }
        });
    });

class Custom {
    _constructor() {

    }
    static preparerDes(element) {
        var jet = Custom.splitG($.trim(element.attr('data-jet')));
        var desLances = jet[0];
        var desGardes = jet[1];
        $('#des-lances').val(desLances);
        $('#des-gardes').val(desGardes);
    }

    static splitG(string) {
        return string.replace('g', 'G').split('G');
    }

    static definirRangsdeBlessures(terre) {
        var blessuresPourIndemne = terre * 5;
        var blessuresParRang = terre * 2;
        var marge = blessuresPourIndemne;
        var rangs = new Array(8);
        rangs[0] = {
            nom: Translator.trans('wounds_rank.indemn'),
            marge: marge,
            malus: '+ 0'
        };
        marge += blessuresParRang;
        rangs[1] = {
            nom: Translator.trans('wounds_rank.scratched'),
            marge: marge,
            malus: '+ 3'
        };
        marge += blessuresParRang;
        rangs[2] = {
            nom: Translator.trans('wounds_rank.slightly_wounded'),
            marge: marge,
            malus: '+ 5'
        };
        marge += blessuresParRang;
        rangs[3] = {
            nom: Translator.trans('wounds_rank.wounded'),
            marge: marge,
            malus: '+ 10'
        };
        marge += blessuresParRang;
        rangs[4] = {
            nom: Translator.trans('wounds_rank.gravely_wounded'),
            marge: marge,
            malus: '+ 15'
        };
        marge += blessuresParRang;
        rangs[5] = {
            nom: Translator.trans('wounds_rank.disabled'),
            marge: marge,
            malus: '+ 20'
        };
        marge += blessuresParRang;
        rangs[6] = {
            nom: Translator.trans('wounds_rank.exhausted'),
            marge: marge,
            malus: Translator.trans('wounds_rank.exhausted_descr')
        };
        marge += blessuresParRang;
        rangs[7] = {
            nom: Translator.trans('wounds_rank.out'),
            marge: marge,
            malus: Translator.trans('wounds_rank.out_descr')
        };
        marge += blessuresParRang;
        rangs[8] = {
            nom: Translator.trans('wounds_rank.dead'),
            marge: marge,
            malus: Translator.trans('wounds_rank.dead_descr')
        };
        return rangs;
    }

    static initAccordions() {
        var acc = $('.accordion');

        acc.each(function () {
            var that = $(this);
            that.click(function () {
                that.toggleClass('active');

                var panel = that.next();

                if (panel.css('max-height') !== '0px') {
                    panel.css('max-height', '0px');
                } else {
                    panel.css('max-height', panel[0].scrollHeight + 'px')
                }
            })
        });

        // Japonais
        if ($('.colonne-japonais').length > 0) {
            var jap = $('.colonne-japonais');
            var holder_width = $('.colonnes-holder').width();
            var colonne_width = $('.colonne-japonais').outerWidth();

            var width_max = holder_width - (colonne_width * 4) - 6; // -6 or not exactly correct somehow
            jap.each(function () {
                var that = $(this);
                that.click(function () {
                    that.toggleClass('active');

                    var panel = that.next();

                    if (panel.css('max-width') !== '0px') {
                        panel.css('max-width', '0px');
                    } else {
                        // Enlever active pour les autres
                        that.siblings('.colonne-japonais').each(function () {
                            $(this).removeClass('active').next().css('max-width', '0px');
                        });

                        panel.css({
                            'max-width': width_max + 'px',
                            'width': width_max + 'px'
                        })
                    }
                });
                if (that.hasClass('defaut')) {
                    var panel = that.next();
                    panel.css({
                        'max-width': width_max + 'px',
                        'width': width_max + 'px'
                    })
                }
            });
        }
    };

    static lancerDes() {
        var desLances = parseInt($('#des-lances').val());
        var desGardes = parseInt($('#des-gardes').val());
        var jetSansCompetence = $('#sans-competence');
        var jetSpecialite = $('#jet-specialite');
        var typeJet = '';
        if (jetSansCompetence.is(':checked')) {
            typeJet = 'sc';
        } else if (jetSpecialite.is(':checked')) {
            typeJet = 'spe';
        }

        var unRelance = 0; // Si spécialité, ne peut relancer qu'un seul 1
        var resultatsDes = [];
        var tousLesDes = [];
        for (var i = 0; i < desLances; i++) {
            var resultatDe = Math.floor((Math.random() * 10) + 1);
            if (typeJet !== 'sc') {
                while ((resultatDe % 10) === 0) {
                    resultatDe += Math.floor((Math.random() * 10) + 1);
                }
                if (typeJet === 'spe' && unRelance === 0) { // Lancer de spécialité
                    if (resultatDe === 1) {
                        resultatDe += Math.floor((Math.random() * 10) + 1);
                        unRelance = 1;
                    }
                }
            }

            tousLesDes.push(resultatDe);
            if (resultatsDes.length < desGardes) {
                resultatsDes[i] = resultatDe;
            } else {
                var minValue = Math.min.apply(Math, resultatsDes); // Applique min au tableau
                if (resultatDe > minValue) {
                    resultatsDes[resultatsDes.indexOf(minValue)] = resultatDe;
                }
            }
        }
        ;

        $('#resultat-des').text(resultatsDes.toString());
        $('#tous-des').text(tousLesDes.toString());
        $('#resultat-total').text(resultatsDes.reduce(function (a, b) {
            return a + b;
        }))
    }

    static initCheckboxesJets() {
        $(document).on('click', "#boite-a-jets input[type='checkbox']", function () {
            $(this).siblings("input[type='checkbox']").prop('checked', false);
        })
    }

    static initBoutons() {
        if ($('.block-anneaux').length > 0) {
            $('.bouton-preparation-lancer.trait').each(function () {
                var that = $(this);
                var valeur = that.parent().siblings('.valeur').text();
                var jet = valeur + 'G' + valeur;
                that.attr('data-jet', jet);
            });
        }

        if ($('#details-principaux').length > 0) {

            $('.bouton-preparation-lancer.competence').each(function () {
                var that = $(this);
                that.attr('data-jet', that.parent().siblings('.jet').text());
            });

            $('.bouton-preparation-lancer.attaque').each(function () {
                var that = $(this);
                that.attr('data-jet', that.parent().parent().parent().find('.jet').text());
            });

            $('.bouton-preparation-lancer.degats').each(function () {
                var that = $(this);
                that.attr('data-jet', that.parent().parent().parent().find('.jet-degats').text());
            });
        }
    }

    static majTextareas(element) {
        if (element.hasClass('exclude-autoupdate')) {
            return;
        }
        while (element.outerHeight() < element[0].scrollHeight + parseFloat(element.css("borderTopWidth")) + parseFloat(element.css("borderBottomWidth"))) {
            element.height(element.height() + 1);
        }
    }

    static ouvrirNotesPerso(element) {
        var notes_perso = element.attr('data-notes-perso');
        if ($('#notes-perso').length > 0) {
            $('#notes-perso-modal').html('');
        }

        var message = $('<textarea/>').attr('id', 'notes-perso').val(notes_perso);
        $('#notes-perso-modal').pup({
            titre: Translator.trans('personal_notes'),
            hauteur: 600,
            largeur: 400,
            message: ''
        });
        // Need to be direct child
        message.appendTo($('#notes-perso-modal'));
    }

    static ouvrirHistoriqueXp(element) {
        var notes_perso = element.attr('data-notes-perso');
        var samurai_id = element.attr('data-samurai-id');
        if ($('#notes-perso').length > 0) {
            $('#notes-perso-modal').html('');
        }

        var message = $('<div/>').attr('id', 'notes-perso');

        $('#notes-perso-modal').pup({
            titre: Translator.trans('menu.xp_history'),
            hauteur: 600,
            largeur: 400
        });
        Custom.displayLoader($('#notes-perso-modal'));

        $.ajax({
            url: route_historique_xp,
            method: 'post',
            data: {
                samurai_id: samurai_id
            },
            success: function (response) {
                message.html(response.html);
                $('#notes-perso-modal').html(message);
            },
            error: function () {
                toastr["error"](Translator.trans('toast.xp_history.failure'));
            }
        });
    }

    static majNotesPersos(element) {
        // var url = Routing.generate('maj_samurai', {id: $('#id-samurai').data('id-samurai')});
        var content = element.val();
        $.ajax({
            url: routeMajSamurai,
            type: 'post',
            data: {
                champ: 'NotesPerso',
                valeur: content
            },
            success: function (response) {
                $('#ouvrir-notes-perso').attr('data-notes-perso', content);
            },
            error: function () {
                toastr["error"](Translator.trans('toast.player_notes.failure'));
            }
        })
    }

    static displayLoader(html) {
        $(html).html(
            '<div style="text-align:center;margin-bottom: 75px">' +
            '<div class="uil-spin-css" style="-webkit-transform:scale(0.6)">' +
            '<div><div></div></div>' +
            '<div><div></div></div>' +
            '<div><div></div></div>' +
            '<div><div></div></div>' +
            '<div><div></div></div>' +
            '<div><div></div></div>' +
            '<div><div></div></div>' +
            '<div><div></div></div>' +
            '</div>' +
            '</div>');
    }

    static friendlyWaving() {
        console.log('O|__|O  *Waving in a friendly fashion*');
    }

    static toggleDes(element) {
        element.addClass('hidden');
        element.siblings('.toggle-des').removeClass('hidden');
    }

    static supprimerArmeGM(element) {
        var id = element.attr('data-id');

        var modale = $('#notes-perso-modal');
        if ($('#notes-perso').length > 0) {
            modale.html('');
        }

        var boutons = [];
        buttons[Translator.trans('Validate')] = function () {
            $.ajax({
                url: route_supprimerArme,
                method: 'post',
                data: {id: id},
                success: function (response) {
                    $('#arme-' + id).remove();
                    var row = $(element.parents('tr'));
                    row.remove();
                    toastr["success"](Translator.trans('toast.delete_weapon.success'));
                },
                error: function () {
                    toastr["error"](Translator.trans('toast.delete_weapon.failure'));
                }
            });
            $(this).dialog("close");
        };
        buttons[Translator.trans('Cancel')] = function () {
            $(this).dialog("close");
        };
        modale.pup({
            titre: Translator.trans('pup.title.delete_weapon'),
            message: '<p>' + Translator.trans('pup.text.validate_delete_weapon') + '</p>',
            boutons: boutons
        });
    }

    static supprimerArmureGM(element) {
        var id = element.attr('data-id');

        var modale = $('#notes-perso-modal');
        if ($('#notes-perso').length > 0) {
            modale.html('');
        }

        var boutons = [];
        buttons[Translator.trans('Validate')] = function () {
            $.ajax({
                url: route_supprimerArmure,
                method: 'post',
                data: {id: id},
                success: function (response) {
                    $('#armure-' + id).remove();
                    var row = $(element.parents('tr'));
                    row.remove();
                    toastr["success"](Translator.trans('toast.delete_armor.success'));
                },
                error: function () {
                    toastr["error"](Translator.trans('toast.delete_armor.failure'));
                }
            });
            $(this).dialog("close");
        };
        buttons[Translator.trans('Cancel')] = function () {
            $(this).dialog("close");
        };
        modale.pup({
            titre: Translator.trans('pup.title.delete_armor'),
            message: '<p>' + Translator.trans('pup.text.validate_delete_armor') + '</p>',
            boutons: boutons
        });
    }

    static supprimerAvantageGM(element) {
        var id = element.attr('data-id');

        var modale = $('#notes-perso-modal');
        if ($('#notes-perso').length > 0) {
            modale.html('');
        }

        var boutons = [];
        buttons[Translator.trans('Validate')] = function () {
            $.ajax({
                url: route_supprimerAvantage,
                method: 'post',
                data: {id: id},
                success: function (response) {
                    $('#avantage-' + id).remove();
                    var row = $(element.parents('tr'));
                    row.remove();
                    toastr["success"](Translator.trans('toast.delete_advantage.success'));
                },
                error: function () {
                    toastr["error"](Translator.trans('toast.delete_advantage.failure'));
                }
            });
            $(this).dialog("close");
        };
        buttons[Translator.trans('Cancel')] = function () {
            $(this).dialog("close");
        };
        modale.pup({
            titre: Translator.trans('pup.title.delete_advantage'),
            message: '<p>' + Translator.trans('pup.text.validate_delete_advantage') + '</p>',
            boutons: boutons
        });
    }

    static supprimerDesavantageGM(element) {
        var id = element.attr('data-id');

        var modale = $('#notes-perso-modal');
        if ($('#notes-perso').length > 0) {
            modale.html('');
        }

        var boutons = [];
        boutons[Translator.trans('validate')] = function () {
            $.ajax({
                url: route_supprimerDesavantage,
                method: 'post',
                data: {id: id},
                success: function (response) {
                    $('#desavantage-' + id).remove();
                    var row = $(element.parents('tr'));
                    row.remove();
                    toastr["success"](Translator.trans('toast.delete_disadvantage.success'));
                },
                error: function () {
                    toastr["error"](Translator.trans('toast.delete_disadvantage.failure'));
                }
            });
            $(this).dialog("close");
        };
        boutons[Translator.trans('Cancle')] = function () {
            $(this).dialog("close");
        };
        modale.pup({
            titre: Translator.trans('pup.title.delete_disadvantage'),
            message: '<p>' + Translator.trans('pup.text.validate_delete_disadvantage') + '</p>',
            boutons: boutons
        });
    }

    /*function convertirPDF(element) {
        var id = element.attr('data-samurai-id');
        var url = route_convertir_pdf;
        $.ajax({
           url: url,
           method: 'get',
           'success': {}
        });
    }*/

    static afficherListes(element) {
        var game_id = element.attr('data-game-id');
        var type = element.attr('data-type');
        var url = route_listes;

        if (game_id === null) {
            toastr["error"](Translator.trans('toast.no_game'));
            return;
        }

        var modal_existe = $('#liste-archive').length > 0;
        var message = $('<div/>').attr('id', 'liste-archive');

        if (!modal_existe) {
            $('#cadre-listes').pup({
                titre: 'Archives',
                hauteur: 800,
                largeur: 1000,
                message: ''
            });

            var cadre_liste = $('<div/>').html(
                '<nav>\n' +
                '            <button class="afficher-listes boutons" data-type="techniques" data-game-id="' + game_id + '">' + Translator.trans('Techniques') + '</button>\n' +
                '            <button class="afficher-listes boutons" data-type="desavantages" data-game-id="' + game_id + '">' + Translator.trans('Disadvantages') + '</button>\n' +
                '            <button class="afficher-listes boutons" data-type="avantages" data-game-id="' + game_id + '">' + Translator.trans('Advantages') + '</button>\n' +
                '            <button class="afficher-listes boutons" data-type="armures" data-game-id="' + game_id + '">' + Translator.trans('Armors') + '</button>\n' +
                '            <button class="afficher-listes boutons" data-type="armes" data-game-id="' + game_id + '">' + Translator.trans('weapons') + '</button>\n' +
                '            <button class="afficher-listes boutons" data-type="pnjs" data-game-id="' + game_id + '">' + Translator.trans('NPCs') + '</button>\n' +
                '        </nav>\n' +
                '        <div class="espace-listes"></div>');
        }
        $('#cadre-listes').append(cadre_liste);
        $('#cadre-listes .afficher-listes.disabled').each(function() {
          $(this).removeClass('disabled');
        });
        $('#cadre-listes .afficher-listes[data-type=' + type + ']').addClass('disabled');
        Custom.displayLoader($('#cadre-listes .espace-listes'));

        $.ajax({
            url: url,
            method: 'post',
            data: {
                game_id: game_id,
                type: type
            },
            success: function (response) {
                message.html(response.html);
                $('#cadre-listes .espace-listes').html(message);
                $('#cadre-listes .espace-listes .datatable').DataTable({
                    'order': [0, 'asc'],
                    autoWidth: false
                });
            }
        });
    }

    static attribuerEquipement(element) {
        var samurai_id = element.prev().val();
        var type = element.attr('data-type');
        var valeur = element.attr('data-id');
        var nom = element.attr('data-nom');
        $.ajax({
            url: route_attribuerEquipement,
            method: 'post',
            data: {
                samurai_id: samurai_id,
                valeur: valeur,
                nom: nom,
                type: type
            },
            success: function (response) {
                toastr["success"](Translator.trans('toast.attributed_item.success'));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.attributed_item.failure'));
            }
        });
    }

    static supprimerTechniqueGM(element) {
        var id = element.attr('data-id');

        var modale = $('#notes-perso-modal');
        if ($('#notes-perso').length > 0) {
            modale.html('');
        }

        var buttons = [];
        buttons[Translator.trans('Validate')] = function () {
            $.ajax({
                url: route_supprimerTechnique,
                method: 'post',
                data: {id: id},
                success: function (response) {
                    if (response.success === true) {
                        $('#technique-' + id).remove();
                        var row = $(element.parents('tr'));
                        row.remove();
                        toastr["success"](Translator.trans('toast.delete_technique.success'));
                    } else {
                        toastr["error"](response.message);
                    }
                },
                error: function (response) {
                    toastr["error"](Translator.trans('toast.delete_technique.failure'));
                }
            });
            $(this).dialog("close");
        };
        buttons[Translator.trans('Cancel')] = function () {
            $(this).dialog("close");
        };

        modale.pup({
            titre: Translator.trans('pup.title.delete_technique'),
            message: '<p>' + Translator.trans('pup.text.validate_delete_technique') + '</p>',
            boutons: buttons
        });
    }

}

export default Custom;
/*function afficherListeArmes(game_id) {
    var url = route_liste_armes;
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var message = $('<div/>').attr('id', 'notes-perso');

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.weapons_list'),
        hauteur: 800,
        largeur: 1000
    });
    displayLoader($('#notes-perso-modal'));

    $.ajax({
        url: url,
        method: 'post',
        data: {
            game_id: game_id
        },
        success: function (response) {
            message.html(response.html);
            $('#notes-perso-modal').html(message);
            $('#table-arme').DataTable({
                'order': [0, 'asc'],
                autoWidth: false
            });
        }
    });
}

function afficherListeArmures(game_id) {
    var url = route_liste_armures;
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var message = $('<div/>').attr('id', 'notes-perso');

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.armors_list'),
        hauteur: 800,
        largeur: 1000
    });
    displayLoader($('#notes-perso-modal'));

    $.ajax({
        url: url,
        method: 'post',
        data: {
            game_id: game_id
        },
        success: function (response) {
            message.html(response.html);
            $('#notes-perso-modal').html(message);
            $('#table-armure').DataTable({
                'order': [0, 'asc'],
                autoWidth: false
            });
        }
    });
}

function afficherListeAvantages(game_id) {
    var url = route_liste_avantages;
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var message = $('<div/>').attr('id', 'notes-perso');

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.advantages_list'),
        hauteur: 800,
        largeur: 1000
    });
    displayLoader($('#notes-perso-modal'));

    $.ajax({
        url: url,
        method: 'post',
        data: {
            game_id: game_id
        },
        success: function (response) {
            message.html(response.html);
            $('#notes-perso-modal').html(message);
            $('#table-avantages').DataTable({
                'order': [0, 'asc'],
                autoWidth: false
            });
        }
    });
}

function afficherListeDesavantages(game_id) {
    var url = route_liste_desavantages;
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var message = $('<div/>').attr('id', 'notes-perso');

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.disadvantages_list'),
        hauteur: 800,
        largeur: 1000
    });
    displayLoader($('#notes-perso-modal'));

    $.ajax({
        url: url,
        method: 'post',
        data: {
            game_id: game_id
        },
        success: function (response) {
            message.html(response.html);
            $('#notes-perso-modal').html(message);
            $('#table-desavantages').DataTable({
                'order': [0, 'asc'],
                autoWidth: false
            });
        }
    });

}*/

/*function afficherListeTechniques(game_id) {
    var url = route_liste_techniques;
    if ($('#notes-perso').length > 0) {
        $('#notes-perso-modal').html('');
    }

    var message = $('<div/>').attr('id', 'notes-perso');

    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.techniques_list'),
        hauteur: 800,
        largeur: 1000
    });
    displayLoader($('#notes-perso-modal'));

    $.ajax({
        url: url,
        method: 'post',
        data: {
            game_id: game_id
        },
        success: function (response) {
            message.html(response.html);
            $('#notes-perso-modal').html(message);
            $('#table-techniques').DataTable({
                'order': [0, 'asc'],
                autoWidth: false
            });
        }
    });

}*/
