require ('../../css/l5a/game-master.css');
require('datatables');
require('datatables/media/css/jquery.dataTables.min.css');
import Custom from './custom';

// Page GM Dashboard
    // Init dataTables
if ($('.datatable').length > 0) {
    console.log($('.datatable').length + ' datatables');
    $('.datatable').each(function() {
        $(this).DataTable({
            // scrollY: 1000
            // columns.editField
            'order': [0, 'asc'],
            autoWidth: false,
        });
    })
};

checkTypeArme();
majRangsBlessures();
// iterator for additional bonus fields
var i_field = 1;

$(document).on('change', '#l5a_personnage_terre', function() {
    majRangsBlessures();
});

$(document).on('change', '#l5a_arme_type', function () {
    checkTypeArme();
});

$(document).on('change', '.filtre-sort', function() {
    filtrer_attribut_sort($(this));
});

$(document).on('click', '.attribuer-joueur', function () {
    attribuerJoueur($(this));
});

$(document).on('click', '.editer-perso', function () {
    gmEditerPerso($(this));
});

$(document).on('click', '#add-bonus', function(e) {
    // Ajouter la ligne et le select de type d'attribut
    e.preventDefault();
    var newTypeSelect = $('#type-bonus-model').clone().attr({
        'id': 'l5a_personnage_bonus_Type_'+i_field+'',
        'class': 'get-bonus l5a-form',
        'data-type-bonus': 'Type',
        'name': 'l5a_personnage[bonus_Type_'+i_field+']'
    });
    var newRow = $('<div/>').attr({
        'class': 'col-md-12 mb row',
        'data-field': i_field
    });

    newTypeSelect.appendTo(newRow);
    newTypeSelect.removeClass('hidden');
    newRow.prependTo('#block-bonus');
    i_field++;
});

$(document).on('click', '#rafraichir-liste-samurais', function () {
    rafraichirListeSamurais($(this));
});

$(document).on('change', '.get-bonus', function() {
    // Vars
    var that = $(this);
    var value = that.val();
    var row = $(that.parent('.row')[0]);
    var row_id = row.attr('data-field');
    var type_bonus = that.attr('data-type-bonus');

    // Traitement
    $.ajax({
        url: chemin.getBonus,
        type: 'post',
        data: {
            type_valeur: type_bonus,
            valeur: value
        },
        success: function (response) {
            var i_field_1 = i_field -1;
            if (row.find('#l5a_personnage_bonus_'+response.champ+'_'+i_field_1).length > 0 || type_bonus === 'Type') {
                var champsAEnlever = type_bonus === 'Type' ?
                    row.find('#l5a_personnage_bonus_Type_'+i_field_1).nextAll():
                    row.find('#l5a_personnage_bonus_'+response.champ+'_'+i_field_1).nextAll();
                // Virer tout ce qui suit le Type quand on le change (mais pas le Type lui-même)
                if (type_bonus !== 'Type') {
                    row.find('#l5a_personnage_bonus_'+response.champ+'_'+i_field_1).remove();
                }
                champsAEnlever.each(function () {
                    $(this).remove();
                })
            }

            // Si Sorts, afficher tout
            var champModel = null;
            if (response.champ  === 'Sorts') {
                champModel = [
                    $('#Anneau-bonus-model'),
                    $('#Maitrise-bonus-model'),
                    $('#Nom-bonus-model'),
                    $('#Description-bonus-model')
                ];

                $.each(champModel, function() {
                    var that = $(this);
                    var champ = that.clone();
                    let fieldName = '';
                    var classes = 'col-md-2 attribut-'+row_id+'';
                    // Note the order
                    switch (that.attr('id')) {
                        case 'Anneau-bonus-model':
                            classes += ' anneau filtre-sort l5a-form';
                            fieldName = 'Anneau';
                            break;
                        case 'Maitrise-bonus-model':
                            classes += ' maitrise filtre-sort l5a-form';
                            fieldName = 'Maitrise';
                            break;
                        case 'Nom-bonus-model':
                            classes += ' nom bonus-nom l5a-form';
                            champ.append(response.content);
                            fieldName = 'Nom';
                            break;
                        case 'Description-bonus-model':
                            classes += ' description Description l5a-form';
                            fieldName = 'Description';
                            break;
                    }
                    champ.attr({
                        'class': classes,
                        'data-type-bonus': fieldName,
                        'id': 'l5a_personnage_bonus_'+fieldName+'_'+row_id+'',
                        'name': 'l5a_personnage[bonus_'+fieldName+'_'+row_id+']'
                    });
                    champ.appendTo(row);
                });
            } else {
                champModel = $('#' + response.champ + '-bonus-model');

                var classes =
                    response.champ === 'Nom' ?
                          'bonus-nom l5a-form':
                        'get-bonus l5a-form '+type_bonus+'';
                var newChamp = champModel.clone().attr({
                    'id': 'l5a_personnage_bonus_'+response.champ+'_'+row_id+'',
                    'class': classes,
                    'data-type-bonus': response.champ,
                    'name': 'l5a_personnage[bonus_'+response.champ+'_'+row_id+']'
                });
                newChamp.append(response.content);

                newChamp.appendTo(row);
            }
/*               var classes =
                response.champ === 'Nom' ?
                    'bonus-nom col-md-2':
                    'get-bonus col-md-2 '+type_bonus+'';
            var newChamp = champModel.clone().attr({
                'id': 'l5a_personnage_bonus_'+response.champ+'_'+row_id+'',
                'class': classes,
                'data-type-bonus': response.champ,
                'name': 'l5a_personnage[bonus_'+response.champ+'_'+row_id+']'
            });
            newChamp.append(response.content);
            newChamp.appendTo(row);*/
        }
    })
});

$(document).on('change', '.bonus-nom', function () {
    var that = $(this);
    var description = that.find(':selected').attr('data-description');
    var row = $(that.parent('.row')[0]);
    var row_id = row.attr('data-field');

    if (row.find('.Description').length > 0) {
        var field_description = row.find('.Description');
        field_description.val(description);
        Custom.majTextareas(field_description);
    } else {
        var champModel = $('#Description-bonus-model');
        var newChamp = champModel.clone().attr({
            'id': 'l5a_personnage_bonus_Description_'+row_id+'',
            'class': 'Description l5a-form',
            'name': 'l5a_personnage[bonus_Description_'+row_id+']'
        });
        newChamp.val(description);
        newChamp.appendTo(row);
        Custom.majTextareas(newChamp);
    }
});

$(document).on('click', '#ajouter-xp', function () {
    ajouterXP($(this));
});

$(document).on('keyup', '#maj-notes-gm textarea', function() {
    majNotesGM($(this));
});

function filtrer_attribut_sort(element) {
		var row = element.parent();
		var anneau = element.hasClass('anneau') ? element.val() : row.find('.anneau').val();
		var maitrise = element.hasClass('maitrise') ? element.val() : row.find('.maitrise').val();

		var sorts = row.find('.nom option');
		sorts.each(function() {
			var oneSort = $(this);
			var voir = true; // Si voir reste 0 true, ne pas cacher le sort.
			var maitrise_sort = oneSort.attr('data-rang');
			var anneau_sort = oneSort.attr('data-anneau');
			if (anneau !== '' && anneau !== anneau_sort) {
				voir = false;
			}
			if (maitrise < maitrise_sort) {
				voir = false;
			}

			// moment of truth
			if (voir) {
				oneSort.removeClass('hidden');
			} else {
				oneSort.addClass('hidden');
			}
		});
	}

function attribuerJoueur(element) {
    var samurai_id = element.attr('data-id');
    var nouveau_joueur = 'none';

    var	buttons= {
        "Valider": function() {

            // Si rien à ajouter, fermer la fenêtre
            if (nouveau_joueur === 'none') {
                $(this).dialog( "close" );
            }

            $.ajax({
                url: chemin.attribuerJoueur,
                method: 'post',
                data: {
                    'samurai_id': samurai_id,
                    'nouveau_joueur': nouveau_joueur
                },
                success: function (data) {
                    rafraichirListeSamurais();
                    toastr["success"]("Le joueur a été attribué.");
                },
                error: function () {
                    toastr["error"]("Une erreur est survenue, le joueur n'a pas pu être attribué.");
                }

            });
            $(this).dialog( "close" );
        },
        "Annuler": function() {
            $(this).dialog( "close" );
        }
    };


    $(document).on("change", "#select-joueur", function(){
        nouveau_joueur = $(this).val(); //get new value
    });

    var contenu = $('<div/>').attr('id', 'attribuer-joueur');

    var liste_joueurs = $('#liste-joueurs select').clone();
    contenu.append(liste_joueurs);

    $('#notes-perso-modal').pup({
        titre: 'Attribuer un joueur',
        modal: true,
        position: {
            my: 'center center'
        },
        containment: false,
        boutons: buttons,
        message: contenu
    });
};

function gmEditerPerso(element) {
    var samurai_id = element.attr('data-id');
    var form = $('#editer-perso-model .content').clone();

    var	buttons= {
        "Valider": function() {
            var
                honneur = form.find('input[name="points-honneur"]').val(),
                gloire = form.find('input[name="points-gloire"]').val(),
                statut = form.find('input[name="points-statut"]').val(),
                souillure = form.find('input[name="points-souillure"]').val(),
                terre = form.find('input[name="points-terre"]').val(),
                air = form.find('input[name="points-air"]').val(),
                eau = form.find('input[name="points-eau"]').val(),
                feu = form.find('input[name="points-feu"]').val(),
                vide = form.find('input[name="points-vide"]').val();

            $.ajax({
                url: chemin.gmEditerSamurai,
                method: 'post',
                data: {
                    samurai_id: samurai_id,
                    honneur: honneur,
                    gloire: gloire,
                    statut: statut,
                    souillure: souillure,
                    terre: terre,
                    air: air,
                    eau: eau,
                    feu: feu,
                    vide: vide
                },
                success: function (data) {
                    if (data.success === false) {
                        toastr["error"](data.message);
                        return;
                    }
                    rafraichirListeSamurais();
                    toastr["success"]("Le samuraï a été modifié.");
                },
                error: function () {
                    toastr["error"]("Une erreur est survenue, le samuraï n'a pas pu être modifié.");
                }

            });
            $(this).dialog( "close" );
        },
        "Annuler": function() {
            $(this).dialog( "close" );
        }
    };

    var contenu = $('<div/>').attr('id', 'editer-perso');

    contenu.append(form);

    $('#notes-perso-modal').pup({
        titre: 'Éditer un samuraï',
        modal: true,
        hauteur: 'auto',
        largeur: 250,
        position: {
            my: 'center center'
        },
        containment: false,
        boutons: buttons,
        message: contenu
    });
};

function rafraichirListeSamurais(element) {
    Custom.displayLoader('#liste-samurais');
    $.ajax({
        type: 'post',
        url: chemin.rafraichirListeSamurais,
        success: function(data) {
            $('#liste-samurais').html(data.html);
        },
        error: function () {
            toastr["error"](Translator.trans('toast.samurai_list.failure'));
        }
    });
}

function ajouterXP(element) {

    var buttons = {};
    buttons[Translator.trans('Validate')]= function() {
        Custom.displayLoader('#liste-samurais');
        var joueurs = [];
        $($('#attribuer-xp').find('input.pj')).each(function() {
            var joueur = $(this);
            if(joueur.is(':checked')) {
                joueurs.push($(this).attr('data-id'));
            }
        });
        var valeur = $('#combien-xp').val();
        $.ajax({
            url: chemin.attribuerXP,
            method: 'post',
            data: {
                'valeur': valeur,
                'joueurs': joueurs
            },
            success: function (data) {
                $('#liste-samurais').html(data.html);
                toastr["success"](Translator.trans('toast.add_xp.success'));
            },
            error: function () {
                toastr["error"](Translator.trans('toast.add_xp.failure'));
            }

        });
        $(this).dialog( "close" );
    };
    buttons[Translator.trans('Cancel')]= function() {
        $(this).dialog( "close" );
    };

    var contenu = $('<div/>').attr('id', 'attribuer-xp');
    var a_liste_samurai = [];

    $($('#liste-samurais').find('tr.samurai')).each(function() {
        var samurai_row = $(this);
        var samurai_id = samurai_row.attr('data-id');
        var samurai_nom = $(samurai_row.children('.nom')[0]).text();
        var a_samurai = [samurai_id, samurai_nom];
        // a_samurai[0] = samurai_id;
        // a_samurai['nom'] = samurai_nom;

        a_liste_samurai.push(a_samurai);
        var check_box_joueur =
            $('<div/>');
        check_box_joueur.html(
            '<input type="checkbox" class="pj" data-id="'+samurai_id+'" name="pj-'+samurai_id+'" checked />'+
            '<label for="pj-'+samurai_id+'">'+samurai_nom+'</label>'
        );
        contenu.append(check_box_joueur);
    });
    var combien_xp = $('<div/>').attr('class', 'col-md-1');
    combien_xp.html(
        '<div class="separateur"></div>'+
        '<label for="combien-xp">'+ Translator.trans('xp_points') +'</label>'+
        '<input type="number" name="combien-xp" id="combien-xp" value="0" />'
    );
    contenu.append(combien_xp);

    console.log(buttons);
    $('#notes-perso-modal').pup({
        titre: Translator.trans('pup.title.give_xp'),
        modal: true,
        containment: false,
        hauteur: 400,
        largeur: 300,
        boutons: buttons,
        message: contenu
    });
}

function majNotesGM(element) {
    var valeur = element.val();
    var url = chemin.majNotesGM;
    $.ajax({
        url: url,
        type: 'post',
        data: { valeur: valeur }
    });
}

function checkTypeArme() {
    var type = $('#l5a_arme_type').val();
    var vd = $($('#l5a_arme_vd').parents('.col-md-4'));
    var force = $($('#l5a_arme_force').parents('.col-md-4'));
    var portee = $($('#l5a_arme_portee').parents('.col-md-4'));

    if (type === Translator.trans('arme.arc')) {
        vd.addClass('hidden');
        force.removeClass('hidden');
        portee.removeClass('hidden');
    } else {
        vd.removeClass('hidden');
        force.addClass('hidden');
        portee.addClass('hidden');
    }
}

function majRangsBlessures() {
    var element = $('#l5a_personnage_terre');
    var nouvelle_terre = element.val();
    var rangs = Custom.definirRangsdeBlessures(nouvelle_terre);
    for (var i = 0; i < rangs.length; i++) {
        $('#l5a_personnage_rangBlessure'+parseInt(i+1)).attr('value',rangs[i]['marge']);
    }
}

/*function editer(e, element) {
    e.preventDefault();
    var id = element.attr('data-id');
    var edit = element.attr('data-edit');
    var blockToLoad = '#form-' + edit.toLowerCase();
    Custom.displayLoader(blockToLoad);

    var speed = 750; // Durée de l'animation (en ms)
    $('html, body').animate( { scrollTop: $(blockToLoad).offset().top }, speed ); // Go

    var url = '';
    switch (edit) {
        case 'PNJ':
            url =
                id === 'new' ?
                    chemin.creerPNJ :
                    chemin.editerPNJ;
            break;
        case 'Arme':
            url =
                id === 'new' ?
                    chemin.creerArme :
                    chemin.editerArme;
            break;
        case 'Armure':
            url =
                id === 'new' ?
                    chemin.creerArmure :
                    chemin.editerArmure;
            break;
    }
    $.ajax({
        url: url,
        type: 'post',
        data: {
            'id': id,
            'return-form': true
        },
        success: function(response) {
            $(blockToLoad).html(response.content);
            if (edit === 'PNJ') {
                majRangsBlessures();
            }
            var le = edit === 'PNJ' || edit === 'Désavantage' || edit === 'Technique' ? Translator.trans('Le')+' ' : Translator.trans('L');
            toastr["success"](Translator.trans('toast.edit2.success', {'le': le, 'edit': edit}));
        },
        error: function () {
            var le =
                edit === 'PNJ' || edit === 'Désavantage' || edit === 'Technique' ?
                    lowercase(Translator.trans('Le')) +' ' : lowercase(Translator.trans('L'));
            toastr["error"](Translator.trans('toast.edit2.failure', {'le': le, 'edit': edit}));
        }
    })
}*/
