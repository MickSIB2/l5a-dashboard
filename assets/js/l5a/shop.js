/**
 * Created by Mickaël on 18/02/2017.
 */
require('./custom');
require('datatables');
require('datatables/media/css/jquery.dataTables.min.css');
require('../../css/l5a/shop.css');

var gm = $('#samurai-id').text() === 'gm';

$(document).ready(function() {
    var columnsCompetences =
        gm ?
            [
                null,
                { "bSearchable": false },
                null,
                { "bSearchable": false },
                null,
                null,
                { "bSearchable": false }
            ] :
            [
                null,
                { "bSearchable": false },
                null,
                { "bSearchable": false },
                null,
                null,
                null,
                { "bSearchable": false },
                { "bSearchable": false }
            ];

    $('#table-competences.datatable').DataTable({
        // scrollY: 1000
        // columns.editField
        'order': [0, 'asc'],
        autoWidth: false,
        'aoColumns': columnsCompetences
    });

    var columnsArmes =
        gm ?
            [
                null,
                { "bSearchable": false },
                null,
                null,
                null,
                null,
                null,
                { "bSearchable": false }
            ] :
            [
                null,
                { "bSearchable": false },
                null,
                null,
                null,
                null,
                null,
                { "bSearchable": false },
                { "bSearchable": false }
            ];
    $('#table-armes.datatable').DataTable({
        // scrollY: 1000
        // columns.editField
        'order': [0, 'asc'],
        autoWidth: false,
        'aoColumns': columnsArmes
    });

    var columnsKatas =
        gm ?
            [
                null,
                null,
                null,
                null,
                null
            ] :
            [
                null,
                null,
                null,
                null,
                null,
                { "bSearchable": false }
            ];
    $('#table-katas.datatable').DataTable({
        // scrollY: 1000
        // columns.editField
        'order': [0, 'asc'],
        autoWidth: false,
        'aoColumns': columnsKatas
    });

    $('#table-avantages.datatable').DataTable({
        // scrollY: 1000
        // columns.editField
        'order': [0, 'asc'],
        autoWidth: false
    });
    $('#table-desavantages.datatable').DataTable({
        // scrollY: 1000
        // columns.editField
        'order': [0, 'asc'],
        autoWidth: false
    });

    $(document).on('click', '.acheter', function () {
        acheterCompetence($(this));
    });

    $(document).on('click', '.acheter-specialite', function () {
        acheterSpecialite($(this));
    });

    $(document).on('click', '.acheter-kata', function () {
        acheterKata($(this));
    });

    $(document).on('click', '.augmenter', function () {
        acheterTrait($(this));
    });

    $(document).on('click', '.acheter-item', function () {
        acheterItem($(this));
    });

    $(document).on('click', '#filtrer', function () {
        filtrerSorts();
    });

    $(document).on('click', '.acheter-sort', function () {
        acheterSort($(this));
    });

    $(document).on('click', '.ajouter-des-avantage', function () {
        ajouterDesAvantages($(this));
    });

    $(document).on('click', '#acheter-des-avantages', function () {
        acheterDesAvantages($(this));
    });
    $(document).on('click', '#refresh-xp', function () {
        refresh_xp();
    });

    // Réinitialier les checkbox pour la page desAvantages, sinon en raffaîchissant la page on peut
    // avoir des avantages gratuitements
    var checkboxes = $('input[type=checkbox]');
    checkboxes.each(function () {
        var that = $(this);
        that.prop('checked',false);
    });
});

function acheterCompetence(element) {
    element.attr('disabled', true);
    refresh_xp(function () {
        element.removeAttr('disabled');
        var competenceId = element.data('id');
        var rang = parseInt($($(element.parents()[0]).siblings('.rang')).text());
        var xp_restante_block = $('#indicateur-bas-de-page #xp-restante');
        var xprestante = xp_restante_block.text();
        var buttons = {};
        var message = '';
        var titre = 'Vous ne pouvez pas faire ceci';
        var cout = rang + 1;

        if ($('#achat-creation-'+competenceId).prop('checked')) {
            cout = 0;
        }
        var diff = cout - xprestante;

        if (diff > 0) {
            message = '<p>Il vous manque ' + diff + ' points d\'Expérience.</p>';
        } else {
            message =
                '<p>Voulez-vous acheter cette compétence pour ' + cout + ' points d\'Expérience ?' +
                ' Vous ne pourrez pas faire marche arrière.</p>'
            ;
            titre = "Confirmation de l'achat";
            buttons = {
                "Enregistrer": function() {
                    $.ajax({
                        url: chemins.acheterCompetence,
                        method: 'post',
                        data: {
                            competenceId: competenceId,
                            rang: rang +1,
                            cout: cout
                        },
                        success: function (response) {
                            $('#xp-restante').text(response.xpRestante);
                            $('#competence-' + competenceId + ' .rang').text(response.rang);
                            if (response.rang == 1) { // Ne pas mettre ===
                                var parent = $(element.parent(0));
                                parent.html('');
                                parent.append(
                                    '<button class="acheter" data-id="' + competenceId + '">Améliorer</button>'+
                                    '<button class="acheter-specialite" data-id="' + competenceId + '"' +
                                    'data-specialites="' + response.specialisations + '" ' +
                                    'data-specialites-acquises="">' +
                                    'Acheter Spécialité</button>'
                                );
                            }
                            toastr["success"]("La compétence a été achetée avec succès.");
                        },
                        error: function () {
                            toastr["error"]("Une erreur est survenue, la compétence n'a pas pu être achetée.");
                        }
                    });
                    $(this).dialog( "close" );
                },
                "Annuler": function() {
                    $(this).dialog( "close" );
                }
            };
        }

        $('#modal-principale').pup({
            titre: titre,
            hauteur: 'auto',
            message: message,
            boutons: buttons,
            modal: true
        });
    });
}

function acheterSpecialite(element) {
    element.attr('disabled', true);
    refresh_xp(function () {
        element.removeAttr('disabled');
        var competenceId = element.data('id');
        var xprestante = $('#indicateur-bas-de-page #xp-restante').text();
        var cout = 2;
        var diff = cout - xprestante;
        var specialites = element.attr('data-specialites').split(',');
        var specialites_acquises = element.attr('data-specialites-acquises') + ' ';
        var buttons = {};
        var message = '';
        var titre = 'Vous ne pouvez pas faire ceci';

        if (diff > 0) {
            message = '<p>Il vous manque ' + diff + ' points d\'Expérience.</p>';
        } else {
            var select = '<select class="choisir-specialite">' + '<choices>';
            $.each(specialites, function (i, l) {
                var that = $.trim(l);
                if (that !== 'Dépend de la discipline' &&
                    (specialites_acquises.toLocaleLowerCase().indexOf(that.toLocaleLowerCase()) === -1)) {
                    select += '<option value="' + that + '">' + that + '</option>';
                }
            });
            select +=
                '<option value="Autre">Autre</option>' +
                '</choices>' +
                '</select>' +
                '<input class="hidden specialite-perso" type="text">';

            message =
                '<p>Voulez-vous acheter une spécialité pour cette compétence pour ' + cout + ' points d\'Expérience ? ' +
                'Vous ne pourrez pas faire marche arrière.</p>';
            if (specialites_acquises !== ' ') {
                message += '<p>Vous possédez déjà ' + specialites_acquises + '</p>';
            }
            message += select;

            if ($('.choisir-specialite').val() === 'Autre') {
                $('.specialite-perso').removeClass('hidden');
            }
            $(document).on('change', '.choisir-specialite', function () {
                if ($(this).val() === 'Autre') {
                    $('.specialite-perso').removeClass('hidden');
                } else {
                    $('.specialite-perso').addClass('hidden');
                }
            });
            titre = "Confirmation de l'achat";

            buttons = {
                "Enregistrer": function () {
                    if ($('.choisir-specialite').val() === 'Aucune') {
                        alert('Aucune spécialité sélectionnée');
                        return;
                    }
                    var specialite =
                        $('.choisir-specialite').val() === 'Autre' ?
                            $('.specialite-perso').val() :
                            $('.choisir-specialite').val();
                    $.ajax({
                        url: chemins.acheterSpecialite,
                        method: 'post',
                        data: {
                            competenceId: competenceId,
                            specialite: specialite
                        },
                        success: function (response) {
                            $('#xp-restante').text(response.xpRestante);
                            element.attr('data-specialites-acquises', response.new_specialities);
                            toastr["success"]("La spécialité a été achetée avec succès.");
                        },
                        error: function () {
                            toastr["error"]("Une erreur est survenue, la spécialité n'a pas pu être achetée.");
                        }
                    });
                    $(this).dialog("close");
                },
                "Annuler": function () {
                    $(this).dialog("close");
                }
            };
        }
        ;

        $('#modal-principale').pup({
            titre: titre,
            hauteur: 'auto',
            message: message,
            boutons: buttons,
            modal: true
        });
    });
}

function acheterTrait(element) {
    element.attr('disabled', true);
    refresh_xp(function () {
        element.removeAttr('disabled');
        var trait = element.data('trait');
        var anneau = element.data('anneau');
        var valeur = parseInt($(element.parent(0)).prev().text());
        var xprestante = $('#indicateur-bas-de-page #xp-restante').text();
        var buttons = {};
        var message = '';
        var titre = 'Vous ne pouvez pas faire ceci';

        var cout =
            trait === 'Vide' ?
                (valeur + 1) * 6 :
                (valeur + 1) * 4;
        var diff = cout - xprestante;
        titre = "Confirmation de l'achat";

        if (diff > 0) {
            message = '<p>Il vous manque ' + diff + ' points d\'Expérience.</p>';
        } else {
            var de =
                (trait === 'Intuition' || trait === 'Agilite' || trait === 'Intelligence') ?
                    'd\'' :
                    'de ';

            message =
                '<p>Voulez-vous acheter un rang ' + de + trait + ' pour ' + cout + ' points d\'Expérience ? Vous ne pourrez pas faire marche arrière.</p>';
            buttons = {
                "Enregistrer": function () {
                    $.ajax({
                        url: chemins.majSamurai,
                        method: 'post',
                        data: {
                            champ: trait,
                            valeur: valeur + 1,
                            anneau: anneau,
                            cout: cout
                        },
                        success: function (response) {
                            $('#xp-restante').text(response.xpRestante);
                            $('#anneau-' + anneau).text(response.anneauValeur);
                            $(element.parent(0)).prev().text(valeur + 1)
                            toastr["success"]("Le trait a été acheté avec succès.");
                        },
                        error: function () {
                            toastr["error"]("Une erreur est survenue, le trait n'a pas pu être achetée.");
                        }
                    });
                    $(this).dialog("close");
                },
                "Annuler": function () {
                    $(this).dialog("close");
                }
            };
        }

        $('#modal-principale').pup({
            titre: titre,
            hauteur: 'auto',
            message: message,
            boutons: buttons,
            modal: true
        });
    });
}

function acheterKata(element) {
    element.attr('disabled', true);
    refresh_xp(function () {
        element.removeAttr('disabled');
        var kataId = element.data('id');
        var cout = element.data('cout');
        var nom = element.data('nom');
        var anneau = element.data('anneau');
        var anneau_valeur = element.data('anneau-valeur');
        var xprestante = $('#indicateur-bas-de-page #xp-restante').text();
        var buttons = {};
        var message = '';
        var titre = 'Vous ne pouvez pas faire ceci';

        var diff = cout - xprestante;

        if (diff > 0) {
            message = '<p>Il vous manque ' + diff + ' points d\'Expérience.</p>';

        } else if (anneau_valeur < cout) {
            message = '<p>Vous devez avoir un d\'anneau ' + anneau + ' au rang ' + cout + ', et le vôtre est au rang ' + anneau_valeur + '.</p>';
        } else {
            message =
                '<p>Voulez-vous acheter le kata ' + nom + ' pour ' + cout + ' points d\'Expérience ? Vous ne pourrez pas faire marche arrière.</p>';
            titre = "Confirmation de l'achat";
            buttons = {
                "Enregistrer": function () {
                    $.ajax({
                        url: routeAcheterKata,
                        method: 'post',
                        data: {
                            kata_id: kataId,
                            cout_xp: cout
                        },
                        success: function (response) {
                            if (response.success === false) {
                                toastr["error"](response.message);
                            }
                            $('#xp-restante').text(response.xpRestante);
                            $('#kata-' + kataId).addClass('disabled');
                            $('#kata-' + kataId).find('.acheter-kata').remove();
                            toastr["success"]("Le kata a été acheté avec succès.");
                        },
                        error: function () {
                            toastr["error"]("Une erreur est survenue, le kata n'a pas pu être achetée.");
                        }
                    });
                    $(this).dialog("close");
                },
                "Annuler": function () {
                    $(this).dialog("close");
                }
            };
        }

        $('#modal-principale').pup({
            titre: titre,
            hauteur: 'auto',
            message: message,
            boutons: buttons,
            modal: true
        });
    });
}

function acheterItem(element) {
    var valeur = element.data('cout');
    var nom = element.data('nom');
    var id = element.data('id');
    var type = element.data('type');
    var cout = valeur;
    var koku = 0;
    var bu = 0;
    var article = 'un';
    var nb_fleches = null;
    var buttons = {};
    var message = '';
    var titre = 'Vous ne pouvez pas faire ceci';

    if (type === 'fleche') {
        nb_fleches = parseInt($(element.parent(0).parent(0).find('.nombre-fleches')).val());
        if (nb_fleches === 0) {
            return;
        }
        cout = cout * nb_fleches;
        article = nb_fleches;
    }

    cout = cout.toString();

    zeni = parseInt(cout.substr(-1, 1));
    if (cout.length >= 2) {
        bu = parseInt(cout.substr(-2, 1));
    }
    if (cout.length >= 3) {
        koku = parseInt(cout.substr(0, cout.length - 2)) * 2;
    }

    var koku_restants = parseInt($('#koku').text());
    var bu_restants = parseInt($('#bu').text());
    var zeni_restants = parseInt($('#zeni').text());

    // en zeni
    var monnaie_restante = koku_restants * 50 + bu_restants * 10 + zeni_restants;

    if ($('#achat-creation-'+id).prop('checked')) {
        cout = 0;
    }
    var diff = parseInt(cout) - monnaie_restante;

    if (diff > 0) {
        message = '<p>Il vous manque ' + diff + ' zeni.</p>';

    } else {
        message = '<p>Voulez-vous acheter ' + article  + ' ' + nom + ' pour ' + cout + ' zeni ? Vous ne pourrez pas' +
            ' faire marche arrière.</p>';
        titre = "Confirmation de l'achat";
        buttons = {
            "Enregistrer": function() {
                $.ajax({
                    url: chemins.acheterItem,
                    method: 'post',
                    data: {
                        type: type,
                        id: id,
                        cout_zeni: cout,
                        nb_fleches: nb_fleches
                    },
                    success: function (response) {
                        if (response.success === false) {
                            toastr["error"](response.message);
                            return;
                        }
                        $('#koku').text(response.koku);
                        $('#bu').text(response.bu);
                        $('#zeni').text(response.zeni);
                        toastr["success"]("L'achat a été effectué avec succès.");
                    },
                    error: function () {
                        toastr["error"](response.message);
                    }
                });
                $(this).dialog( "close" );
            },
            "Annuler": function() {
                $(this).dialog( "close" );
            }
        };
    };

    $('#modal-principale').pup({
        titre: titre,
        hauteur: 'auto',
        message: message,
        boutons: buttons,
        modal: true
    });
}

function filtrerSorts() {
	var nom = $('#filtre-nom').val(),
	rang_max = $('#filtre-rang-max').val(),
	mot_cle = $('#filtre-mot-cle').val();
	
	$('.sort').each(function() {
		var sort = $(this);
		
		if (sort.attr('data-nom').indexOf(nom) === -1 || parseInt(sort.attr('data-rang')) > rang_max || sort.attr('data-mc').indexOf(mot_cle) === -1) {
			sort.hide(500);
		} else {
			sort.show(500);
		}
	})
}

function acheterSort(element) {
    var sortId = element.attr('data-id');
    var cout = element.attr('data-maitrise');
	var nom = $(element.parent(0)).find('.titre').text();
    var xprestante = $('#indicateur-bas-de-page #xp-restante').text();
    if ($('#achat-creation-'+sortId).prop('checked')) {
        cout = 0;
    }
    cout = 0; // Acheter un Sort est gratuit
    var buttons = {};
    var message = '';
    var titre = 'Vous ne pouvez pas faire ceci';

    var diff = cout - xprestante;

    if (diff > 0) {
        message = '<p>Il vous manque ' + diff + ' points d\'Expérience.</p>';
        $('#modal-principale').append(message);
    } else {
        message = '<p>Voulez-vous acheter le sort <strong>'+nom+'</strong> ? ' +
            'Vous ne pourrez pas faire marche arrière.</p>'
        ;
        titre = "Confirmation de l'achat";
        buttons = {
            "Enregistrer": function() {
                $.ajax({
                    url: routeAcheterSort,
                    method: 'post',
                    data: {
                        sort_id: sortId
                    },
                    success: function (response) {
                        if (response.success === false) {
                            toastr["error"](response.message);
                            return;
                        }
                        $($(element.parent(0)).parent(0)).addClass('disabled');
                        element.hide();
                        toastr["success"]("Le sort " + nom + " a été acheté avec succès.");
                    },
                    error: function () {
                        toastr["error"]("Une erreur est survenue, le sort " + nom + " n'a pas pu être acheté.");
                    }
                });
                $(this).dialog( "close" );
            },
            "Annuler": function() {
                $(this).dialog( "close" );
            }
        };
    };

    $('#modal-principale').pup({
        titre: titre,
        hauteur: 'auto',
        message: message,
        boutons: buttons,
        modal: true
    });
}



function ajouterDesAvantages(element) {
    var id = element.attr('data-id');
    var type = element.attr('data-type');
    var valeur = parseInt(element.attr('data-valeur'));
    var nom = element.attr('data-nom');
    var message = '';

    // samurai concerné
    var samurai_select = element.prev();
    if (samurai_select.val() === '') {
        return;
    }
    var samurai_id = samurai_select.val(),
        option_sel = samurai_select.find('option:selected'),
        samurai_nom = option_sel.text(),
        samurai_des_avantage = option_sel.attr('data-'+type+'s').split(','),
        samurai_xp_restant = option_sel.attr('data-xp-restant')
    ;

    var le = type === 'avantage' ? 'l\'' : 'le ';

    var titre = 'Attribuer ' + type;
    buttons = {};

    var prix_custom = '<input type="number" min="0" value="'+valeur+'"' +
        ' class="prix-custom">' + '<choices>';

    if ($.inArray(id, samurai_des_avantage) !== -1) {
        message = '<p>' + samurai_nom + ' possède déjà ' + nom + '. Le supprimer ?</p>';
    } else {
        message = '<p>Voulez-vous attribuer ' + le + type + ' <strong>' + nom + '</strong> pour ' + prix_custom +
            ' points d\'Expérience pour ' + samurai_nom + ' ? Vous ne pourrez pas faire marche arrière.</p>';
    }

   titre = "Confirmation de l'achat";
    buttons = {
        "Valider": function() {
            var cout_final = $('.prix-custom').val();

            // Si désavantage, ne pas prendre en compte la différence
            var diff = type === 'avantage' ? cout_final - samurai_xp_restant : 0;
            if (diff > 0) {
                alert('Il vous manque ' + diff + ' points d\'Expérience.');
                return;
            }

            $.ajax({
                url: chemins.attribuerDesAvantages,
                method: 'post',
                data: {
                    type: type,
                    id: id,
                    valeur: cout_final,
                    samurai_id: samurai_id
                },
                success: function (response) {
                    option_sel.attr('data-'+type+'s', option_sel.attr('data-'+type+'s') + ',' + id);
                    $('option').each(function() {
                        var that = $(this);
                        if (that.attr('value') === samurai_id) {
                            that.attr('data-xp-restant', response.xp_restant)
                        }
                    });
                    toastr["success"](samurai_nom + " a maintenant " + le + type + nom + ".");
                },
                error: function () {
                    toastr["error"]("Une erreur est survenue, l'achat n'a pas pu être effectué.");
                }
            });
            $(this).dialog( "close" );
        },
        "Annuler": function() {
            $(this).dialog( "close" );
        }
    };

    $('#modal-principale').pup({
        titre: titre,
        modal: true,
        hauteur: 'auto',
        message: message,
        boutons: buttons
    });

    return;
    var xprestante = parseInt($('#xp-restante').text());

    switch (type) {
        case 'avantage':
            // Mettre à jour l'aperçu
            apercu_nvl_xp =
                is_checked ?
                    xprestante - valeur:
                    xprestante + valeur;

            // Mettre à jour les avantages
                var avantages =
                    is_checked ?
                        listes.attr('data-avantages') + ',' + id :
                        listes.attr('data-avantages').replace(id, '').replace(',,', ',')
                ;
            listes.attr('data-avantages', trimComa(avantages));
            break;

        case 'desavantage':
            // Mettre à jour l'aperçu
            apercu_nvl_xp =
                is_checked ?
                    xprestante + valeur:
                    xprestante - valeur;

            // Mettre à jour les désavantages
            var desavantages =
                is_checked ?
                    listes.attr('data-desavantages') + ',' + id :
                    listes.attr('data-desavantages').replace(id, '').replace(',,', ',')
            ;
            listes.attr('data-desavantages', trimComa(desavantages));
            break;
    }

    $('#xp-restante').text(apercu_nvl_xp);
}

function acheterDesAvantages(element) {
    var avantages = element.attr('data-avantages');
    var desavantages = element.attr('data-desavantages');
    var nvlle_xp = $('#xp-restante').text();
    var url = chemins.acheterDesAvantages;
    $.ajax({
        url: url,
        type: 'post',
        data: {
            avantages: avantages,
            desavantages: desavantages,
            nvlle_xp: nvlle_xp
        },
        success: function (response) {
            var url_redirect = chemins.samurai;
            window.location.replace(url_redirect);
        }
    });
}

function refresh_xp (callback) {
    var url = routeGetXp;
    $.ajax({
        url: url,
        method: 'post',
        success: function (response) {
            $('#xp-restante').text(response.xp);
            if (callback !== undefined) {
                callback();
            }
            return true;
        },
        failure: function () {
            toastr["error"]("Une erreur est survenue, l'expérience n'a pas pu être mise à jour.");
            return false;
        }
    });
}

/**
 * Permet de virer une , en début  et fin de chaîne
 * @param str La chaîne à modifier
 */
function trimComa(str) {
    return str.replace(/(^,)|(,$)/g, "");
}