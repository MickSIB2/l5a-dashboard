import Custom from './custom';

require('datatables');
require('datatables/media/css/jquery.dataTables.min.css');

///// Créer Perso /////////
$('select#samurai_clanId, select#samurai_familleId, select#samurai_type').each(function() {
    chargerInformations($(this), false);
});

$(document).on('change', '#toutes-ecoles', function() {
    cacherEcoles($('select#samurai_clanId').val(), $('select#samurai_type').val());
});

$(document).on('change', 'form[name="samurai"] .form-group-1 select', function () {
    if ($(this).attr('name') !== 'samurai[joueur]') {
        toggleInformations(false);
        chargerInformations($(this), true);
    }
});

$(document).on('click', '#creation-samurai-next', function(e) {
    e.preventDefault();
    toggleInformations(true);
});

// Charger les informations
function chargerInformations(element, afficher_informations) {
    var name = element.attr('name');
    var val = element.val();
    var table = '';

    switch (name) {
        case 'samurai[clanId]':
            var type = $('select#samurai_type').val();

            //Réinitialiser les select
            $('select#samurai_familleId, select#samurai_ecoleId').each(function(){
                $(this).val('');
            });
            $('select#samurai_familleId option').each(function() {
                var choix = $(this);
                if (choix.hasClass('choix')) {
                    if (choix.hasClass('clan-'+val)) {
                        choix.show();
                    } else {
                        choix.hide();
                    }
                }
            });
            cacherEcoles(val, type);

            table = 'clan';
            // Enlever les familles pour Ronin et Confrérie de Shinsei
            if (val == 31 || val == 32) {
                $('label[for="samurai_nom"]').html(Translator.trans('fullname') +' <small class="required" title="This field is required">*</small>');
                $('#samurai_familleId').attr('required', false).addClass('hidden');
                $('label[for="samurai_familleId"]').addClass('hidden');
            } else {
                $('label[for="samurai_nom"]').html(Translator.trans('First Name') +' <small class="required" title="This field is required">*</small>');
                $('#samurai_familleId').attr('required', true).removeClass('hidden');
                $('label[for="samurai_familleId"]').removeClass('hidden');
            }

            afficher_informations = true; // Afficher les informations au chargement de la page.
            break;
        case 'samurai[familleId]':
            table = 'famille';
            break;
        case 'samurai[type]':

            //Réinitialiser les select
            $('select#samurai_ecoleId').each(function(){
                $(this).val('');
            });

            var clanId = $('select#samurai_clanId').val();
            cacherEcoles(clanId, val);

            afficher_informations = false; // Aucune information à afficher
            break;
        case 'samurai[ecoleId]':

            table = 'ecole';
            break;
    }

    if (afficher_informations && val !== '') {
        // Afficher les informations
        var url = chemins.afficherInformations;
        Custom.displayLoader('#informations');
        $.ajax({
            method: 'post',
            url: url,
            data: {
                'table': table,
                'valeur': val
            },
            success: function(data) {
                $('#informations').html(data.html);
            },
            error: function () {
                toastr["error"](Translator.trans('toast.error'));
            }
        });
    }
}

function toggleInformations(nextonly) {
    var informations = $('#informations');
    var details = $('#details');
    if (informations.hasClass('hidden') && ! nextonly) {
        details.addClass('hidden');
        informations.removeClass('hidden');
    } else {
        if (nextonly) {
            informations.addClass('hidden');
            details.removeClass('hidden');
        }
    }
}

function cacherEcoles(clanId, filtre) {
    var clanSeulement = $('#toutes-ecoles').prop('checked');
    $('select#samurai_ecoleId option').each(function() {
        var choix = $(this);
        var filtre_type = (filtre !== '') ? ' type-' + filtre : '';
        // Si filtre vide, ne pas regarder si l'option a la classe filtrée
        var matchfiltre = filtre_type !== '' ? choix.hasClass($.trim(filtre_type)) : true;

        var check = (!clanSeulement && matchfiltre) ||
            (clanSeulement && choix.hasClass('clan-' + clanId + filtre_type));

        if (choix.hasClass('choix')) {
            if (check) {
                choix.show();
            } else {
                choix.hide();
            }
        }
    });
}
