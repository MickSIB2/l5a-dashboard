(function (t) {
// fr
t.add("light.name", "Armure L\u00e9g\u00e8re", "armor", "fr");
t.add("light.rules", "Le port de l'armure l\u00e9g\u00e8re augmente de 5 le ND de tous les Jets d'Athl\u00e9tisme et de Discr\u00e9tion", "armor", "fr");
t.add("light.description", null, "armor", "fr");
t.add("heavy.name", "Armure Lourde", "armor", "fr");
t.add("heavy.rules", "Le port de l'armure lourde augmente de 5 le ND de tous les Jets de Comp\u00e9tence utilisant l'Agilit\u00e9 ou les r\u00e9fl\u00e8xes.", "armor", "fr");
t.add("heavy.description", null, "armor", "fr");
t.add("cavalry.name", "Armure de Cavalerie", "armor", "fr");
t.add("cavalry.rules", "Le bonus au ND est de 12 quand le porteur est \u00e0 cheval. On traite l'armure de cavalerie comme une armure lourde quand il s'agit d'appliquer tous les effets des r\u00e8gles li\u00e9s \u00e0 nu type d'armure sp\u00e9cifique, y compris les Techniques, Avantages, etc. Porter une armure de cavalerie augmente le ND de 5 de tous les Jets de Comp\u00e9tences utilisant l'Agilit\u00e9 ou les R\u00e9fl\u00e8xes quand son porteur ne se trouve pas \u00e0 cheval.  \n", "armor", "fr");
t.add("cavalry.description", null, "armor", "fr");
t.add("ashigaru.name", "Armure d'Ashigaru", "armor", "fr");
t.add("ashigaru.rules", null, "armor", "fr");
t.add("ashigaru.description", null, "armor", "fr");
t.add("fancy.name", "V\u00eatements Extravagants", "armor", "fr");
t.add("fancy.rules", null, "armor", "fr");
t.add("fancy.description", null, "armor", "fr");
t.add("traditional.name", "V\u00eatemens Traditionnels", "armor", "fr");
t.add("traditional.rules", null, "armor", "fr");
t.add("traditional.description", null, "armor", "fr");
t.add("stout.name", "V\u00eatements Robustes", "armor", "fr");
t.add("stout.rules", null, "armor", "fr");
t.add("stout.description", null, "armor", "fr");
t.add("robe.name", "Robe", "armor", "fr");
t.add("robe.rules", null, "armor", "fr");
t.add("robe.description", null, "armor", "fr");
})(Translator);
