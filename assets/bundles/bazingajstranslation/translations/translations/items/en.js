(function (t) {
// en
t.add("Armure l\u00e9g\u00e8re", "Light armor", "items", "en");
t.add("V\u00eatements robustes", "Stout clothes", "items", "en");
t.add("V\u00eatements traditionnels", "Traditional clothes", "items", "en");
t.add("V\u00eatements  traditionnels", "Traditional clothes", "items", "en");
t.add("N\u00e9cessaire de Calligraphie", "N\u00e9cessaire de Calligraphie", "items", "en");
t.add("N\u00e9cessaire  de  Calligraphie", "N\u00e9cessaire de Calligraphie", "items", "en");
t.add("N\u00e9cessaire de Voyage", "N\u00e9cessaire de Voyage", "items", "en");
t.add("Couteau", "Knife", "items", "en");
})(Translator);
