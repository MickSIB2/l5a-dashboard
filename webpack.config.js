var Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
var webpack = require('webpack');

Encore

    .setOutputPath('public/build/')

    .setPublicPath('/build')

    .addEntry('app', './assets/js/app.js')
    //.addEntry('functions', './assets/js/functions.js')
    //.addEntry('app', './assets/js/components/bs-datatable.js')
    //.addEntry('datatables', './assets/js/components/real-datatables.js')
    //.addEntry('app', './assets/js/plugins/jquery.color.js')
    //.addEntry('app', './assets/js/plugins/jquery.form.js')
    //.addEntry('app', './assets/js/plugins/jquery.validation.js')
    //.addEntry('jquery-ui', './assets/js/plugins/jquery-ui.js')
    //.addEntry('popup', './assets/js/plugins/popup.js')
    //.addEntry('toatr', './assets/js/plugins/toastr.min.js')

    .addEntry('custom', './assets/js/l5a/custom.js')
    .addEntry('map', './assets/js/l5a/map.js')
    .addEntry('combat', './assets/js/l5a/combat.js')
    .addEntry('game-master', './assets/js/l5a/game-master.js')
    .addEntry('shop', './assets/js/l5a/shop.js')
    .addEntry('game', './assets/js/l5a/game.js')
    .addEntry('character-creation', './assets/js/l5a/character-creation.js')
    .addEntry('character-sheet', './assets/js/l5a/character-sheet.js')
    .addEntry('welcome', './assets/js/l5a/welcome.js')

    .enableBuildNotifications()
    .autoProvidejQuery()

    .addPlugin(new CopyWebpackPlugin([
        { from: './assets/images', to: 'images' },
        { from: './assets/bundles/bazingajstranslation/translations', to: 'translations' }
    ]))

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

// uncomment if you use TypeScript
//.enableTypeScriptLoader()

// uncomment if you use Sass/SCSS files
//.enableSassLoader()
;

module.exports = Encore.getWebpackConfig();
