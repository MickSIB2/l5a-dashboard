<?php

namespace App\Form\L5A;

use App\Entity\L5A\Ecole;
use App\Entity\L5A\Technique;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Button;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class TechniqueType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('id', HiddenType::class)
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('description')
			->add('ecoleVoie', TextType::class, array(
				'required' => false,
				'label' => 'form.path',
				'translation_domain' => 'messages'
			))
			->add('rangMinimum', IntegerType::class, array(
				'required' => false,
				'label' => 'minimum_rank',
				'translation_domain' => 'messages'
			))
			->add('officielle', ChoiceType::class, array(
				'choices' => array(
					'Yes' => '1',
					'No' => '0'
				),
				'label' => 'Official_fem',
				'translation_domain' => 'messages',
				'choice_translation_domain' => 'messages'
			))
			->add('typePersonnage', ChoiceType::class, array(
				'choices' => array(
					'bushi' => 'samurai.type.bushi',
					'shugenja' => 'samurai.type.shugenja',
					'courtisan' => 'samurai.type.courtier',
					'moine' => 'samurai.type.monk',
					'artisan' => 'samurai.type.craftman',
				),
				'label' => 'character_type',
				'translation_domain' => 'messages',
				'choice_translation_domain' => 'messages'					
			))
			->add('ecoles', EntityType::class, array(
				'class' => Ecole::class,
				'choice_label' => 'nom',
				'multiple' => true,
				'required' => false,
				'attr' => array(
					'size' => '7'
				),
				'label' => 'form.schools',
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('e')
			          ->orderBy('e.nom', 'ASC');
				},
				'translation_domain' => 'messages',
				'choice_translation_domain' => 'school'	
			))
			->add('save', SubmitType::class, array(
				'attr' => array(
					'class' => 'save btn',
					'data-id' => 'new',
					'data-edit' => 'Technique'
				),
				'label' => 'form.save',
				'translation_domain' => 'messages'	
			))
			->add('annuler', ButtonType::class, array(
				'attr' => array(
					'class' => 'annuler btn col-md-offset-1 editer-gm',
					'data-id' => 'new',
					'data-edit' => 'Technique'
				),
				'label' => 'form.cancel',
				'translation_domain' => 'messages'
			))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(
			array(
				'data_class' => Technique::class
			)
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'l5a_technique';
	}
}
