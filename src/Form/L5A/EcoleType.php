<?php

namespace App\Form\L5A;

//use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use App\Entity\L5A\Ecole;
use App\Entity\L5A\Clan;
use App\Entity\L5A\Competence;
use App\Entity\L5A\Sort;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;;
use Symfony\Component\Form\Button;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
//use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class EcoleType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('id', HiddenType::class)
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('type', ChoiceType::class, array(
				'choices' => array(
					'bushi' => 'samurai.type.bushi',
					'shugenja' => 'samurai.type.shugenja',
					'courtisan' => 'samurai.type.courtier',
					'moine' => 'samurai.type.monk',
					'artisan' => 'samurai.type.craftman',
				),
				'choice_translation_domain' => 'messages',
				'translation_domain' => 'messages'
			))
			->add('traitBonus', ChoiceType::class, array(
				'choices' => array(
					'Constitution' => 'samurai.trait.constitution',
					'Volonté' => 'samurai.trait.will',
					'Reflèxes' => 'samurai.trait.reflexes',
					'Intuition' => 'samurai.trait.awareness',
					'Force' => 'samurai.trait.strength',
					'Perception' => 'samurai.trait.perception',
					'Agilité' => 'samurai.trait.agility',
					'Intelligence' => 'samurai.trait.intelligence',
					'Vide' => 'samurai.ring.void'
				),
				'label' => 'school.bonus_trait',
				'choice_translation_domain' => 'messages',
				'translation_domain' => 'messages'
			))
			->add('description')
			->add('clanId', EntityType::class, array(
				'class' => Clan::class,
				'choice_label' => 'nom',
				'label' => 'Clan',
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('c')
					          ->orderBy('c.nom', 'ASC');
				},
				'choice_translation_domain' => 'messages',
				'translation_domain' => 'messages'
			))
			->add('honneurDepart', IntegerType::class, array(
					'choices' => array(
							'Constitution' => 'samurai.trait.constitution',
							'Volonté' => 'samurai.trait.will',
							'Reflèxes' => 'samurai.trait.reflexes',
							'Intuition' => 'samurai.trait.awareness',
							'Force' => 'samurai.trait.strength',
							'Perception' => 'samurai.trait.perception',
							'Agilité' => 'samurai.trait.agility',
							'Intelligence' => 'samurai.trait.intelligence',
							'Vide' => 'samurai.ring.void'
					),
					'label' => 'school.starting_honour',
					'choice_translation_domain' => 'messages',
					'translation_domain' => 'messages'
			))
			->add('competences', EntityType::class, array(
				'class' => Competence::class,
				'choice_label' => 'nom',
				'label' => 'Skills',
				'multiple' => true,
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('c')
					          ->orderBy('c.nom', 'ASC');
				},
				'choice_translation_domain' => 'skills',
				'translation_domain' => 'messages'
			))
			->add('affinite', ChoiceType::class, array(
				'choices' => array(
					'None_fem',
					'samurai.ring.earth',
					'samurai.ring.air',
					'samurai.ring.water',
					'samurai.ring.fire',
					'samurai.ring.void'
				),
				'label' => 'school.affinity',
				'choices_as_values',
				'translation_domain' => 'messages'
			))
			->add('deficience', ChoiceType::class, array(
				'choices' => array(
					'None_fem',
					'samurai.ring.earth',
					'samurai.ring.air',
					'samurai.ring.water',
					'samurai.ring.fire',
					'samurai.ring.void'
				),
				'label' => 'school.weakness',
				'choices_as_values',
				'translation_domain' => 'messages'
			))
			->add('sorts', EntityType::class, array(
				'class' => Sort::class,
				'choice_label' => 'nom',
				'label' => 'Spells',
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('c')
					          ->orderBy('c.nom', 'ASC');
				},
				'translation_domain' => 'messages',
				'choice_translation_domain' => 'spells'
			))
			->add('equipement', TextType::class, array(
				'disabled' => true
			))
			->add('save', SubmitType::class, array(
					'attr' => array(
						'class' => 'save btn',
						'data-id' => 'new',
						'data-edit' => 'Ecole'
					),
					'label' => 'form.save',
					'translation_domain' => 'messages'		
				)
			)
			->add('annuler', ButtonType::class, array(
				'attr' => array(
					'class' => 'annuler btn col-md-offset-1 editer-gm',
					'data-id' => 'new',
					'data-edit' => 'Arme'
				),
				'label' => 'form.cancel',
				'translation_domain' => 'messages'
			))
			;
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(
			array(
				'data_class' => Ecole::class
			)
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'l5a_ecole';
	}
}
