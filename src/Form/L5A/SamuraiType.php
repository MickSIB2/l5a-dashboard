<?php

namespace App\Form\L5A;

use App\Entity\L5A\AppUser;
use App\Entity\L5A\Ecole;
use Doctrine\ORM\EntityRepository;
use App\Entity\L5A\Samurai;
use App\Entity\L5A\Clan;
use App\Entity\L5A\Famille;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SamuraiType extends AbstractType
{
	private $gameId;

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $this->gameId = $options['gameId'];
	    $builder
		    ->add('nom', TextType::class, array(
	    		'label' => 'First Name',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('clanId', EntityType::class, array(
			    'class' => Clan::class,
			    'choice_label' => 'nom',
				'mapped' => false,
				'label' => 'Clan',
			    'query_builder' => function (EntityRepository $er) {
				    return $er->createQueryBuilder('c')
				              ->orderBy('c.nom', 'ASC');
			    },
			    'choice_translation_domain' => 'clans'
		    ))

		    ->add('familleId', EntityType::class, array(
		    	'class' => Famille::class,
			    'choice_label' => 'nom',
			    'mapped' => false,
			    'choice_attr' => function($val, $key, $index) {
		    		return array('class' => 'choix clan-'.$val->getClanId());
			    },
			    'required' => true,
			    'placeholder' => 'form.choose_family',
				'label' => 'Family',
			    'query_builder' => function (EntityRepository $er) {
				    return $er->createQueryBuilder('f')
				              ->orderBy('f.nom', 'ASC');
			    },
			    'choice_translation_domain' => 'families',
			    'translation_domain' => 'messages'
		    ))

		    ->add('type', ChoiceType::class, array(
		    	'choices' => array(
	    			'samurai.type.bushi' => 'bushi',
	    			'samurai.type.shugenja' => 'shugenja',
	    			'samurai.type.courtier' => 'courtier',
	    			'samurai.type.monk' => 'monk',
	    			'samurai.type.craftman' => 'craftman',
			    ),
			    'placeholder' => 'All',
				'required' => false,
			    'label' => 'form.school_type',
		    	'translation_domain' => 'messages'
		    ))

		    ->add('ecoleId', EntityType::class, array(
		    	'class' => Ecole::class,
			    'choice_label' => 'nom',
			    'mapped' => false,
			    'choice_attr' => function($val, $key, $index) {
				    return array('class' => 'choix clan-'.$val->getClanId() . ' type-'.strtolower(
				    	explode('.',$val->getType())[2]) // Look for the last part of the translation key.
				    );
			    },
			    'required' => true,
			    'placeholder' => 'form.choose_school',
				'label' => 'School',
			    'query_builder' => function (EntityRepository $er) {
				    return $er->createQueryBuilder('e')
				              ->orderBy('e.nom', 'ASC');
			    },
			    'translation_domain' => 'messages',
			    'choice_translation_domain' => 'schools'
		    ))

		    ->add('joueur', EntityType::class, array(
			    'class' => AppUser::class,
			    'choice_label' => 'username',
			    'required' => false,
			    'mapped' => false,
			    'placeholder' => 'form.belongs_to',
			    'label' => 'Player',
			    'query_builder' => function (EntityRepository $er) {
				    return $er->createQueryBuilder('u')
			                    ->where('u.gameId = ' .$this->gameId)
				              ->orderBy('u.username', 'ASC');
			    },
			    'translation_domain' => 'messages',
		    ))

		    ->add('sexe', ChoiceType::class, array(
			    'choices' => array(
				   'Homme' => 'Man',
				   'Femme' => 'Woman',
			    ),
			    'required' => false,
	    		'placeholder' => 'form.choose_sex',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('age', IntegerType::class, array(
		    	'required' => false,
	    		'label' => 'Age',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('taille', TextType::class, array(
		    	'required' => false,
	    		'label' => 'Height',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('cheveux', TextType::class, array(
	    		'required' => false,
	    		'label' => 'Hair',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('yeux', TextType::class, array(
	    		'required' => false,
	    		'label' => 'Eyes',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('pere', TextType::class, array(
		    	'required' => false,
	    		'label' => 'Father',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('mere', TextType::class, array(
		    	'required' => false,
	    		'label' => 'Mother',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('freresSoeurs', TextType::class, array(
		    	'required' => false,
	    		'label' => 'BrothersSisters',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('situationFamiliale', TextAreaType::class, array(
		    	'required' => false,
				'attr' => array(
					'rows' => 1,
					'style' => 'height:auto'
				),
	    		'label' => 'family_situation',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('marieA', TextType::class, array(
		    	'required' => false,
	    		'label' => 'Married To',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('enfants', TextType::class, array(
	    		'required' => false,
	    		'label' => 'Children',
	    		'translation_domain' => 'messages'
		    ))

		    ->add('notes', TextareaType::class, array(
		    	'required' => false,
				'attr' => array(
					'rows' => 2,
					'style' => 'height:auto'
				)
		    ))

		    ->add('save', SubmitType::class, array(
		    	'attr' => array('class' => 'save btn'),
	    		'label' => 'form.create',
	    		'translation_domain' => 'messages'
		    ))
	    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
	    $resolver->setDefaults(array(
	    	'data_class' => Samurai::class,
		    'gameId' => 0
	    ));
    }

    public function getName()
    {
        return 'samurai_form';
    }
}
