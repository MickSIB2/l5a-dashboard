<?php

namespace App\Form\L5A;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use App\Entity\L5A\Avantage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Button;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
//use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class AvantageType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('id', HiddenType::class)
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('description')
			->add('valeur', IntegerType::class, array(
				'required' => true,
				'label' => 'des_avantage.valeur',
				'translation_domain' => 'messages'
			))
			->add('sousType', TextType::class, array(
				'required' => false,
				'label' => 'des_avantage.subtype',
				'translation_domain' => 'messages'
			))
			->add('save', SubmitType::class, array(
					'attr' => array(
						'class' => 'save btn',
						'data-id' => 'new',
						'data-edit' => 'Avantage'
					),
					'label' => 'form.save',
					'translation_domain' => 'messages'
				)
			)
			->add('annuler', ButtonType::class, array(
				'attr' => array(
					'class' => 'annuler btn col-md-offset-1 editer-gm',
					'data-id' => 'new',
					'data-edit' => 'Avantage'
				),
				'label' => 'form.cancel',
				'translation_domain' => 'messages'	
			))
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(
			array(
				'data_class' => Avantage::class
			)
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'l5a_avantage';
	}
}
