<?php

namespace App\Form\L5A;

use App\Entity\L5A\Sort;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SortType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('id', HiddenType::class)
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('anneau', ChoiceType::class, array(
				'required' => false,
				'choices' => array(
					'samurai.ring.earth',
					'samurai.ring.air',
					'samurai.ring.water',
					'samurai.ring.fire',
					'samurai.ring.void',
					'All'
				),
				'choice_label' => function ($value, $key, $index) {
					return $value;
				},
				'placeholder' => 'form.choose_ring',
				'translation_domain' => 'messages',
				'choice_translation_domain' => 'messages'
			))
			->add('maitrise', NumberType::class, array(
				'attr' => array(
					'value' => 1,
					'min' 	=> 1
				),
				'label' => 'Mastery',
				'translation_domain' => 'messages'
			))
			->add('motsCles', TextType::class, array(
				'required' => false,
				'label' => 'keyword',
				'translation_domain' => 'messages'
			))
			->add('portee', TextType::class, array(
				'required' => false,
				'label' => 'Reach',
				'translation_domain' => 'messages'
			))
			->add('zoneEffet', TextType::class, array(
				'required' => false,
				'label' => 'spell.aoe',
				'translation_domain' => 'messages'
			))
			->add('duree', TextType::class, array(
				'required' => false,
				'label' => 'spell.duration',
				'translation_domain' => 'messages'
			))
			->add('maho', CheckboxType::class)
			->add('officiel', CheckboxType::class, array(
				'required' => false,
				'label' => 'Official',
				'translation_domain' => 'messages'
			))
			->add('augmentations', TextareaType::class)
			->add('description', TextareaType::class)
			
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save btn'),
				'label' => 'Enregistrer',
				'label' => 'form.save',
				'translation_domain' => 'messages'
			))
			->add('annuler', ButtonType::class, array(
				'attr' => array(
					'class' => 'annuler btn col-md-offset-1 editer-gm',
					'data-id' => 'new',
					'data-edit' => 'Sort'
				),
				'label' => 'Annuler',
				'label' => 'form.cancel',
				'translation_domain' => 'messages'					
			))
			;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Sort::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'l5a_sort';
    }


}
