<?php

namespace App\Form\L5A;

use App\Entity\L5A\Competence;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompetenceType extends AbstractType {
	/**
	 * {@inheritdoc}
	 */
	public function buildForm(FormBuilderInterface $builder, array $options) {
		$builder
			->add('nom', TextType::class, array(
				'required' => true,
				'label' => 'Nom',
				'translation_domain' => 'messages'
			))
			->add('type')
			->add('description')
			->add('effet', TextType::class, array(
				'required' => false,
				'label' => 'skill.effect',
				'translation_domain' => 'messages'
			))
			->add('trait', TextType::class, array(
				'required' => true,
				'label' => 'skill.trait',
				'translation_domain' => 'messages'
			))
			->add('sousTypes')
			->add('specialisations')
			->add('capaciteMaitrise1Description', TextareaType::class)
			->add('capaciteMaitrise1Effet')
			->add('capaciteMaitrise2Description', TextareaType::class)
			->add('capaciteMaitrise2Effet')
			->add('capaciteMaitrise3Description', TextareaType::class)
			->add('capaciteMaitrise3Effet')
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save btn'),
				'label' => 'Créer !')
			)
		;
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(
			array(
				'data_class' => Competence::class
			)
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getBlockPrefix() {
		return 'l5a_competence';
	}


}
