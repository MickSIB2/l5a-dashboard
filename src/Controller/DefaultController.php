<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    	var_dump('blue');
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

	/**
	 * @param Request $request
	 * @param $locale fr or en
	 */
    public function changeLocaleAction(Request $request, $_locale) {
    	if ($_locale !== 'en' && $_locale !== 'fr') {
		    return;
	    }

	    //$request->setLocale($_locale);
	    $request->getSession()->set('_locale', $_locale);
	    //header("Refresh:0");
	    //header('Location: '.$_SERVER['PHP_SELF']);
	    return $this->redirectToRoute('homepage');
    }
}
