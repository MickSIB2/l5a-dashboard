<?php

namespace App\Controller\L5A;

use App\Entity\L5A\Arme;
use App\Entity\L5A\ArmePerso;
use App\Entity\L5A\Armure;
use App\Entity\L5A\AvantageXSamurai;
use App\Entity\L5A\DesavantageXSamurai;
use App\Entity\L5A\Game;
use App\Entity\L5A\Personnage;
use App\Entity\L5A\PnjBonus;
use App\Entity\L5A\Samurai;
use App\Entity\L5A\Avantage;
use App\Entity\L5A\Desavantage;
use App\Entity\L5A\PartieXJoueur;
use App\Entity\L5A\Technique;
use App\Form\L5A\CompetenceType;
use App\Form\GameType;
use App\Form\L5A\PersonnageType;
use App\Form\L5A\ArmeType;
use App\Form\L5A\ArmureType;
use App\Form\L5A\AvantageType;
use App\Form\L5A\DesavantageType;
use App\Form\L5A\TechniqueType;
use App\Service\L5A\DetailsQuester;
use App\Service\SessionService;
use App\Service\MyLogger;
use App\Controller\FatherController;
use Doctrine\ORM\EntityManager;
use Pusher\Pusher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GameMasterController extends FatherController
{

    public function indexAction(TranslatorInterface $translator, SessionInterface $session, Request $request = null, $type = null, $id = null) {

	    $defaultEM = $this->getDoctrine()->getManager();
		$sessionService = SessionService::getInstance($session);
		$gameId = $this->getGameId();
		$user = $this->getUser();
	    $game = $defaultEM->getRepository(Game::class)->findOneBy(array('id' => $gameId));
		$sessionService->setIsGm($user, $game);
		$isGm = $sessionService->getIsGm();
		if (!$isGm) throw new Exception('Vous n\'êtes pas le maître de jeu de cette partie');		

	    // Si un type et un id sont définis, éditer le type (pnj, arme, armure...)
	    // PNJ Form
	    $perso = null;
	    $action = null;
	    $bonus = array();
	    if ($type !== null && $type === 'pnj') {
		    $perso = $defaultEM->getRepository(Personnage::class)->findOneBy(array('id' => $id));
		    if ($perso->getGameId() !== (int)$gameId) {
			    throw $this->createAccessDeniedException(
			    	$translator->trans('gm.unauthorized', array('%type%' => 'ce personnage'))
			    );
		    }
		    $action = $this->generateUrl('gm_editer_pnj');
		    $bonus = $defaultEM->getRepository(PnjBonus::class)->findBy(
			    array(
				    'idPnj' => $id,
			    )
		    );
	    } else {
	    	$perso = new Personnage();
	    	$action = $this->generateUrl('gm_creation_pnj');
	    }

	    $pnjForm = $this->createForm(PersonnageType::class, $perso, array(
		    'action' => $action,
	    ));
	    // Fin PNJ Form

	    // Arme Form
	    $arme = null;
	    $action = null;
	    if ($type !== null && $type === 'arme') {
		    $arme = $defaultEM->getRepository(Arme::class)->findOneBy(array('id' => $id));
		    if ($arme->getGameId() !== $gameId) {
			    throw $this->createAccessDeniedException(
			    	$translator->trans('gm.unauthorized', array('%type%' => 'cette arme'))
			    );
		    }
		    $action = $this->generateUrl('gm_editer_arme');
	    } else {
		    $arme = new Arme();
		    $action = $this->generateUrl('gm_creation_arme');
	    }

	    $armeForm = $this->createForm(ArmeType::class, $arme, array(
		    'action' => $action,
	    ));
	    // Fin Arme Form

	    // Armure Form
	    $armure = null;
	    $action = null;
	    if ($type !== null && $type === 'armure') {
		    $armure = $defaultEM->getRepository(Armure::class)->findOneBy(array('id' => $id));
		    if ($armure->getGameId() !== $gameId) {
		    	throw $this->createAccessDeniedException(
		    		$translator->trans('gm.unauthorized', array('%type%' => 'cette armure'))
		    	);
		    }
		    $action = $this->generateUrl('gm_editer_armure');
	    } else {
		    $armure = new Armure();
		    $action = $this->generateUrl('gm_creation_armure');
	    }

	    $armureForm = $this->createForm(ArmureType::class, $armure, array(
		    'action' => $action,
	    ));
	    // Fin Arme Form
		
		// Avantage Form
	    $avantage = null;
	    $action = null;
	    if ($type !== null && $type === 'avantage') {
		    $avantage = $defaultEM->getRepository(Avantage::class)->findOneBy(array('id' => $id));
		    if ($avantage->getGameId() !== $gameId) {
		    	throw $this->createAccessDeniedException(
		    		$translator->trans('gm.unauthorized', array('%type%' => 'cet avantage'))
		    	);
		    }
		    $action = $this->generateUrl('gm_editer_avantage');
	    } else {
		    $avantage = new Avantage();
		    $action = $this->generateUrl('gm_creation_avantage');
	    }

	    $avantageForm = $this->createForm(AvantageType::class, $avantage, array(
		    'action' => $action,
	    ));
	    // Fin Avantage Form
		
		// D2savantage Form
	    $desavantage = null;
	    $action = null;
	    if ($type !== null && $type === 'desavantage') {
	    	/** @var Desavantage $desavantage */
		    $desavantage = $defaultEM->getRepository(Desavantage::class)->findOneBy(array('id' => $id));
		    if ($desavantage->getGameId() !== $gameId) {
		    	throw $this->createAccessDeniedException(		
		    		$translator->trans('gm.unauthorized', array('%type%' => 'ce désavantage'))
		    	);
		    }
		    $action = $this->generateUrl('gm_editer_desavantage');
	    } else {
		    $desavantage = new Desavantage();
		    $action = $this->generateUrl('gm_creation_desavantage');
	    }

	    $desavantageForm = $this->createForm(DesavantageType::class, $desavantage, array(
		    'action' => $action,
	    ));
	    // Fin D2savantage Form

		// Technique Form
	    $technique = null;
	    $action = null;
	    if ($type !== null && $type === 'technique') {
	    	/** @var Technique $technique */
		    $technique = $defaultEM->getRepository(Technique::class)->findOneBy(array('id' => $id));
		    if ($technique->getGameId() !== $gameId) {
		    	throw $this->createAccessDeniedException(
		    		$translator->trans('gm.unauthorized', array('%type%' => 'cette technique'))
		    	);
		    }
		    $action = $this->generateUrl('gm_editer_technique');
	    } else {
		    $technique = new Technique();
		    $action = $this->generateUrl('gm_creation_technique');
	    }

	    $techniqueForm = $this->createForm(TechniqueType::class, $technique, array(
		    'action' => $action,
	    ));
	    // Fin Technique Form

	    /*$query = $defaultEM->createQuery(
		    'SELECT p.id, p.nom, p.notes, p.genereGm, p.clan
	    	FROM App\Entity\L5A\Personnage p
			INDEX BY p.id
			WHERE p.gameId = ' .$gameId. ''
	    );
	    $pnjs = $query->getResult();
	    $armes = $defaultEM->getRepository(Arme::class)->findBy(array('personnalisee' => 1, 'gameId' => $gameId));
	    $armures = $defaultEM->getRepository(Armure::class)->findBy(array('personnalisee' => 1, 'gameId' => $gameId));*/

		$query = $defaultEM->createQuery(
			'SELECT s.id, s.nom, s.xp, s.blessures, s.honneur, s.gloire, s.statut, s.souillure, s.reputation, s.joueur
			FROM App\Entity\L5A\Samurai s
			INDEX BY s.id
			WHERE s.gameId = ' .$gameId.''
		);
		$samurais =
			sizeof($query->getResult()) == 0 ?
				null:
				$query->getResult();

	    // Joueurs
	    $query = $defaultEM->createQuery(
		    'SELECT u.id, u.username
			FROM App\Entity\L5A\AppUser u, App\Entity\L5A\PartieXJoueur pxj
			INDEX BY u.id
			WHERE pxj.gameId = ' .$gameId.'
			AND pxj.joueurId = u.id'
	    );
	    $joueurs =
		    sizeof($query->getResult()) == 0 ?
			    null:
			    $query->getResult();
	    $pnjs = array();
	    $armes = array();
	    $armures = array();

	    return $this->render('main/l5a/master.html.twig', array(
		    'pnjForm' => $pnjForm->createView(),
		    'armeForm' => $armeForm->createView(),
		    'armureForm' => $armureForm->createView(),
			'samurais' => $samurais,
		    'game' => $game,
		    'bonus' => $bonus,
		    'joueurs' => $joueurs,
		    'gm' => true,
		    'type' => $type,
		    'avantageForm' => $avantageForm->createView(),
		    'desavantageForm' => $desavantageForm->createView(),
		    'techniqueForm' => $techniqueForm->createView(),
	    ));
    }

    public function creerNouvelleCompetenceAction(Request $request) {
	    if ($request != null) {
		    $defaultEM = $this->getDoctrine()->getManager();
		    $competenceForm = $this->createForm(CompetenceType::class);
		    $competenceForm->handleRequest($request);
		    if ($competenceForm->isValid()) {

			    $newCompetence = $competenceForm->getData();
			    $defaultEM->persist($newCompetence);
			    $defaultEM->flush();
			    return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
		    }

	    }
    }

    private function creerformPNJ ($pnj = null) {
    	if ($pnj === null) {
		    $pnj = new Personnage();
		    $route = $this->generateUrl('gm_creation_pnj');
	    } else {
		    $route = $this->generateUrl('gm_editer_pnj');
	    }
	    $pnjForm = $this->createForm(
		    PersonnageType::class,
		    $pnj,
		    array(
			    'action' => $route,
			    'method' => 'POST',
		    )
	    );
	    return $pnjForm;
    }

	/**
	 * @param $data // All fields from
	 * @param $pnjIdrequest
	 * @param $em
	 */
    private function setBonus($data, $pnjId, $em) {
	    $aBonus = array();
	    foreach($data as $key => $value) {
		    if (strpos($key, 'bonus') !== false) {
			    // rassembler les bonus entre eux
			    $aField = explode('_', $key);
			    $nb = $aField[2];
			    $type = $aField[1];
			    $aBonus[$nb][$type] = $value;
		    }
	    }

	    if (! empty($aBonus)) {
		    // Ajouter tous les bonus au pnj
		    foreach ($aBonus as $bonus) {
			    $newBonus = new PnjBonus();
			    $newBonus->setIdPnj($pnjId);
			    $newBonus->setType($bonus['Type']);
			    $newBonus->setNom($bonus['Nom']);
			    $newBonus->setDescription($bonus['Description']);
			    $em->persist($newBonus);
		    }
		    $em->flush();
	    }
    }

    public function creerPNJAction(Request $request) {
	    if ($request != null) {
		    $defaultEM = $this->getDoctrine()->getManager();
		    $pnjForm = $this->creerformPNJ();
		    $bonus = array();
		    $pnjForm->handleRequest($request);
		    if ($pnjForm->isValid()) {
			    $gameId = $this->getGameId();

			    /** @var Personnage $newPNJ */
			    $newPNJ = $pnjForm->getData();
			    $newPNJ->setGenereGm(1);
			    $newPNJ->setGameId($gameId);
			    $defaultEM->persist($newPNJ);
			    $defaultEM->flush();

			    $formAllData = $request->request->get('l5a_personnage');
			    $this->setBonus($formAllData, $newPNJ->getId(), $defaultEM);

			    return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
		    } else {
			    //			    if ($r->get('return-form')) {
			    $reponse = array();
			    $reponse['content'] = $this->renderView('blocks/l5a/_form_pnj.html.twig',array(
					    'pnjForm' => $pnjForm->createView(),
					    'bonus' => $bonus,
				    )
			    );

			    return new JsonResponse($reponse);

		    }
	    }
	    return $this->createAccessDeniedException('Dead end O|__|O');
    }

	public function editerPNJAction(Request $request) {
		if ($request != null) {
			$defaultEM = $this->getDoctrine()->getManager();
			$r = $request->request;
			$reponse = array();
			$postData = $r->get('l5a_personnage');

			$id = ($r->get('id') !== null)
				? $r->get('id')
				: $postData['id'];

			$bonus = $defaultEM->getRepository(PnjBonus::class)->findBy(
				array(
					'idPnj' => $id,
				)
			);

			$pnj = $defaultEM->getRepository(Personnage::class)->find($id);
			$pnjForm = $this->creerformPNJ($pnj);
			try {
				$pnjForm->handleRequest ($request);
			} catch (Exception $e) {
				var_dump('Caught exception: ',  $e->getMessage(), "\n");
			}

			if ($pnjForm->isValid()) {
				$this->setBonus($request->request->get('l5a_personnage'), $pnj->getId(), $defaultEM);
				$defaultEM->merge($pnj);
				$defaultEM->flush();

				return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
			} else {

				$reponse['content'] = $this->renderView('blocks/l5a/_form_pnj.html.twig', array(
					'pnjForm' => $pnjForm->createView(),
					'bonus' => $bonus,
					)
				);
			}
			return new JsonResponse($reponse);
		}
		return $this->createAccessDeniedException('Dead end O|__|O');
	}

    public function creerArmeAction(Request $request) {
	    if ($request != null) {
		    $defaultEM = $this->getDoctrine()->getManager();
		    $r = $request->request;
			$gameId = $this->getGameId();
			$nouvelleArme = new Arme();
		    $armeForm = $this->createForm(ArmeType::class, $nouvelleArme, array(
			    'action' => $this->generateUrl('gm_creation_arme'),
			    'method' => 'POST',
		    ));
		    $armeForm->handleRequest($request);
		    if ($armeForm->isValid()) {

		    	/** @var Arme $nouvelleArme */
			    $nouvelleArme = $armeForm->getData();
			    $nouvelleArme->setPersonnalisee(1);
				$nouvelleArme->setGameId($gameId);
			    $defaultEM->persist($nouvelleArme);
			    $defaultEM->flush();
			    return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
		    }
			else {
				if ($r->get('return-form')) {
					$reponse = array();
					$reponse['content'] = $this->renderView('blocks/l5a/_form_arme.html.twig', array(
							'armeForm' => $armeForm->createView())
					);
					return new JsonResponse($reponse);
				} else {
					return $armeForm->getErrors();
				}
			}
	    }
	    return $this->createAccessDeniedException('Dead end O|__|O');
    }

	public function editerArmeAction(Request $request) {
		if ($request != null) {
			$defaultEM = $this->getDoctrine()->getManager();
			$r = $request->request;
			$reponse = array();
			$postData = $r->get('l5a_arme');

			$id = ($r->get('id') !== null)
				? $r->get('id')
				: $postData['id'];
			$arme = $defaultEM->getRepository(Arme::class)->find($id);
			$armeForm = $this->createForm(ArmeType::class, $arme, array(
				'action' => $this->generateUrl('gm_editer_arme'),
			));
			$armeForm->handleRequest($request);

			if ($armeForm->isSubmitted() && $armeForm->isValid()) {
				$defaultEM->merge($arme);
				$defaultEM->flush();
				return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
			} else {
				$reponse['content'] = $this->renderView('blocks/l5a/_form_arme.html.twig', array(
					'armeForm' => $armeForm->createView())
				);
			}
			return new JsonResponse($reponse);
		}
		return $this->createAccessDeniedException('Dead end O|__|O');
	}

    public function creerAvantageAction(Request $request) {
	    if ($request != null) {
		    $defaultEM = $this->getDoctrine()->getManager();
		    $r = $request->request;
			$gameId = $this->getGameId();
			$nouvelAvantage = new Avantage();
		    $avantageForm = $this->createForm(AvantageType::class, $nouvelAvantage, array(
			    'action' => $this->generateUrl('gm_creation_avantage'),
			    'method' => 'POST',
		    ));
		    $avantageForm->handleRequest($request);
		    if ($avantageForm->isValid()) {

		    	/** @var Arme $nouvelAvantage */
			    $nouvelAvantage = $avantageForm->getData();
			    $nouvelAvantage->setPersonnalise(1);
				$nouvelAvantage->setGameId($gameId);
			    $defaultEM->persist($nouvelAvantage);
			    $defaultEM->flush();
			    return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
		    }
			else {
				if ($r->get('return-form')) {
					$reponse = array();
					$reponse['content'] = $this->renderView('blocks/l5a/_form_avantage.html.twig', array(
							'avantageForm' => $avantageForm->createView())
					);
					return new JsonResponse($reponse);
				} else {
					return $avantageForm->getErrors();
				}
			}
	    }
	    return $this->createAccessDeniedException('Dead end O|__|O');
    }

	public function editerAvantageAction(Request $request) {
		if ($request != null) {
			$defaultEM = $this->getDoctrine()->getManager();
			$r = $request->request;
			$reponse = array();
			$postData = $r->get('l5a_avantage');

			$id = ($r->get('id') !== null)
				? $r->get('id')
				: $postData['id'];
			$avantage = $defaultEM->getRepository(Avantage::class)->find($id);
			$avantageForm = $this->createForm(AvantageType::class, $avantage, array(
				'action' => $this->generateUrl('gm_editer_avantage'),
			));
			$avantageForm->handleRequest($request);

			if ($avantageForm->isSubmitted() && $avantageForm->isValid()) {
				$defaultEM->merge($avantage);
				$defaultEM->flush();
				return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
			} else {
				$reponse['content'] = $this->renderView('blocks/l5a/_form_avantage.html.twig', array(
					'avantageForm' => $avantageForm->createView())
				);
			}
			return new JsonResponse($reponse);
		}
		return $this->createAccessDeniedException('Dead end O|__|O');
	}

    public function creerDesavantageAction(Request $request) {
	    if ($request != null) {
		    $defaultEM = $this->getDoctrine()->getManager();
		    $r = $request->request;
			$gameId = $this->getGameId();
			$nouveauDesavantage = new Desavantage();
		    $desavantageForm = $this->createForm(DesavantageType::class, $nouveauDesavantage, array(
			    'action' => $this->generateUrl('gm_creation_desavantage'),
			    'method' => 'POST',
		    ));
		    $desavantageForm->handleRequest($request);
		    if ($desavantageForm->isValid()) {

		    	/** @var Arme $nouvelAvantage */
			    $nouveauDesavantage = $desavantageForm->getData();
			    $nouveauDesavantage->setPersonnalise(1);
				$nouveauDesavantage->setGameId($gameId);
			    $defaultEM->persist($nouveauDesavantage);
			    $defaultEM->flush();
			    return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
		    }
			else {
				if ($r->get('return-form')) {
					$reponse = array();
					$reponse['content'] = $this->renderView('blocks/l5a/_form_desavantage.html.twig', array(
							'desavantageForm' => $desavantageForm->createView())
					);
					return new JsonResponse($reponse);
				} else {
					return $desavantageForm->getErrors();
				}
			}
	    }
	    return $this->createAccessDeniedException('Dead end O|__|O');
    }

	public function editerDesavantageAction(Request $request) {
		if ($request != null) {
			$defaultEM = $this->getDoctrine()->getManager();
			$r = $request->request;
			$reponse = array();
			$postData = $r->get('l5a_desavantage');

			$id = ($r->get('id') !== null)
				? $r->get('id')
				: $postData['id'];
			$desavantage = $defaultEM->getRepository(Desavantage::class)->find($id);
			$desavantageForm = $this->createForm(DesavantageType::class, $desavantage, array(
				'action' => $this->generateUrl('gm_editer_desavantage'),
			));
			$desavantageForm->handleRequest($request);

			if ($desavantageForm->isSubmitted() && $desavantageForm->isValid()) {
				$defaultEM->merge($desavantage);
				$defaultEM->flush();
				return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
			} else {
				$reponse['content'] = $this->renderView('blocks/l5a/_form_desavantage.html.twig', array(
					'desavantageForm' => $desavantageForm->createView())
				);
			}
			return new JsonResponse($reponse);
		}
		return $this->createAccessDeniedException('Dead end O|__|O');
	}

    public function creerArmureAction(Request $request) {
	    if ($request != null) {
		    $r = $request->request;
		    $defaultEM = $this->getDoctrine()->getManager();
			$gameId = $this->getGameId();
			$nouvelleArmure = new Armure();
		    $armureForm = $this->createForm(ArmureType::class, $nouvelleArmure, array(
			    'action' => $this->generateUrl('gm_creation_armure'),
			    'method' => 'POST',
		    ));
		    $armureForm->handleRequest($request);
		    if ($armureForm->isValid()) {

		    	/** @var Armure $nouvelleArmure */
			    $nouvelleArmure = $armureForm->getData();
			    $nouvelleArmure->setPersonnalisee(1);
			    $nouvelleArmure->setGameId($gameId);
			    $defaultEM->persist($nouvelleArmure);
			    $defaultEM->flush();
			    return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
		    }
			else {
		    	if ($r->get('return-form')) {
				    $reponse = array();
				    $reponse['content'] = $this->renderView('blocks/l5a/_form_armure.html.twig', array(
						    'armureForm' => $armureForm->createView())
				    );
				    return new JsonResponse($reponse);
			    } else {
				    return $armureForm->getErrors();
			    }
			}
	    }
	    return $this->createAccessDeniedException('Dead end O|__|O');
    }

	public function editerArmureAction(Request $request) {
		if ($request != null) {
			$defaultEM = $this->getDoctrine()->getManager();
			$r = $request->request;
			$reponse = array();
			$postData = $r->get('l5a_armure');

			$id = ($r->get('id') !== null)
				? $r->get('id')
				: $postData['id'];

			$armure = $defaultEM->getRepository(Armure::class)->find($id);
			$armureForm = $this->createForm(ArmureType::class, $armure, array(
				'action' => $this->generateUrl('gm_editer_armure'),
			));
			$armureForm->handleRequest($request);

			if ($armureForm->isSubmitted() && $armureForm->isValid()) {
				$defaultEM->merge($armure);
				$defaultEM->flush();
				return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
			} else {
				$reponse['content'] = $this->renderView('blocks/l5a/_form_armure.html.twig', array(
					'armureForm' => $armureForm->createView())
				);
			}
			return new JsonResponse($reponse);
		}
		return $this->createAccessDeniedException('Dead end O|__|O');
	}

    public function creerTechniqueAction(Request $request) {
	    if ($request != null) {
		    $r = $request->request;
		    $defaultEM = $this->getDoctrine()->getManager();
			$gameId = $this->getGameId();
			$nouvelleTechnique = new Technique();
		    $techniqueForm = $this->createForm(TechniqueType::class, $nouvelleTechnique, array(
			    'action' => $this->generateUrl('gm_creation_technique'),
			    'method' => 'POST',
		    ));
		    $techniqueForm->handleRequest($request);
		    if ($techniqueForm->isValid()) {
		    	/** @var Armure $nouvelleArmure */
			    $nouvelleTechnique = $techniqueForm->getData();
			    $nouvelleTechnique->setPersonnalisee(1);
			    $nouvelleTechnique->setGameId($gameId);
			    $defaultEM->persist($nouvelleTechnique);
			    $defaultEM->flush();
			    return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
		    }
			else {
		    	if ($r->get('return-form')) {
				    $reponse = array();
				    $reponse['content'] = $this->renderView('blocks/l5a/_form_technique.html.twig', array(
						    'techniqueForm' => $techniqueForm->createView())
				    );
				    return new JsonResponse($reponse);
			    } else {
				    return $techniqueForm->getErrors();
			    }
			}
	    }
	    return $this->createAccessDeniedException('Dead end O|__|O');
    }

	public function editerTechniqueAction(Request $request) {
		if ($request != null) {
			$defaultEM = $this->getDoctrine()->getManager();
			$r = $request->request;
			$reponse = array();
			$postData = $r->get('l5a_technique');

			$id = ($r->get('id') !== null)
				? $r->get('id')
				: $postData['id'];

			$technique = $defaultEM->getRepository(Technique::class)->find($id);
			$techniqueForm = $this->createForm(TechniqueType::class, $technique, array(
				'action' => $this->generateUrl('gm_editer_technique'),
			));
			$techniqueForm->handleRequest($request);

			if ($techniqueForm->isSubmitted() && $techniqueForm->isValid()) {
				$defaultEM->merge($technique);
				$defaultEM->flush();
				return $this->redirectToRoute('master_create', array('_locale' => $request->getLocale()));
			} else {
				$reponse['content'] = $this->renderView('blocks/l5a/_form_technique.html.twig', array(
					'techniqueForm' => $techniqueForm->createView())
				);
			}
			return new JsonResponse($reponse);
		}
		return $this->createAccessDeniedException('Dead end O|__|O');
	}

	public function listeSamuraisAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$gameId = $this->getGameId();
		$query = $defaultEM->createQuery(
			'SELECT s.id, s.nom, s.xp, s.blessures, s.honneur, s.gloire, s.statut, s.souillure, s.reputation, s.joueur
			FROM App\Entity\L5A\Samurai s
			INDEX BY s.id
			WHERE s.gameId = ' .$gameId. ''
		);
		$samurais = $query->getResult();

		// Joueurs
		$query = $defaultEM->createQuery(
			'SELECT u.id, u.username
			FROM App\Entity\L5A\AppUser u, App\Entity\L5A\PartieXJoueur pxj
			INDEX BY u.id
			WHERE pxj.gameId = ' .$gameId.'
			AND pxj.joueurId = u.id'
		);
		$joueurs =
			sizeof($query->getResult()) == 0 ?
				null:
				$query->getResult();
		$data = array(
			'success' => true,
		);
		$data['html'] = $this->renderView('blocks/l5a/_liste_samurais.html.twig', array(
			'samurais' => $samurais,
			'joueurs' => $joueurs,
		));

		return new JsonResponse($data);
	}

	public function attribuerXPAction(Request $request) {
    	/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;

		$joueurs = $r->get('joueurs');
		$valeur = $r->get('valeur');
		foreach($joueurs as $joueur) {
			$samurai = $defaultEM->getRepository(Samurai::class)->findOneBy(array('id' => $joueur));
			$detailQuester = new DetailsQuester($defaultEM, $samurai);
			$detailQuester->setXPRestant('+', $valeur);
			$samurai->setXp($samurai->getXp() + $valeur);
			$defaultEM->merge($samurai);
		}
		$defaultEM->flush();

		return $this->listeSamuraisAction($request);
	}

	public function attribuerEquipementAction(Request $request) {

		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;

		$samuraiId = $r->get('samurai_id');
		$type = $r->get('type');
		$nom = $r->get('nom');
		$id = $r->get('valeur');

		$samurai = $defaultEM->getRepository(Samurai::class)->findOneBy(array('id' => $samuraiId));
		$detailQuester = new DetailsQuester($defaultEM, $samurai);

		// Ajouter arme
		if ($type === 'arme') {
			$detailQuester->creerArmePerso($nom, $id);
		}
		// Ajouter armure
		if ($type === 'armure') {
			$detailQuester->creerItem($nom, $id);
		}
//		$defaultEM->merge($samurai);
//		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	public function creerPartieAction(Request $request) {

		$defaultEM = $this->getDoctrine()->getManager();
		$newGame = new Game();
		$form = $this->createForm(GameType::class, $newGame);
		$form->handleRequest($request);
		if ($form->isValid()) {

			//Créer la partie
			$user = $this->getUser();
			$newGame = $form->getData();
			$defaultEM->persist($newGame);
			$defaultEM->flush();

			// Faire du créateur le gamemaster

			$partieXJoueur = new PartieXJoueur();
			$partieXJoueur->setGameId($newGame->getId());
			$partieXJoueur->setJoueurId($user->getId());
			$defaultEM->merge($partieXJoueur);
			
			$user->addRole('ROLE_GM');
			$newGame->setGamemaster($user->getId());

			$defaultEM->merge($user);
			$defaultEM->merge($newGame);
			$defaultEM->flush();

			return $this->redirectToRoute('homepage');
		}
		else {
			die('game not created');
		}
	}

	public function selectPNJAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;
		$pnjId = $r->get('id');
//		$pnj = $defaultEM->getRepository(Personnage::class)->findOneBy(array('id' => $pnjId));
		$query = $defaultEM->createQuery('SELECT p
		FROM App\Entity\L5A\Personnage p
		WHERE p.id = '. $pnjId . ''
		);
		$pnj = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];

		$query = $defaultEM->createQuery('SELECT bonus
		FROM App\Entity\L5A\PnjBonus bonus
		WHERE bonus.idPnj = '. $pnjId . ''
		);
		$bonus = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
//		$bonus = !empty($bonus) ? $bonus : $bonus;

		return new JsonResponse(array(
			'success' => true,
			'pnj' => $pnj,
			'bonus' => $bonus,
		));
	}

	public function majCombatPJAction(Request $request, Pusher $pusher) {
		$r = $request->request;

		$logger = new MyLogger();
		$data = array();
		$data['content'] = $r->get('html');
		$game = $this->getGameId();
//		$pusher->set_logger($logger);
		$pusher->trigger('game-'.$game, 'maj-combat', $r->get('html'));

		return new JsonResponse(
			array(
				'success' => true,
				'message' => $pusher->get_channel_info('game-'.$game),
			)
		);
	}

	public function majNotesGMAction(Request $request){
		$defaultEM = $this->getDoctrine()->getManager();
		$game =
			$defaultEM->getRepository(Game::class)->findOneBy(
				array('id' => $this->getGameId())
			);
		$game->setNotes($request->request->get('valeur'));
		$defaultEM->merge($game);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	public function getBonusAction(TranslatorInterface $translator, Request $request){
    	/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		$detailsQuester = new DetailsQuester($defaultEM, null, $this->getGameId(), $translator);
		$r = $request->request;
		$typeValeur = $r->get('type_valeur');
		$valeur = $r->get('valeur');

		$content = $detailsQuester->getStuff($typeValeur, $valeur, true);

		return new JsonResponse(array(
			'success' => true,
			'champ' => $content[0],
			'content' => $content[1],
		));
	}

	public function attribuerDesAvantageAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;

		$type = ucfirst($r->get('type'));
		$id = $r->get('id');
		$samuraiId = $r->get('samurai_id');
		$valeur = $r->get('valeur');

		/** @var Samurai $samurai */
		$samurai = $defaultEM->getRepository(Samurai::class)->find($samuraiId);
		$detailQuester = new DetailsQuester($defaultEM, $samurai);

		if ($type == "Avantage") {

			$avantages = $detailQuester->getAvantages(false);
			if (in_array($id, $avantages)) {
				$avantagePerso = $defaultEM->getRepository(AvantageXSamurai::class)->findBy(
					array(
						'avantageId' => $id,
						'samuraiId' => $samuraiId,
					))[0];
				$defaultEM->remove($avantagePerso);
			} else {
				$sql = $defaultEM->createQuery("SELECT a.nom FROM App\Entity\L5A\Avantage a WHERE a.id = $id");
				$nom = $sql->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0]['nom'];
				$newAvantagePerso = new AvantageXSamurai();
				$newAvantagePerso->setAvantageId($id);
				$newAvantagePerso->setSamuraiId($samuraiId);
				$detailQuester->setXPRestant('-', $valeur, 'l\'avantage', $nom);
				$defaultEM->persist($newAvantagePerso);
			}

		} else {

			$desavantages = $detailQuester->getDesavantages(false);
			if (in_array($id, $desavantages)) {
				$desavantagePerso = $defaultEM->getRepository(DesavantageXSamurai::class)->findBy(
					array(
						'desavantageId' => $id,
						'samuraiId' => $samuraiId,
					))[0];
				$defaultEM->remove($desavantagePerso);
			} else {
				$sql = $defaultEM->createQuery("SELECT a.nom FROM App\Entity\L5A\Desavantage a WHERE a.id = $id");
				$nom = $sql->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0]['nom'];
				$newDesavantagePerso = new DesavantageXSamurai();
				$newDesavantagePerso->setDesavantageId($id);
				$newDesavantagePerso->setSamuraiId($samuraiId);
				$detailQuester->setXPRestant('+', $valeur, 'le désavantage', $nom);
				$defaultEM->persist($newDesavantagePerso);
			}
		}
		$defaultEM->merge($samurai);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
			'xp_restant' => $samurai->getXpRestant(),
		));
	}

	function supprimerArmeGMAction(Request $request) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		$armeId = $r->get('id');
		$arme = $defaultEM->getRepository(Arme::class)->find($armeId);
		$defaultEM->remove($arme);

		// Enlever les armes perso
		$armesPerso = $defaultEM->getRepository(ArmePerso::class)->findBy(array('armeId' => $armeId));
		foreach ($armesPerso as $oneArmePerso) {
			$defaultEM->remove($oneArmePerso);
		}

		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function supprimerArmureGMAction(Request $request) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		$armureId = $r->get('id');
		$armure = $defaultEM->getRepository(Armure::class)->find($armureId);
		$defaultEM->remove($armure);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function supprimerAvantageGMAction(Request $request) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		$avantageId = $r->get('id');
		$avantage = $defaultEM->getRepository(Avantage::class)->find($avantageId);
		$defaultEM->remove($avantage);

		// Éliminer les liaisons aux samurai
		$avantagesPerso = $defaultEM->getRepository(AvantageXSamurai::class)->findBy(array('avantageId' => $avantageId));
		if (! empty($avantagesPerso)) {
			foreach ($avantagesPerso as $oneAvantagePerso) {
				$defaultEM->remove($oneAvantagePerso);
			}
		}

		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function supprimerDesavantageGMAction(Request $request) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		$desavantageId = $r->get('id');
		$desavantage = $defaultEM->getRepository(Desavantage::class)->find($desavantageId);
		$defaultEM->remove($desavantage);

		// Éliminer les liaisons aux samurai
		$desavantagesPerso = $defaultEM->getRepository(DesavantageXSamurai::class)->findBy(array('desavantageId' => $desavantageId));
		if (! empty($desavantagesPerso)) {
			foreach ($desavantagesPerso as $oneDesavantagePerso) {
				$defaultEM->remove($oneDesavantagePerso);
			}
		}

		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function supprimerTechniqueGMAction(Request $request, TranslatorInterface $translator) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		$techniqueId = $r->get('id');
		$supprimer = true;
		$continuerLoop = true;
		$samuraiAvecTechnique = '';
		// Chercher chez les samurai pour voir si l'un d'entre eux utilise la technique. Dans ce cas, annuler
		//la suppression

		$samurais = $defaultEM->getRepository(Samurai::class)->findByGameId($this->getGameId());

		/** @var Samurai $oneSamurai */
		foreach ($samurais as $oneSamurai) {
			if ($continuerLoop) {
				if ($oneSamurai->getTechnique1() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
				if ($oneSamurai->getTechnique2() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
				if ($oneSamurai->getTechnique3() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
				if ($oneSamurai->getTechnique4() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
				if ($oneSamurai->getTechnique5() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
				if ($oneSamurai->getTechnique6() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
				if ($oneSamurai->getTechnique7() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
				if ($oneSamurai->getTechnique8() == $techniqueId) {
					$samuraiAvecTechnique = $oneSamurai->getNom();
					$supprimer = false;
					$continuerLoop = false;
				}
			}
		}

		// Si la technique est utilisée par un joueur
		if (!$supprimer) {
			return new JsonResponse(array(
				'error' => true,
				'success' => false,
				'message' => $translator->trans('gm.cant_delete_technic', array(
					'%samuraiAvecTechnique%' => $samuraiAvecTechnique
				)),
				'status' => array(
					'type' => 'error',
					'value' => 'Valid JSON value found',
				)
			));
		}

		$technique = $defaultEM->getRepository(Technique::class)->find($techniqueId);
		$defaultEM->remove($technique);

		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function attribuerJoueurAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;
		$samuraiId = $r->get('samurai_id');
		$samurai = $defaultEM->getRepository(Samurai::class)->find($samuraiId);
		$joueur = $r->get('nouveau_joueur');
		$samurai->setJoueur($joueur);

		$defaultEM->merge($samurai);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	/**
	 * @param array $haystack array selectionné
	 * @param $key false si la clé n'a pas de nom, string ou int sinon
	 * @return $value valeur du tableau retournée
	 */
	private function array_deep_dig($haystack, $needle_key, $needle_value) {
		foreach($haystack as $key=>$value){
			if($key == $needle_key && $value == $needle_value) return $haystack;
			if(is_array($value)){
				if( ($result = $this->array_deep_dig($value, $needle_key, $needle_value)) !== false)
					return $result;
			}
		}
		return false;
	}

	public function afficherListeAction(Request $request) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();
		$gameID = $r->get('game_id');
		$type = $r->get('type');
		$view = null;

		switch ($type) {
			case 'armes':
				$armes = $defaultEM->getRepository(Arme::class)->findBy(array(
					'gameId' => $gameID,
					'personnalisee' => 1,
				));
				$samurais = $defaultEM->getRepository(Samurai::class)->findBy(array('gameId' => $gameID));
				$view = $this->renderView('blocks/l5a/_liste_armes.html.twig', array(
					'armes' => $armes,
					'samurais' => $samurais,
				));
				break;
			case 'armures':
				$armures = $defaultEM->getRepository(Armure::class)->findBy(array(
					'gameId' => $gameID,
					'personnalisee' => 1,
				));
				$samurais = $defaultEM->getRepository(Samurai::class)->findBy(array('gameId' => $gameID));
				$view = $this->renderView('blocks/l5a/_liste_armures.html.twig', array(
					'armures' => $armures,
					'samurais' => $samurais,
				));
				break;
			case 'avantages':
				$avantages = $defaultEM->getRepository(Avantage::class)->findBy(array(
					'gameId' => $gameID,
					'personnalise' => 1,
				));
				$samurais = $defaultEM->getRepository(Samurai::class)->findBy(array('gameId' => $gameID));
				$view = $this->renderView('blocks/l5a/_liste_avantages.html.twig', array(
					'avantages' => $avantages,
					'samurais' => $samurais,
				));
				break;
			case 'desavantages':
				$desavantages = $defaultEM->getRepository(Desavantage::class)->findBy(array(
					'gameId' => $gameID,
					'personnalise' => 1,
				));
				$samurais = $defaultEM->getRepository(Samurai::class)->findBy(array('gameId' => $gameID));
				$view = $this->renderView('blocks/l5a/_liste_desavantages.html.twig', array(
					'desavantages' => $desavantages,
					'samurais' => $samurais,
				));
				break;
			case 'techniques':
				$techniques = $defaultEM->getRepository(Technique::class)->findBy(array(
					'gameId' => $gameID,
					'personnalisee' => 1,
				));
				$samurais = $defaultEM->getRepository(Samurai::class)->findBy(array('gameId' => $gameID));
				$view = $this->renderView('blocks/l5a/_liste_techniques.html.twig', array(
					'techniques' => $techniques,
					'samurais' => $samurais,
				));
				break;
			case 'pnjs':
			default:
				$query = $defaultEM->createQuery(
					'SELECT p.id, p.nom, p.notes, p.genereGm, p.clan, p.rangMaitrise
			        FROM App\Entity\L5A\Personnage p 
					INDEX BY p.id
					WHERE p.gameId = ' .$gameID. ' AND p.genereGm = 1'
				);
				$pnjs = $query->getResult();
				$view = $this->renderView('blocks/l5a/_liste_pnjs.html.twig', array(
					'pnjs' => $pnjs,
				));
				break;
		}

		return new JsonResponse(array(
			'success' => true,
			'html' => $view,
		));
	}

	public function gmEditerSamuraiAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;
		$samuraiId = $r->get('samurai_id');
		$samurai = $defaultEM->getRepository(Samurai::class)->find($samuraiId);

		if ($samurai->getGameId() !== (int)$this->getGameId()) { // Pas le bon samurai
			return new JsonResponse(array(
				'failure' => true,
				'success' => false,
				'message' => 'Vous n\'avez pas le droit de modifier ce samurai.'
			));
		}

		foreach ($r as $field => $value) {
			if ($field !== 'samurai_id' && $value !== 0) {
				$functionSet = 'set' . ucfirst($field);
				$functionGet = 'get' . ucfirst($field);
				$limiteMin = 0;

				$anneaux = ['terre', 'air', 'eau', 'feu', 'vide'];
				if ($field === 'gloire') {
					$limiteMin = -10;
				} else if (in_array($field, $anneaux)) {
					$limiteMin = 1;
				}

				if ($field === 'gloire' || $field === 'honneur' || $field === 'statut') {
					$newValue = $samurai->$functionGet() + $value / 10;
					if ($newValue > 10) { // Honneur ou gloire hors des limites
						return new JsonResponse(array(
							'failure' => true,
							'success' => false,
						));
					}
				} else {
					$oldValue = $samurai->$functionGet();
					$newValue = $oldValue + $value;
					// SI le MJ peut réduire les traits avec cet outils, il devrait pouvoir aussi les augmenter,
					// et je suis pas sûr que ça vaille la peine.

//					if ($newValue < $oldValue && in_array($field, $anneaux)) {
//						switch ($field) {
//							case 'terre':
//								$samurai->setVolonte($newValue);
//								$samurai->setConstitution($newValue);
//								break;
//							case 'air':
//								$samurai->setReflexes($newValue);
//								$samurai->setConstitution($newValue);
//								break;
//							case 'terre':
//								$samurai->setVolonte($newValue);
//								$samurai->setConstitution($newValue);
//								break;
//							case 'terre':
//								$samurai->setVolonte($limiteMin);
//								$samurai->setConstitution($limiteMin);
//								break;
//							case 'terre':
//								$samurai->setVolonte($limiteMin);
//								$samurai->setConstitution($limiteMin);
//								break;
//						}
//					}
				}
				if ($newValue < $limiteMin) {
					$newValue = $limiteMin;
				}
				$samurai->$functionSet($newValue);
			}
		}
		$defaultEM->merge($samurai);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}
}
