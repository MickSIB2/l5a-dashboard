<?php

namespace App\Controller\L5A;

use App\Entity\L5A\AppUser;
use App\Entity\L5A\Arme;
use App\Entity\L5A\Armure;
use App\Entity\L5A\Avantage;
use App\Entity\L5A\Clan;
use App\Entity\L5A\Competence;
use App\Entity\L5A\Coordonnees;
use App\Entity\L5A\Desavantage;
use App\Entity\L5A\Ecole;
use App\Entity\L5A\Famille;
use App\Entity\L5A\Fleche;
use App\Entity\L5A\HistoriqueXp;
use App\Entity\L5A\Kata;
use App\Entity\L5A\Samurai;
use App\Entity\L5A\SamuraiXrefSkill;
use App\Entity\L5A\Sort;
use App\Entity\L5A\Technique;
use App\Entity\L5A\Game;
use App\Entity\L5A\PartieXJoueur;
use App\Form\GameType;
use App\Service\L5A\DetailsQuester;
use App\Service\SessionService;
use App\Service\MyLogger;
use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\EntityManager;
use Swift_Mailer;
use Swift_SmtpTransport;
use App\Controller\FatherController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Translation\TranslatorInterface;

class L5AController extends FatherController
{

	/**
	 * Page Bienvenue
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function accueilAction(Request $request) {
		return $this->render('main/accueil.html.twig');
	}

	public function indexRedirectAction(Request $request) {
		return $this->redirectToRoute('homepage', (['_locale' => 'fr']));
	}
	public function samuraiRedirectAction(Request $request, $id) {
		return $this->redirectToRoute('samurai', (['_locale' => 'fr', 'id' => $id]));
	}

	public function accueilRedirectAction(Request $request) {
		return $this->redirectToRoute('accueil', (['_locale' => 'fr']));
	}

    public function indexAction(SessionInterface $session, Request $request)
    {
    	$defaultEM = $this->getDoctrine()->getManager();
    	/** @var AppUser $user */
	    $user = $this->getUser();
		$gamesXUser = $defaultEM->getRepository(PartieXJoueur::class)->findByJoueurId($user->getId());
		$games = array();
		// Remove isGm
		$sessionService = SessionService::getInstance($session);
		$sessionService->removeIsGm();

		/** @var PartieXJoueur $oneGameXUser */
	    foreach ($gamesXUser as $oneGameXUser) {
			$games[] = $defaultEM->getRepository(PartieXJoueur::class)
			                     ->getNbJoueurs($defaultEM, $oneGameXUser->getGameId());
		}

        return $this->render('main/l5a/index.html.twig', array(
			'games' => $games,
        ));
    }
    
    public function partieAction(Request $request, SessionInterface $session, $gameId =  null) {

    	$gameForm = null;
    	$isNew = false;
    	$game = null;

    	/** @var EntityManager $defaultEM */
    	$defaultEM = $this->getDoctrine()->getManager();
    	$user = $this->getUser();

    	// Si le mot passe n'est pas bon, le redemander :
    	if ($gameId !== null) {
    		$game = $defaultEM->getRepository(Game::class)->findOneBy(array('id' => $gameId));
    		if (isset($_POST['mdp'])) {
	    		$mdp = $_POST['mdp'];
	    		if ($game->getMotDePasse() !== $mdp) {
	    			return $this->redirectToRoute('rejoindre_partie', array('id' => $gameId));
	    		} else {
	    			$partieXJoueur = new PartieXJoueur();
	    			$partieXJoueur->setGameId($gameId)->setJoueurId($user->getId());
	    			$defaultEM->merge($partieXJoueur);
	    			$defaultEM->flush();
	    		}
    		}
			

    		/** @var array[Samurai] $allSamurai */
    		$allSamurai = $defaultEM->getRepository(Samurai::class)->findBy(array('gameId' => $gameId));
		    $aJoueurs = $defaultEM->getRepository(PartieXJoueur::class)->getJoueurs($defaultEM, $gameId);
			$sessionService = SessionService::getInstance($session);
    		$sessionService->setGameId($gameId);
			$sessionService->setIsGm($user, $game);
			$isGm = $sessionService->getIsGm();
    	} else {
	    	$allSamurai = array();
	    	$aJoueurs = array();
	    	$isNew = true;
	    	$newGame = new Game();
	    	$gameForm = $this->createForm(GameType::class, $newGame,
	    		array('action' => $this->generateUrl('creer_partie', array('_locale' => $request->get('_locale')))))->createView();
    	}

    	return $this->render('main/l5a/partie.html.twig', array(
    		'aJoueurs' => $aJoueurs,
    		'allSamurai' => $allSamurai,
    		'isNew' => $isNew,
    		'formGame' => $gameForm,
    		'game' => $game,
			'gm' => $isGm
    	));
    }

    public function rechercheAction(Request $request, $id = null) {
    	return false;
    }

    public function rechercheCompetencesAction(Request $request, $id = null) {
    	/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
    	$competences = $defaultEM->getRepository(Competence::class)->findAll();

	    $params = array(
		    'competences' => $competences,
	    );

	    if ($id !== 'gm') {
		    $samurai = $defaultEM->getRepository(Samurai::class)->find($id);
		    $detailQuester = new DetailsQuester($defaultEM, $samurai);
		    $competencesAcquises = $defaultEM->getRepository(SamuraiXrefSkill::class)
		                                     ->findBy(array('samuraiId' => $id));

		    // Si le personnage a été créé il y a plus d'une semaine, plus d'achat gratuit.
		    $firstXP = $defaultEM->getRepository(HistoriqueXp::class)->findOneBy(array('idSamurai' => $id), array('id' => 'ASC'));
		    $firstDate = date($firstXP->getDate());
		    $now = date("d-m-Y");
		    $oneWeekPassed = strtotime($now) - strtotime($firstDate) >= 604800;
		    if (!$oneWeekPassed) {
			    $params['created'] = 'newly';
		    }

		    $params['samurai'] = $samurai;
		    $params['competencesAcquises'] = $competencesAcquises;
	    } else {
		    $params['gm'] = true;
	    }

	    return $this->render('blocks/l5a/recherche_competences.html.twig', $params);
    }

    public function rechercheArmesAction(Request $request, $id = null) {
    	$defaultEM = $this->getDoctrine()->getManager();
    	$gameId = $this->getGameId($id);

	    $items = array();
	    $items['armes'] = $defaultEM->getRepository(Arme::class)->findByGameIdCustom($gameId);
	    $items['armures'] = $defaultEM->getRepository(Armure::class)->findByGameIdCustom($gameId);
	    $items['fleches'] = $defaultEM->getRepository(Fleche::class)->findAll();

	    $params = array(
		    'items' => $items,
	    );
	    if ($id !== 'gm') {
		    $samurai = $defaultEM->getRepository(Samurai::class)->find($id);
		    // Si le personnage a été créé il y a plus d'une semaine, plus d'achat gratuit.
		    $firstXP = $defaultEM->getRepository(HistoriqueXp::class)->findOneBy(array('idSamurai' => $id), array('id' => 'ASC'));
		    $firstDate = date($firstXP->getDate());
		    $now = date("d-m-Y");
		    $oneWeekPassed = strtotime($now) - strtotime($firstDate) >= 604800;
		    if (!$oneWeekPassed) {
			    $params['created'] = 'newly';
		    }
		    $params['samurai'] = $samurai;
	    } else {
		    $params['gm'] = true;
	    }

	    return $this->render('blocks/l5a/recherche_armes.html.twig', $params);
    }

    public function rechercheAnneauxAction(Request $request, $id = null) {
    	$defaultEM = $this->getDoctrine()->getManager();

    	$samurai = $defaultEM->getRepository(Samurai::class)->find($id);

	    return $this->render('blocks/l5a/recherche_anneaux.html.twig', array(
	    	'samurai' => $samurai,
	    ));
    }

    public function rechercheKatasAction(Request $request, $id = null) {
    	$defaultEM = $this->getDoctrine()->getManager();

	    $katas = $defaultEM->getRepository(Kata::class)->findAll();
	    $query = $defaultEM->createQuery(
	    	'SELECT e.id, e.nom
	    	FROM App\Entity\L5A\Ecole e
		  INDEX BY e.id'
	    );
	    $ecoles = $query->getResult();

	    $params = array(
		    'katas' => $katas,
		    'ecoles' => $ecoles,
	    );

	    if ($id !== 'gm') {
		    $samurai = $defaultEM->getRepository(Samurai::class)->find($id);
		    $params['samurai'] = $samurai;
	    } else {
		    $params['gm'] = true;
	    }

	    return $this->render('blocks/l5a/recherche_katas.html.twig', $params);
    }

    public function rechercheSortsAction(Request $request, $id = null) {
    	$defaultEM = $this->getDoctrine()->getManager();

	    $sorts = $defaultEM->getRepository(Sort::class)->findAll();
	    $params = array(
		    'sorts' => $sorts,
	    );

	    if ($id !== 'gm') {
		    $samurai = $defaultEM->getRepository(Samurai::class)->find($id);
		    $ecole = $defaultEM->getRepository(Ecole::class)->findOneBy(array('id' => $samurai->getEcoleId()));
		    $sql = $defaultEM->createQuery(
		    	'SELECT sXs.spellId
		    	FROM App\Entity\L5A\SamuraiXrefSpell sXs
		    	WHERE sXs.samuraiId = ' .$id. ''
		    );
		    $result = $sql->getResult();
		    $knownSpells = array();
		    foreach ($result as $oneSpell) {
			    $knownSpells[] = $oneSpell['spellId'];
		    }

		    $params['samurai'] = $samurai;
		    $params['ecole'] = $ecole;
		    $params['known_spells'] = $knownSpells;
	    } else {
		    $params['gm'] = true;
	    }

	    return $this->render('blocks/l5a/recherche_sorts.html.twig', $params);
    }

	public function creationAfficherInformationsAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;

		$table = $r->get('table');
		$valeur = $r->get('valeur');

		$html = '';

		switch ($table) {
			case 'clan':
				$clan = $defaultEM->getRepository(Clan::class)->findOneBy(array('id'=>$valeur));
				$html = $this->renderView('blocks/l5a/_creation_informations.html.twig',
				array(
					'type' => 'clan',
					'valeur' => $clan,
				));
				break;
			case 'famille':
				$famille = $defaultEM->getRepository(Famille::class)->findOneBy(array('id'=>$valeur));
				$html = $this->renderView('blocks/l5a/_creation_informations.html.twig',
				array(
					'type' => 'famille',
					'valeur' => $famille,
				));
				break;
			case 'ecole':
				$ecole = $defaultEM->getRepository(Ecole::class)->findOneBy(array('id'=>$valeur));
				$techniques = array();
				$techniques[1] = $defaultEM->getRepository(Technique::class)->findOneBy(array('id' => $ecole->getTechnique1Id()));
				$techniques[2] = ($ecole->getTechnique2Id() !== null) ?
					$defaultEM->getRepository(Technique::class)->findOneBy(array('id' => $ecole->getTechnique2Id())) : 'aucune';
				$techniques[3] = ($ecole->getTechnique3Id() !== null) ?
					$defaultEM->getRepository(Technique::class)->findOneBy(array('id' => $ecole->getTechnique3Id())) : 'aucune';
				$techniques[4] = ($ecole->getTechnique4Id() !== null) ?
					$defaultEM->getRepository(Technique::class)->findOneBy(array('id' => $ecole->getTechnique4Id())) : 'aucune';
				$techniques[5] = ($ecole->getTechnique5Id() !== null) ?
					$defaultEM->getRepository(Technique::class)->findOneBy(array('id' => $ecole->getTechnique5Id())) : 'aucune';

				$html = $this->renderView('blocks/l5a/_creation_informations.html.twig',
				array(
					'type' => 'ecole',
					'valeur' => $ecole,
					'techniques' => $techniques,
				));
				break;
		};

		$reponse = array(
			'html' => $html,
		);

		return new JsonResponse($reponse);
	}

    public function afficherCombatAction(SessionInterface $session, Request $request, $gameId = null) {
    	$defaultEM = $this->getDoctrine()->getManager();
	    $allSamurai = $defaultEM->getRepository(Samurai::class)->findByGameId($gameId);
    	// TODO Si $id est null, le gameId sera déterminé à partir de l'utilisateur GM

	    // Calculate ND
	    $aND = array();
	    /** @var Samurai $oneSamurai */
	    foreach ($allSamurai as $oneSamurai) {

		    $armure = $oneSamurai->getArmure() === null ?
			    null : $defaultEM->getRepository(Armure::class)->find(
				    $oneSamurai->getArmure()
			    );

		    $armureND = $oneSamurai->getArmure() === null ?
			    0:
			    $armure->getND();
		    $reduction = $oneSamurai->getArmure() === null ?
			    0:
			    $armure->getReduction();
		    $aND[$oneSamurai->getId()]['nd'] = $oneSamurai->getBonusNd() + $armureND + $oneSamurai->getReflexes() * 5 + 5;
		    $aND[$oneSamurai->getId()]['reduction'] = $reduction;
	    }

		// Check if GM, and if so get npcs
		$sessionService = SessionService::getInstance($session);
		if ($gameId === null) {
			$gameId = $sessionService->getGameId();
			if ($gameId === 0) {
				return $this->redirectToRoute('index', $request->get('_locale'));
			}
		}
		
		$user = $this->getUser();
		$sessionService->setIsGm($user, $defaultEM->getRepository(Game::class)->find($gameId));
		$isGm = $sessionService->getIsGm();
			
		$query = $defaultEM->createQuery(
	    	'SELECT p.id, p.nom
	    	FROM App\Entity\L5A\Personnage p
			INDEX BY p.id
			WHERE p.genereGm = 1 AND p.gameId = ' .$gameId. ''
	    );
		// $pnjs = $defaultEM->getRepository(Personnage::class)->findBy(array(
			// 'genereGM' => 1,
			// 'gameID' => $gameId
		// ));
		$pnjs = $query->getResult();
	    return $this->render('main/l5a/combat.html.twig', array(
		    'allSamurai' => $allSamurai,
			'pnjs' => $pnjs,
		    'nds' => $aND,
		    'game_id' => $gameId,
			'gm' => $isGm
	    ));
    }

	public function triggerPusherAction(Request $request) {

		/** @var \Pusher $pusher */
		$pusher = $this->get('lopi_pusher.pusher');
		$logger = new MyLogger();
		$data['content'] = 'hello world';
		$game = $this->getGameId();

		$pusher->set_logger($logger);
		$pusher->trigger('game-'.$game, 'maj-combat', $data);

		return true;
	}
	
	public function envoyerMailAction(Request $request, TranslatorInterface $translator) {
		$r = $request->request;
		$mail = $r->get('mail');
		$gameId = $r->get('game_id');
		$gm = $r->get('gm');

//		var_dump(gethostbyname("smtp.gmail.com"));
//		var_dump(phpinfo());
		ini_set("smtp","smtp.gmail.com");
		ini_set("smtp_port","465");

		$transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl' ))
		  ->setUsername('mickael.sib2@gmail.com')
		  ->setPassword('wM9gWYn8Qj2L3WqmUd3k')
	  ;
		$mailer = new Swift_Mailer($transport);
		$message = new \Swift_Message(
				$translator->trans('mail_invite_to_game', array(
					'%gm%' => $gm,
					'%rpg%' => 'la Légende des Cinq Anneaux',
				)));
		$message->setFrom(['mickael.sib2@gmail.com' => 'Mickaël Sibé'])
			->setTo($mail)
			->setBody('<h2>Whatever</h2>');
		$mailer->send($message);

		return new JsonResponse(array(
			'success' => true,
		));
	}
	
	public function enregistrerMDPPartieAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;
		$nouveauMDP = $r->get('mdp');
		$gameId = $r->get('game_id');
		$game = $defaultEM->getRepository(Game::class)->findOneBy(array('id' => $gameId));
		$game->setMotDePasse($nouveauMDP);
		$defaultEM->merge($game);
		$defaultEM->flush();
		
		return new JsonResponse(array(
			'success' => true,
		));
	}
	
	public function rejoindrePartieAction(Request $request, $id) {
		$defaultEM = $this->getDoctrine()->getManager();
		/** @var AppUser $user */
		$user = $this->getUser();
		$partieXJoueur = $defaultEM->getRepository(PartieXJoueur::class)->findOneBy(array('joueurId' => $user->getId()));
		if (!empty( $partieXJoueur) && $partieXJoueur->getGameId() === $id) {
			return $this->redirectToRoute('homepage', $request->get('_locale'));
		}
		$game = $defaultEM->getRepository(Game::class)->findOneBy(array('id' => $id));
		
		return $this->render('main/l5a/rejoindre_partie.html.twig', array('game' => $game));
	}

	public function rechercheDesAvantagesAction(Request $request, $id) {
		/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		$gameId = $this->getGameId($id);

		/** @var Avantage $avantages */
		$query = $defaultEM->createQuery(
			'SELECT a
	    	FROM App\Entity\L5A\Avantage a
			INDEX BY a.id
			WHERE a.gameId = ' .$gameId. '
			OR a.gameId IS NULL'
			);
		$avantages = $query->getResult();
		/** @var Desavantage $desavantages */
		$query = $defaultEM->createQuery(
			'SELECT a
	    	FROM App\Entity\L5A\Desavantage a
			INDEX BY a.id
			WHERE a.gameId = ' .$gameId. '
			OR a.gameId IS NULL'
			);
		$desavantages = $query->getResult();
		$params = array(
			'avantages' => $avantages,
			'desavantages' => $desavantages,
		);
		if ($id !== 'gm') {
			$samurai = $defaultEM->getRepository(Samurai::class)->find($id);
			$params['samurai'] = $samurai;
		} else {
//			$query = $defaultEM->createQuery(
//				'SELECT s.id, s.nom, s.xpRestant, s.avantages, s.desavantages
//	    	FROM L5ABundle:Samurai s
//			INDEX BY s.id
//			WHERE s.gameId = ' .$gameId. ''
//			);
//			$allSamurai = $query->getResult();
			$allSamurai = $defaultEM->getRepository(Samurai::class)->findBy(array('gameId' => $gameId));

			// Faire un tableau composé d'un part le samurai et de l'autre les avantages et désavantages vanilla
			for ($i = 0; $i < sizeof($allSamurai); $i++) {
				$detailQuester = new DetailsQuester($defaultEM, $allSamurai[$i]);
				$avantagesPossedes = implode(',', $detailQuester->getAvantages(false)); // only vanilla ids
				$desavantagesPossedes = implode(',',$detailQuester->getDesavantages(false)); // only vanilla ids
				$allSamurai[$i] = array(
					'samurai' => $allSamurai[$i],
					'avantages' => $avantagesPossedes,
					'desavantages' => $desavantagesPossedes,
				);
			}

			$params['gm'] = true;
			$params['allSamurai'] = $allSamurai;
		}

		return $this->render('blocks/l5a/recherche_avantages_desavantages.html.twig', $params);
	}

	public function enregistrerCoordonneesAction(Request $request, $id) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;
		$gameId = $this->getGameId($id);
		$x = $r->get('x');
		$y = $r->get('y');
		$nom = $r->get('nom');
		$estMarqueur = $r->get('est_marqueur');
		$lastPosition = 0;
		$position = null;

		if ($gameId === null)  {
			return new JsonResponse(array(
				'success' => false,
			));
		}

		// Récupérer la dernière position si le point n'est pas un marqueur
		if ($estMarqueur != 1) {
			/*$coordonneesExistantes = $defaultEM->getRepository(Coordonnees::class)->findBy(
				array('gameId' => $gameId),
				array('position' => 'DESC')
			);*/
			$query = $defaultEM->createQuery(
				'SELECT c.position
		    	FROM App\Entity\L5A\Coordonnees c
		    	WHERE c.gameId=' . $gameId . '
		    	AND c.position IS NOT NULL
		    	ORDER BY c.position DESC');
			$aPositions = $query->getResult();
			if ($aPositions) {
				$lastPosition = $aPositions[0]['position'];
			}
			$position = $lastPosition + 1;
		}

		$newCoordonnee = new Coordonnees();
		$newCoordonnee->setGameId($gameId);
		$newCoordonnee->setNom($nom);
		$newCoordonnee->setPosition($position);
		$newCoordonnee->setEstMarqueur($estMarqueur);
		$newCoordonnee->setX($x);
		$newCoordonnee->setY($y);

		$defaultEM->persist($newCoordonnee);
		$defaultEM->flush();

		// Envoyer coordonnées
		/*if ($estMarqueur != 1) {
			array_unshift($coordonneesExistantes, $newCoordonnee);
			//$coordonneesExistantes[] = $newCoordonnee;
		}*/

		$coordonneesExistantes = $defaultEM->getRepository(Coordonnees::class)->findBy(
			array('gameId' => $gameId), array('position' => 'DESC')
		);
		$aCoordonnees = $this->recupererCoordonnees($coordonneesExistantes);
		return new JsonResponse(array(
			'success' => true,
			'coordonnees' => $aCoordonnees,
		));
	}

	/**
	 * @param array $coordonnees // array d'objets Coordonnees
	 * @return array $aCoordonnees // array qui sera renvoyé en json pour pouvoir être récupéré par javascript
	 */
	private function recupererCoordonnees($coordonnees) {
		$aCoordonnees = array();
		/** @var Coordonnees $oneCoordonnee */
		foreach ($coordonnees as $oneCoordonnee) {
			$aCoordonnees[] = array(
				'nom' => $oneCoordonnee->getNom(),
				'x' => $oneCoordonnee->getX(),
				'y' => $oneCoordonnee->getY(),
				'position' => $oneCoordonnee->getPosition(),
				'est_marqueur' => $oneCoordonnee->getEstMarqueur(),
			);
		}

		return $aCoordonnees;
	}

	public function tutoPJAction(Request $request) {
		return $this->render('main/tuto_pj.html.twig');
	}

	public function tutoMJAction(Request $request) {
		return $this->render('main/tuto_mj.html.twig');
	}

	/**
	 * @param Request $request
	 * @param $id Game id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function carteAction(Request $request, $id) {
		$defaultEM = $this->getDoctrine()->getManager();
		$gameId = $this->getGameId($id);
		// Récupérer la dernière position
		$coordonneesExistantes = $defaultEM->getRepository(Coordonnees::class)->findBy(
			array('gameId' => $gameId), array('position' => 'DESC')
		);
		$aCoordonnees = $this->recupererCoordonnees($coordonneesExistantes);

		return $this->render('main/l5a/carte.html.twig', array(
			'coordonnees' => $aCoordonnees,
		));
	}

	public function sourcesAction(Request $request) {
		return $this->render('main/sources.html.twig');
	}

	public function vousPourriezAimerAction(Request $request) {
		return $this->render('main/vous_pourriez_aimer.html.twig');
	}

	public function aVenirAction(Request $request) {
		return $this->render('main/a_venir.html.twig');
	}

	function updateTransDB() {
		$defaultEM = $this->getDoctrine()->getManager();
		$sorts = $defaultEM->getRepository(Sort::class)->findAll();
		foreach ($sorts as $oneSort) {
			$motsCles = $oneSort->getId() . '.mots_cles';
			$portee = $oneSort->getId() . '.portee';
			$zone_effet = $oneSort->getId() . '.zone_effet';
			$duree = $oneSort->getId() . '.duree';
			$augmentations = $oneSort->getId() . '.augmentations';
			$description = $oneSort->getId() . '.description';
			$oneSort->setMotsCles($motsCles);
			$oneSort->setPortee($portee);
			$oneSort->setZoneEffet($zone_effet);
			$oneSort->setDuree($duree);
			$oneSort->setAugmentations($augmentations);
			$oneSort->setDescription($description);
			$defaultEM->merge($oneSort);
		}
		$defaultEM->flush();
	}
}