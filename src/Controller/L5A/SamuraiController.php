<?php

namespace App\Controller\L5A;

use App\Entity\L5A\Arme;
use App\Entity\L5A\ArmePerso;
use App\Entity\L5A\Armure;
use App\Entity\L5A\AvantageXSamurai;
use App\Entity\L5A\Clan;
use App\Entity\L5A\DesavantageXSamurai;
use App\Entity\L5A\Ecole;
use App\Entity\L5A\Equipement;
use App\Entity\L5A\Famille;
use App\Entity\L5A\Game;
use App\Entity\L5A\HistoriqueXp;
use App\Entity\L5A\Lieu;
use App\Entity\L5A\SamuraiXrefArrow;
use App\Entity\L5A\SamuraiXrefCharacter;
use App\Entity\L5A\SamuraiXrefKata;
use App\Entity\L5A\SamuraiXrefNotes;
use App\Entity\L5A\Personnage;
use App\Entity\L5A\Samurai;
use App\Entity\L5A\SamuraiXrefPlace;
use App\Entity\L5A\SamuraiXrefSkill;
use App\Entity\L5A\SamuraiXrefSpell;
use App\Entity\L5A\SamuraiXrefTechnique;
use App\Entity\L5A\Technique;
use App\Entity\L5A\AppUser;
use App\Form\L5A\SamuraiType;
use App\Service\L5A\DetailsQuester;
use App\Service\SessionService;
use App\Service\MyLogger;
use Doctrine\ORM\EntityManager;
use Error;
use Pusher\Pusher;
use Spipu\Html2Pdf\Html2Pdf;
use App\Controller\FatherController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SamuraiController extends FatherController
{
    public function createAction(Request $request, TranslatorInterface $trans)
    {
    	/** @var EntityManager $defaultEM */
    	$defaultEM = $this->getDoctrine()->getManager();
    	$newSamurai = new Samurai();
    	$gameId = $this->getGameId();
	    $detailQuester = new DetailsQuester($defaultEM, $newSamurai);
    	$form = $this->createForm(SamuraiType::class, $newSamurai, array('gameId' => $gameId));
		$equipement = '';

		//dnd($request);
    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {

    		/** @var AppUser $user */
		    $user = $this->getUser();

    		/** @var Samurai $newSamurai */
    		$newSamurai = $form->getData();

    		/** @var Clan $specificClan */
    		$specificClan = $form->get('clanId')->getData();

    		/** @var Famille $specificFamille */
    		$specificFamille = $form->get('familleId')->getData();

    		/** @var Ecole $specificEcole */
    		$specificEcole = $form->get('ecoleId')->getData();

    		if ($specificClan != null) {
    			$newSamurai->setClanId($specificClan->getId());
			    $newSamurai->setClanName($specificClan->getNom());
    		}
    		if ($specificFamille != null) {
    			$newSamurai->setFamilleId($specificFamille->getId());
			    $newSamurai->setFamilleName($specificFamille->getNom());
    		}
            $newSamurai->setEcoleId($specificEcole->getId());
		    $newSamurai->setEcoleName($specificEcole->getNom());
		    $newSamurai->setHonneur($specificEcole->getHonneurDepart());

		    $type = $specificEcole->getType();
			$newSamurai->setType($type);

			$equipement = explode(',', $specificEcole->getEquipement());

		    if ($specificFamille !== null) {
			    if ($specificFamille->getId() != 45) {
				    if ($specificFamille->getId() == 26) {
					    $nom = 'Togashi'; // Otherwise Togashi Order
				    } else {
					    $nom = $specificFamille->getNom();
				    }
			    } else {
				    $nom = '';
			    }
		    } else {
			    $nom = '';
		    }
    		$nomComplet = trim($nom . ' ' . $form->get('nom')->getData());
		    // Si ronin (id = 32), rajouter 25 points d'XP
		    $nbXP = $specificClan->getId() === 32 ? 65 : 40;

			$newSamurai->setNom($nomComplet);
//			$type = strtolower($form->get('type')->getdata());
//    		$newSamurai->setType($type);
    		$newSamurai->setRang(1);
    		$newSamurai->setReputation(1);
    		$newSamurai->setXp($nbXP);
    		$newSamurai->setTerre(2);
    		$newSamurai->setConstitution(2);
    		$newSamurai->setVolonte(2);
    		$newSamurai->setAir(2);
    		$newSamurai->setReflexes(2);
    		$newSamurai->setIntuition(2);
    		$newSamurai->setEau(2);
    		$newSamurai->setForce(2);
    		$newSamurai->setPerception(2);
    		$newSamurai->setFeu(2);
    		$newSamurai->setAgilite(2);
    		$newSamurai->setIntelligence(2);
    		$newSamurai->setVide(2);
			$newSamurai->setGloire(1);
    		$newSamurai->setStatut(1);
    		$newSamurai->setSouillure(0);
    		$newSamurai->setZeni(0);
    		$newSamurai->setBu(0);
    		$newSamurai->setBonusInit('0G0');
		    $newSamurai->setGameId($gameId);

			// Ajouter les traits bonus
		    if ($specificFamille !== null) {
		    	$traitBonus = str_replace('è', 'e', str_replace('é', 'e', $trans->trans($specificFamille->getTraitBonus(),
				    array(), 'messages', 'fr')));
			    $methodeSetTrait = 'set'.$trans->trans($traitBonus, array(), 'messages', 'fr');
			    $methodeGetTrait = 'get'.$trans->trans($traitBonus, array(), 'messages', 'fr');
			    $newSamurai->$methodeSetTrait($newSamurai->$methodeGetTrait()+1);
		    } elseif ($specificClan->getId() === 31) {
			    $newSamurai->setVide(3); // Si membre de la Confrérie, ajouter un Anneau de Vide O:)
		    }
		    $traitBonus = str_replace('è', 'e', str_replace('é', 'e', $trans->trans($specificEcole->getTraitBonus(),
			    array(), 'messages', 'fr')));
			$methodeSetTrait = 'set'.$trans->trans($traitBonus, array(), 'messages', 'fr');
			$methodeGetTrait = 'get'.$trans->trans($traitBonus, array(), 'messages', 'fr');
			$newSamurai->$methodeSetTrait($newSamurai->$methodeGetTrait()+1);

			if ($trans->trans($type) == 'shugenja') {

				// Lui attribuer le nombre d'emplacements restants pour les sorts
				$newSamurai->setTerreRestants($newSamurai->getTerre());
				$newSamurai->setAirRestants($newSamurai->getTerre());
				$newSamurai->setEauRestants($newSamurai->getEau());
				$newSamurai->setFeuRestants($newSamurai->getFeu());
				$newSamurai->setVideRestants($newSamurai->getVide());
			}

		    $newSamurai->setBlessures(0);

			// Let there be samurai
    		$defaultEM->persist($newSamurai);
    		$defaultEM->flush();

		    $samuraiId = $newSamurai->getId();

		    // Set skills
		    $detailQuester->setSkills($specificEcole->getCompetences());

		    // Attribuer les sorts universels au shugenja
		    if ($trans->trans($type) == 'shugenja') {
			    $samuraiXSpell = new SamuraiXrefSpell();
			    $samuraiXSpell->setSamuraiId($samuraiId)
				    ->setSpellId(36);
			    $defaultEM->persist($samuraiXSpell);

			    $samuraiXSpell = new SamuraiXrefSpell();
			    $samuraiXSpell->setSamuraiId($samuraiId)
				    ->setSpellId(37);
			    $defaultEM->persist($samuraiXSpell);

			    $samuraiXSpell = new SamuraiXrefSpell();
			    $samuraiXSpell->setSamuraiId($samuraiId)
				    ->setSpellId(38);
			    $defaultEM->persist($samuraiXSpell);
			}

			// Set techniques
		    $samuraiXTechnique = new SamuraiXrefTechnique();
		    $samuraiXTechnique->setSamuraiId($samuraiId)
		                      ->setTechniqueId($specificEcole->getTechnique1Id())
		                      ->setRank(1);
		    $defaultEM->persist($samuraiXTechnique);
		    $samuraiXTechnique = new SamuraiXrefTechnique();
		    $samuraiXTechnique->setSamuraiId($samuraiId)
		                      ->setTechniqueId($specificEcole->getTechnique2Id())
		                      ->setRank(2);
		    $defaultEM->persist($samuraiXTechnique);
		    $samuraiXTechnique = new SamuraiXrefTechnique();
		    $samuraiXTechnique->setSamuraiId($samuraiId)
		                      ->setTechniqueId($specificEcole->getTechnique3Id())
		                      ->setRank(3);
		    $defaultEM->persist($samuraiXTechnique);
		    $samuraiXTechnique = new SamuraiXrefTechnique();
		    $samuraiXTechnique->setSamuraiId($samuraiId)
		                      ->setTechniqueId($specificEcole->getTechnique4Id())
		                      ->setRank(4);
		    $defaultEM->persist($samuraiXTechnique);
		    $samuraiXTechnique = new SamuraiXrefTechnique();
		    $samuraiXTechnique->setSamuraiId($samuraiId)
		                      ->setTechniqueId($specificEcole->getTechnique5Id())
		                      ->setRank(5);
		    $defaultEM->persist($samuraiXTechnique);

			// Set XP
		    $detailQuester->setXPRestant('+', $nbXP, null, 
		    	$trans->trans('samurai.xp.character_creation'));

		    $samuraiXNotes = new SamuraiXrefNotes();
		    $samuraiXNotes->setSamuraiId($samuraiId);
		    $defaultEM->persist($samuraiXNotes);

		    ////// Équipement /////////
		    $equipementsAAjouter = array();
		    $armesAAjouter = array();
		    // Liste armures
		    $qb = $defaultEM->createQuery(
			    'SELECT a.id, a.nom
				FROM App\Entity\L5A\Armure a
				WHERE a.personnalisee = 0'
		    );
		    $aArmures = array();
		    $result = $qb->getResult();
		    // Créer un tableau sous le bon format
		    for($i = 0; $i < sizeof($result); $i++) {
			    $aArmures[$result[$i]['id']] = $result[$i]['nom'];
		    }
		    // Liste armes
		    $qb = $defaultEM->createQuery(
			    'SELECT a.id, a.nom
				FROM App\Entity\L5A\Arme a
				WHERE a.personnalisee = 0'
		    );
		    $aArmes = array();
		    $result = $qb->getResult();
		    // Créer un tableau sous le bon format
		    for($i = 0; $i < sizeof($result); $i++) {
			    $aArmes[$result[$i]['id']] = $result[$i]['nom'];
		    }

		    // Auto-incrément maison
		    $newIdEquipement = $defaultEM->getRepository(Equipement::class)->findOneBy(array(), array('id' => 'DESC'),1)->getId() +1;
		    $newIdArmePerso = $defaultEM->getRepository(ArmePerso::class)->findOneBy(array(), array('id' => 'DESC'),1)->getId() +1;

		    $samuraiId = $newSamurai->getId();
		    foreach($equipement as $item) {

			    $item = trim($item);

			    if (strpos($item, 'koku') != false) {
				    $koku = explode('ko', $item)[0]; // Get the number only
				    $newSamurai->setKoku((int)$koku);

			    } else if (strtolower($item) == 'daisho') { // L'âme et l'honneur

				    // Katana
				    $newArmePerso = new ArmePerso();
				    $newArmePerso->setId($newIdArmePerso);
				    $newArmePerso->setNom('Katana');
				    $newArmePerso->setArmeId(1);
				    $newArmePerso->setfiltreAttaque('0G0');
				    $newArmePerso->setfiltreDegats('0G0');
				    $newArmePerso->setSamurai($samuraiId);
				    $defaultEM->persist($newArmePerso);
				    $armesAAjouter[] = $newIdArmePerso;
				    $newIdArmePerso++;
				    // Wakizashi
				    $newArmePerso = new ArmePerso();
				    $newArmePerso->setId($newIdArmePerso);
				    $newArmePerso->setNom('Wakizashi');
				    $newArmePerso->setArmeId(3);
				    $newArmePerso->setfiltreAttaque('0G0');
				    $newArmePerso->setfiltreDegats('0G0');
				    $newArmePerso->setSamurai($samuraiId);
				    $defaultEM->persist($newArmePerso);
				    $armesAAjouter[] = $newIdArmePerso;
				    $newIdArmePerso++;

			    } else if (($idArme = array_search(ucfirst(strtolower($item)), $aArmes)) !== false) { // Ajouter Armes

				    $newArmePerso = new ArmePerso();
				    $newArmePerso->setId($newIdArmePerso);
				    $newArmePerso->setNom($item);
				    $newArmePerso->setArmeId($idArme);
				    $newArmePerso->setfiltreAttaque('0G0');
				    $newArmePerso->setfiltreDegats('0G0');
				    $newArmePerso->setSamurai($samuraiId);
				    $defaultEM->persist($newArmePerso);
				    $armesAAjouter[] = $newIdArmePerso;
				    $newIdArmePerso++;

			    } else if (strpos($item, 'au choix') === false) {

				    $newEquipement = new Equipement();
				    $newEquipement->setId($newIdEquipement);
				    $newEquipement->setNom($item);
				    $newEquipement->setSamurai($samuraiId);
				    if (($idArmure = array_search(str_replace('  ', ' ',$item), $aArmures)) !== false) {
					    $newEquipement->setIdArmure($idArmure);
				    }
				    $defaultEM->persist($newEquipement);
				    $equipementsAAjouter[] = $newIdEquipement;
				    $newIdEquipement++;
			    }
		    }

		    $defaultEM->merge($newSamurai);
		    $defaultEM->flush();

    		return $this->redirectToRoute('recherche_des_avantages', array(
			    'id' => $newSamurai->getId(),
			    '_locale' => $request->get('_locale'),
		    ));
	    } else {
		    return $this->render('main/l5a/create_samurai.html.twig', array('form' => $form->createView()));
	    }
    }

    public function displayMainAction(TranslatorInterface $translator, SessionInterface $session, $id = null, $render = true) {
		/** @var EntityManager $defaultEM */
	    $defaultEM = $this->getDoctrine()->getManager();
	    $estProprio = false;
	    $gm = null;

	    /** @var Samurai $samurai */
	    $samurai = $this->getSamurai($id, $defaultEM);
	    $detailQuester = new DetailsQuester($defaultEM, $samurai, null, $translator);
		$sessionService = SessionService::getInstance($session);
 
	    $competences = $detailQuester->getCompetences();
	    $armes = $detailQuester->getArmes();
	    $equipement = $defaultEM->getRepository(Equipement::class)->findBy(array('samurai' => $samurai->getId()));
	    $avantages = $detailQuester->getAvantages();
	    $desavantages = $detailQuester->getDesavantages();
	    /** @var Ecole $ecole */
	    // $ecole = $defaultEM->getRepository('L5ABundle:Ecole')->find($samurai->getEcoleId());
	    $armure = $detailQuester->getArmure();
	    $katas = $detailQuester->getKatas();
		$sorts = $detailQuester->getSorts();
		/** @var SamuraiXrefNotes $notesCampagne */
	    $notesCampagne = $defaultEM->getRepository(SamuraiXrefNotes::class)
	                               ->findOneBy(array('samuraiId' => $samurai->getId()));
	    $personnages = $detailQuester->getCharacters($notesCampagne); // User notesCampaign to check if the
	    // characters are in the right format
	    $lieux = $detailQuester->getPlaces($notesCampagne);
	    $flechesRestantes = $detailQuester->getFleches();
	    $detailQuester->majReputation();
		$techniques = array(
			'usedTechniques' => $detailQuester->getTechniques(),
			'alternativeTechniques' => array()
		);

		// Get the possible techniques
		$qb = $defaultEM->getRepository(Technique::class)->createQueryBuilder('t');
            $qb->select('t')
			->leftJoin(Samurai::class, 's', Join::WITH,
				$qb->expr()->eq('s.type', 't.typePersonnage'))
			->where(
				$qb->expr()->orX(
					$qb->expr()->eq('s.id', ':samurai_id'),
					$qb->expr()->isNull('t.typePersonnage')
				))
            ->setParameter(':samurai_id', $samurai->getId()
			);

		$results = $qb->getQuery()->getResult();
		/** @var Technique $technique */
	    foreach($results as $technique) {
			if(! in_array($technique, $techniques['usedTechniques'])) {
				if($technique->getEcoles() === null || in_array($samurai->getEcoleId(), $technique->getEcoles())) {
					array_push($techniques['alternativeTechniques'], $technique);
				}
			}
		}
		// Si le joueur est MJ ou propriétaire du personnage, il peut voir ses avantages cachés
	    /** @var AppUser $user */
	    $user = $this->getUser();
		// Récupérer les notes communes et les provisions
	    /** @var Game $game */
	    $gameId = $this->getGameId();
	    $game = $defaultEM->getRepository(Game::class)->find($gameId);
	    if ($user !== null) {
		    //$gm = $user->hasRole('ROLE_GM') ? true : null;
			$sessionService->setIsGm($user, $game);
			$isGm = $sessionService->getIsGm();
		    $estProprio = $isGm || $samurai->getJoueur() == $user->getId();
	    }

	    $provisions = $game->getProvisions();
	    $notesJoueurs = $game->getNotesJoueurs();

	    // Image
	    $sql = $defaultEM->createQuery(
		    'SELECT c.mon, c.description
	    	FROM App\Entity\L5A\Clan c
	    	WHERE c.id = '.$samurai->getClanId().''
	    );

	    $results = $sql->getResult()[0];
	    $mon = $results['mon'];
	    $clanDescription = $results['description'];
	    $familleDescription = $samurai->getFamilleId() !== null ?
		    $defaultEM->getRepository(Famille::class)->find($samurai->getFamilleId())->getDescription() : '';

	    $parameters = array(
		    'samurai' => $samurai,
		    'competences' => $competences,
		    'armes' => $armes,
		    'equipement' => $equipement,
		    'avantages' => $avantages,
		    'desavantages' => $desavantages,
		    'techniques' => $techniques,
		    'armure' => $armure,
		    'fleches' => $flechesRestantes,
		    'katas' => $katas,
		    'sorts' => $sorts,
		    'mon' => $mon,
		    'est_proprio' => $estProprio,
		    'gm' => $isGm,
		    'provisions' => $provisions,
		    'notes_joueurs' => $notesJoueurs,
		    'clan_description' => $clanDescription,
		    'famille_description' => $familleDescription,
		    'pusherKey' => $this->getParameter('pusherKey'),
		    'pusherCluster' => $this->getParameter('pusherCluster'),
		    'game_id' => $gameId
	    );

        $parameters['notesCampagne'] = $notesCampagne;
        $parameters['personnages'] = $personnages;
        $parameters['lieux'] = $lieux;

        if ($render === false) {
        	return $parameters;
        } else {
	        return $this->render('main/l5a/samurai.html.twig', $parameters);
        }
    }

    public function nouvelItemAction(Request $request, $id) {

    	$r = $request->request;
    	$defaultEM = $this->getDoctrine()->getManager();

	    /** @var Samurai $samurai */
	    $samurai = $this->getSamurai($id, $defaultEM);
	    $detailQuester = new DetailsQuester($defaultEM, $samurai);
	    $nomItem = $r->get('nom');

	    $detailQuester->creerItem($nomItem);

	    $equipement = $defaultEM->getRepository(Equipement::class)->findBy(array('samurai' => $samurai->getId()));
	    $familleDescription = $defaultEM->getRepository(Famille::class)->find($samurai->getFamilleId())->getDescription();
	    $clanDescription = $defaultEM->getRepository(Clan::class)->find($samurai->getClanId())->getDescription();
	    return new JsonResponse(array(
		    'content' => $this->renderView('blocks/l5a/_details_et_equipements.html.twig', array(
			    'equipement' => $equipement,
			    'samurai' => $samurai,
			    'famille_description' => $familleDescription,
			    'clan_description' => $clanDescription,
		    )),
	    ));
    }

    public function acheterItemAction(Request $request, $id) {
	    $r = $request->request;
	    /** @var EntityManager $defaultEM */
	    $defaultEM = $this->getDoctrine()->getManager();
	    /** @var Samurai $samurai */
	    $samurai = $this->getSamurai($id, $defaultEM);
	    $detailQuester = new DetailsQuester($defaultEM, $samurai);

	    $response = array(
		    'success' => true,
	    );

	    $coutZeni = $r->get('cout_zeni');
	    if ($coutZeni === null) {
		    $response['success'] = false;
		    $response['failure'] = true;
		    $response['message'] = 'Le coût n\'a pas été trouvé';
	    }

	    try {
		    list($koku, $bu, $zeni) = $detailQuester->majMonnaieAction($coutZeni);
	    } catch (\Exception $e) {
	    	return $this->ajaxError($e->getMessage());
	    }

	    switch ($type = $r->get('type')) {
		    case 'arme':

			    $armeId = $r->get('id');
		    	// Créer une arme Perso
				/** @var Arme $arme */
			    $arme = $defaultEM->getRepository(Arme::class)->find($armeId);
				$detailQuester->creerArmePerso($arme->getNom(), $armeId, false);
			    // Le merge et le flush sont effectués dans majMonnaie
			    break;

		    case 'armure':

			    $armureId = $r->get('id');
		    	// Créer un équipement
				$armure = $defaultEM->getRepository(Armure::class)->find($armureId);
			    $detailQuester->creerItem($armure->getNom(), $armureId);
			    // Le merge et le flush sont effectués dans majMonnaie
			    break;

		    case 'fleche':

			    $flecheId = $r->get('id');
			    $nbFleches = $r->get('nb_fleches');
		    	// Créer une arme Perso
				/** @var Arme $arme */
				$detailQuester->setFleche($flecheId, $nbFleches);
			    // Le merge et le flush sont effectués dans majMonnaie
			    break;
	    }

	    $defaultEM->merge($samurai);
	    $defaultEM->flush();

	    $response['koku'] = $koku;
	    $response['bu'] = $bu;
	    $response['zeni'] = $zeni;

	    return new JsonResponse($response);
    }

	public function acheterCompetenceAction(Request $request, $id, TranslatorInterface $translator) {
    	/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		/** @var Samurai $samurai */
		$samurai = $this->getSamurai($id, $defaultEM);
		$detailQuester = new DetailsQuester($defaultEM, $samurai);

		$r = $request->request;
		$competenceId = $r->get('competenceId');
		$rang = $r->get('rang');
		$cout = $r->get('cout');

		if ($samurai->getXpRestant() - (int)$cout <0) {
			return $this->ajaxError('Le samurai n\'a pas assez de points d\'expérience.');
		}

		$sql = $defaultEM->createQuery("SELECT c.nom, c.specialisations FROM App\Entity\L5A\Competence c WHERE c.id = $competenceId");
		$result = $sql->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];
		$nom = $result['nom'];
		$specialisations = $result['specialisations'];

		$samuraiXSkill = $defaultEM->getRepository(SamuraiXrefSkill::class)->findOneBy(
			array('samuraiId' => $id, 'skillId' => $competenceId)
		);
		if ($samuraiXSkill === null) {
			$samuraiXSkill = new SamuraiXrefSkill();
			$samuraiXSkill
				->setSamuraiId($id)
				->setSkillId($competenceId);
		}
		$samuraiXSkill
			->setRank($rang);

		$detailQuester->setXPRestant('-', $cout, $translator->trans('samurai.xp.skill'), $nom .
			' ' . $translator->trans('samurai.xp.rank') . ' ' . $rang);
		$detailQuester->majReputation();

		$defaultEM->persist($samuraiXSkill);
		$defaultEM->merge($samurai);
	    $defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
			'xpRestante' => $samurai->getXpRestant(),
			'rang' => $rang,
			'reputation' => $samurai->getReputation(),
			'specialisations' => $specialisations,
		));
	}

	public function acheterSpecialiteAction(Request $request, ?int $id, TranslatorInterface $translator) : JsonResponse {
		/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		/** @var Samurai $samurai */
		$samurai = $this->getSamurai($id, $defaultEM);
		$detailQuester = new DetailsQuester($defaultEM, $samurai);
		$r = $request->request;
		$competenceId = $r->get('competenceId');
		$specialite = $r->get('specialite');

		if ($samurai->getXpRestant() - 2 <0) {
			return $this->ajaxError('Le samurai n\'a pas assez de points d\'expérience.');
		}

		/** @var SamuraiXrefSkill $samuraiXSkill */
		$samuraiXSkill = $defaultEM->getRepository(SamuraiXrefSkill::class)->findOneBy(
			array('samuraiId' => $id, 'skillId' => $competenceId)
		);
		if ($samuraiXSkill === null) {
			return $this->ajaxError('Le samurai ne semble pas posséder cette compétence.');
		}

		$sNewSpecialities = trim($samuraiXSkill->getSpecialities().','.$specialite,',');
		$samuraiXSkill->setSpecialities($sNewSpecialities);

		$sql = $defaultEM->createQuery("SELECT c.nom FROM App\Entity\L5A\Competence c WHERE c.id = $competenceId");
		$nomCompetence = $sql->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0]['nom'];

		$detailQuester->setXPRestant('-', 2, $translator->trans('samurai.xp.specialty'), $specialite . 
			$translator->trans('samurai.xp.for_skill') . $nomCompetence);
		$defaultEM->merge($samuraiXSkill);
		$defaultEM->merge($samurai);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
			'xpRestante' => $samurai->getXpRestant(),
			'rang' => $samurai->getRang(),
			'reputation' => $samurai->getReputation(),
			'new_specialities' => $sNewSpecialities
		));
	}

	public function acheterKataAction(Request $request, ?int $samuraiId, TranslatorInterface $translator) : JsonResponse {
		/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		/** @var Samurai $samurai */
		$samurai = $this->getSamurai($samuraiId, $defaultEM);
		$detailQuester = new DetailsQuester($defaultEM, $samurai);
		$r = $request->request;
		$kataId = $r->get('kata_id');
		$cout = $r->get('cout_xp');

		if ($samurai->getXpRestant() - $cout <0) {
			return $this->ajaxError('Le samurai n\'a pas assez de points d\'expérience.');
		}

		/** @var SamuraiXrefKata $samuraiXKata */
		$samuraiXKata = $defaultEM->getRepository(SamuraiXrefKata::class)->findOneBy(
			array('samuraiId' => $samuraiId, 'kataId' => $kataId)
		);
		if ($samuraiXKata === null) {
			$samuraiXKata = new SamuraiXrefKata();
			$samuraiXKata
				->setSamuraiId($samuraiId)
				->setKataId($kataId);
		} else {
			return $this->ajaxError('Le samurai possède déjà ce kata.');
		}

		$sql = $defaultEM->createQuery("SELECT k.nom FROM App\Entity\L5A\Kata k WHERE k.id = $kataId");
		$nomKata = $sql->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0]['nom'];

		$detailQuester->setXPRestant('-', $cout, $translator->trans('samurai.xp.kata'), $nomKata);
		$defaultEM->merge($samuraiXKata);
		$defaultEM->merge($samurai);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
			'xpRestante' => $samurai->getXpRestant()
		));
	}

	public function acheterSortAction(Request $request, ?int $samuraiId, TranslatorInterface $translator) : JsonResponse {
		/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		/** @var Samurai $samurai */
		$samurai = $this->getSamurai($samuraiId, $defaultEM);
		$r = $request->request;
		$sortId = $r->get('sort_id');

		/** @var SamuraiXrefSpell $samuraiXSort */
		$samuraiXSort = $defaultEM->getRepository(SamuraiXrefSpell::class)->findOneBy(
			array('samuraiId' => $samuraiId, 'spellId' => $sortId)
		);
		if ($samuraiXSort === null) {
			$samuraiXSort = new SamuraiXrefSpell();
			$samuraiXSort
				->setSamuraiId($samuraiId)
				->setSpellId($sortId);
		} else {
			return $this->ajaxError('Le samurai possède déjà ce sort.');
		}

		$defaultEM->merge($samuraiXSort);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true
		));
	}

    public function majFlechesAction(Request $request, $id) {
	    $r = $request->request;
	    /** @var EntityManager $defaultEM */
	    $defaultEM = $this->getDoctrine()->getManager();

	    $nouvelleQuantite = $r->get('qte');
	    $flecheId = $r->get('id');

	    /** @var SamuraiXrefArrow $samuraiXArrow */
	    $samuraiXArrow = $defaultEM->getRepository(SamuraiXrefArrow::class)->findOneBy(
	    	array('samuraiId' => $id, 'arrowId' => $flecheId)
	    );
	    $samuraiXArrow->setQuantity($nouvelleQuantite);

	    $defaultEM->merge($samuraiXArrow);
	    $defaultEM->flush();

	    return new JsonResponse(array(
		    'success' => true,
	    ));
    }

	public function majSamuraiAction(Request $request, Pusher $pusher, $id) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		/** @var Samurai $samurai */
		$samurai = $this->getSamurai($id, $defaultEM);
		$detailQuester = new DetailsQuester($defaultEM, $samurai);
		$champ = $r->get('champ');
		$valeur = $r->get('valeur');
		$functionAAppliquer = 'set' . $champ;
		$samurai->$functionAAppliquer($valeur);

		$response = array(
			'success' => true,
		);

		// Gérer les différents cas

		if ($champ == 'Blessures' && $r->get('provenance') == 'combat') {
//			$pusher = $this->get('lopi_pusher.pusher');
			$game = $this->getGameId();
			$pusher->trigger('game-'.$game, 'maj-blessures-'.$id, $valeur);
		}

		if ($r->get('cout_xp') !== null) {
			$xpRestante = $r->get('cout_xp');
			$detailQuester->setXPRestant('-', $xpRestante, $champ);
			$response['xpRestante'] = $samurai->getXpRestant() - $xpRestante;
		}

		$defaultEM->merge($samurai);

		if (! is_null($r->get('anneau'))) {
			$cout = $r->get('cout');
			$anneau = $r->get('anneau');

			$functionAAppliquer = 'set'.$anneau;
			$nouvelleValeur = 0;
			if ($anneau == 'Terre') {
				$nouvelleValeur = min($samurai->getConstitution(), $samurai->getVolonte());
				$samurai->setTerreRestants($samurai->getTerreRestants()+1);
			}
			if ($anneau == 'Air') {
				$nouvelleValeur = min($samurai->getReflexes(), $samurai->getIntuition());
				$samurai->setAirRestants($samurai->getAirRestants()+1);
			}
			if ($anneau == 'Eau') {
				$nouvelleValeur = min($samurai->getForce(), $samurai->getPerception());
				$samurai->setEauRestants($samurai->getEauRestants()+1);
			}
			if ($anneau == 'Feu') {
				$nouvelleValeur = min($samurai->getAgilite(), $samurai->getIntelligence());
				$samurai->setFeuRestants($samurai->getFeuRestants()+1);
			}
			if ($anneau == 'Vide') {
				$nouvelleValeur = $samurai->getVide();
				$samurai->setVideRestants($samurai->getVideRestants()+1);
			}

			$samurai->$functionAAppliquer($nouvelleValeur);
			$xpRestante = $samurai->getXpRestant() - $cout;
			$detailQuester->setXPRestant('-', $cout, $champ);
			$response['anneauValeur'] = $nouvelleValeur;
			$response['xpRestante'] = $xpRestante;
			$defaultEM->merge($samurai);
		}
		$defaultEM->flush();

//		if ($champ === 'Honneur' || $champ === 'Gloire') {
//			$response['html'] = $this->renderView('@L5A/Blocks/honneur-gloire-et-souillure.html.twig', array(
//				'samurai' => $samurai
//			));
//		}
		return new JsonResponse($response);
	}

	public function majSamuraiTechniqueAction (Request $request, ?int $id) : JsonResponse {
		$r = $request->request;
		$em = $this->getDoctrine()->getManager();
		$techniqueId = $r->get('technique_id');
		$rank = $r->get('rank');
		/** @var SamuraiXrefTechnique $samuraiXTechnique */
		$samuraiXTechnique = $em->getRepository(SamuraiXrefTechnique::class)
		   ->findOneBy(array('samuraiId' => $id, 'rank' => $rank));
		$samuraiXTechnique->setTechniqueId($techniqueId);
		$em->merge($samuraiXTechnique);
		$em->flush();

		return new JsonResponse(array(
			'success' => true
		));
	}

	public function majPersonnageAction(Request $request, ?int $id) : JsonResponse {

		$r = $request->request;
		/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();

		/** @var Samurai $samurai */
		$samurai = $this->getSamurai($id, $defaultEM);
		$isNew = false;
		$isNew = $r->get('is_new');
		$personnageId = $r->get('id');
		$nom = $r->get('nom');
		$rapport = $r->get('rapport');
		$notes = $r->get('notes');

		if ($isNew == 'true') {
			$detailQuester = new DetailsQuester($defaultEM, $samurai);

			$personnage = new Personnage();
			$personnage->setNom($nom);
			$personnage->setRapport($rapport);
			$personnage->setNotes($notes);
			$defaultEM->persist($personnage);
			$defaultEM->flush();

			$newSamuraiXCharacter = new SamuraiXrefCharacter();
			$newSamuraiXCharacter->setSamuraiId($id)
			                     ->setCharacterId($personnage->getId());
			$defaultEM->merge($newSamuraiXCharacter);
			$defaultEM->flush();

			/** @var Game $game */
			$game = $defaultEM->getRepository(Game::class)->find($this->getGameId());
			$provisions = $game->getProvisions();
			$notesJoueurs = $game->getNotesJoueurs();
			$notesCampagne = $defaultEM->getRepository(SamuraiXrefNotes::class)->findOneBy(array('samuraiId' => $id));
			$personnages = $detailQuester->getCharacters($notesCampagne);
			$lieux = $detailQuester->getPlaces($notesCampagne);

			return new JsonResponse(array(
				'content' => $this->renderView('blocks/l5a/_notes_campagne.html.twig', array(
					'notesCampagne' => $notesCampagne,
					'personnages' => $personnages,
					'lieux' => $lieux,
					'notes_joueurs' => $notesJoueurs,
					'provisions' => $provisions
				)),
			));

		} else {
			$personnage = $defaultEM->getRepository(Personnage::class)->find($personnageId);
			$personnage->setNom($nom);
			$personnage->setRapport($rapport);
			$personnage->setNotes($notes);
			$defaultEM->merge($personnage);
			$defaultEM->flush();

			return new JsonResponse(array(
				'success' => true,
			));
		}
	}

	public function majLieuAction(Request $request, ?int $id) : JsonResponse {

		$r = $request->request;
		/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();

		/** @var Samurai $samurai */
		$samurai = $this->getSamurai($id, $defaultEM);

		$isNew = false;
		$isNew = $r->get('is_new');
		$lieuId = $r->get('id');
		$nom = $r->get('nom');

		if ($isNew == 'true') {
			$detailQuester = new DetailsQuester($defaultEM, $samurai);

			$lieu = new Lieu();
			$lieu->setNom($nom);
			$defaultEM->persist($lieu);
			$defaultEM->flush();

			$newSamuraiXPlace = new SamuraiXrefPlace();
			$newSamuraiXPlace->setSamuraiId($id)
			                 ->setPlaceId($lieu->getId());
			$defaultEM->merge($newSamuraiXPlace);
			$defaultEM->flush();

			/** @var Game $game */
			$game = $defaultEM->getRepository(Game::class)->find($this->getGameId());
			$provisions = $game->getProvisions();
			$notesJoueurs = $game->getNotesJoueurs();
			$notesCampagne = $defaultEM->getRepository(SamuraiXrefNotes::class)->findOneBy(array('samuraiId' => $id));
			$personnages = $detailQuester->getCharacters($notesCampagne);
			$lieux = $detailQuester->getPlaces($notesCampagne);

			return new JsonResponse(array(
				'content' => $this->renderView('blocks/l5a/_notes_campagne.html.twig', array(
					'notesCampagne' => $notesCampagne,
					'personnages' => $personnages,
					'lieux' => $lieux,
					'provisions' => $provisions,
					'notes_joueurs' => $notesJoueurs
				)),
			));

		} else {
			$lieu = $defaultEM->getRepository(Lieu::class)->find($lieuId);
			$lieu->setNom($nom);
			$defaultEM->merge($lieu);
			$defaultEM->flush();

			return new JsonResponse(array(
				'success' => true,
			));
		}
	}

	public function majObjectifsAction(Request $request, $id) {

		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		$objectifs = $r->get('objectifs');

		try {
			/** @var SamuraiXrefNotes $notesCampagne */
			$notesCampagne = $defaultEM->getRepository(SamuraiXrefNotes::class)->findOneBy(array('samuraiId' => $id));
		} catch (\Exception $e) {
			throw new Error($e);
		}


		$notesCampagne->setObjectifs($objectifs);
		$defaultEM->merge($notesCampagne);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	public function majArmePersoAction(Request $request) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		/** @var ArmePerso $armePerso */
		$armePerso = $defaultEM->getRepository(ArmePerso::class)->find($r->get('id'));

		$champ = $r->get('champ');
		$functionAAppliquer = 'set' . $champ;
		$armePerso->$functionAAppliquer($r->get('valeur'));

		$response = array(
			'success' => true,
		);

		$defaultEM->merge($armePerso);
		$defaultEM->flush();

		return new JsonResponse($response);
	}

	public function equiperArmureAction(Request $request, $id) {
		$r = $request->request;
		/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();

		if ($r->get('id_armure') === null) {
			die('No armor');
		};

		$idArmure = $r->get('id_armure');
		$armure = $defaultEM->getRepository(Armure::class)->findOneBy(array('id' =>$idArmure));

		$qb = $defaultEM->createQueryBuilder();
		$armure = array(
			'nd' => $armure->getNd(),
			'reduction' => $armure->getReduction(),
			'nom' => $armure->getNom(),
			'description' => $armure->getDescription(),
		);

		$samurai = $defaultEM->getRepository(Samurai::class)->findOneBy(array('id'=> $id));
		$samurai->setArmure($idArmure);
		$defaultEM->merge($samurai);
		$defaultEM->flush();

		$response = array(
			'success' => true,
			'armure' => $armure,
		);

		return new JsonResponse($response);
	}

	public function getSamurai(?int $id, ?EntityManager $em) : Samurai {
		$userGameId =
			$id == 10 ?
				5: // Démo
				$this->getGameId();
		/** @var Samurai $samurai */
		$samurai = $em->getRepository(Samurai::class)->find($id);
		if ($samurai === null ) {
			die('Ce samuraï n\'existe pas :( ');
		}
		if ($userGameId != $samurai->getGameId()) {
			die('This is not the samurai you are looking for.');
		} else {
			return $samurai;
		}
	}

	public function supprimerEquipementAction(Request $request, $id) {
    	$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;
		$equipementId = $r->get('id');

		$equipement = $defaultEM->getRepository(Equipement::class)->find($equipementId);

		$defaultEM->remove($equipement);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	public function historiqueXPAction(Request $request) {
		$defaultEM = $this->getDoctrine()->getManager();
		$r = $request->request;
		$allUpdates = $defaultEM->getRepository(HistoriqueXp::class)->findBy(array(
			'idSamurai' => $r->get('samurai_id')),array('id' => 'DESC'));

		$html = $this->renderView('blocks/l5a/_historique_xp.html.twig', array('updates' => $allUpdates));

		return new JsonResponse(
			array(
				'success' => true,
				'html' => $html,
			)
		);
	}

	public function supprimerPersonnageAction(Request $request, ?int $id) : JsonResponse {
    	/** @var EntityManager $defaultEM */
		$defaultEM = $this->getDoctrine()->getManager();
		$samurai = $this->getSamurai($id, $defaultEM);
		$r = $request->request;
		$personnageId = $r->get('id');

		$personnage = $defaultEM->getRepository(Personnage::class)->find($personnageId);
		$defaultEM->remove($personnage);

		$samuraiXcharacter = $defaultEM->getRepository(SamuraiXrefCharacter::class)->findOneBy(
			array('samuraiId' => $id, 'characterId' => $personnageId)
		);
		if ($samuraiXcharacter !== null) {
			$defaultEM->remove($samuraiXcharacter);
		}

		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function supprimerArmeAction(Request $request, $id) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();
		$samurai = $this->getSamurai($id, $defaultEM);
		$armeId = $r->get('id');
		$detailQuester = new DetailsQuester($defaultEM, $samurai);
		$detailQuester->supprimerArmePerso($armeId);

		return new JsonResponse(array(
			'success' => true,
		));
	}
	
	function samurai2PDFAction(Request $request, TranslatorInterface $translator, $id) {
		$html2pdf = new Html2Pdf();
		$defaultEM = $this->getDoctrine()->getManager();

		$samurai = $this->getSamurai($id, $defaultEM);
		$parameters = $this->displayMainAction($translator, $id, false);

		// Initiative
		$initKeep = (int)$samurai->getRang() + (int)$samurai->getReflexes();
		$initiative = (string)($initKeep).'G'.$samurai->getReflexes();
		$initBonus = $this->uppercaseRoll($samurai->getBonusInit());
		$aInitBonus = explode('G', $initBonus);
		$initTotal =
			(string)((int)$aInitBonus[0] + $initKeep).'G'.(string)((int)$aInitBonus[1] + (int)$samurai->getReflexes())
		;
		$parameters['initiative'] = $initiative;
		$parameters['initiative_bonus'] = $initBonus;
		$parameters['initiative_total'] = $initTotal;
		// End Initiative

		// ND
		$nd = (int)$samurai->getReflexes() + 5;
		$ndBonus = (int)$samurai->getBonusNd();
		$ndTotal = $nd + $ndBonus;
		$parameters['nd'] = $initiative;
		$parameters['nd_bonus'] = $ndBonus;
		$parameters['nd_total'] = $ndTotal;
		// End ND

		// Armure
		if ($samurai->getArmure() !== null) {
			$armure = $defaultEM->getRepository(Armure::class)->find($samurai->getArmure());
		}
		$nd = $samurai->getArmure() !== null ? $armure->getNd() : 0;
		$reduction = $samurai->getArmure() !== null ? $armure->getReduction() : 0 ;
		$parameters['armure_nd'] = $nd;
		$parameters['armure_reduction'] = $reduction;
		// End Armure

		// Blessures et Rangs de Blessures
		$parameters['blessures'] = $samurai->getBlessures();
		$terre = $samurai->getTerre();
		$tempRangBlessures = $terre * 5;
		$parameters['rb_1'] = $tempRangBlessures;
		for ($i = 2; $i <= 8; $i++) {
			$tempRangBlessures = $tempRangBlessures + $terre * 2;
			$parameters['rb_'.$i] = $tempRangBlessures;
		}
		// End Blessures

		// Récupération des blessures
		$recuperation = (int)$samurai->getConstitution() + (int)$samurai->getRang();
		$recuperationBonus = (int)$samurai->getBonusRecup();
		$recuperationTotal = $recuperation + $recuperationBonus;
		$parameters['recuperation'] = $recuperation;
		$parameters['recuperation_bonus'] = $recuperationBonus;
		$parameters['recuperation_total'] = $recuperationTotal;
		// End Récupération des blessures

		// Armes


		$vue = $this->render('main/l5a/samurai_pdf.html.twig', $parameters)->getContent();
//		$html2pdf->setDefaultFont('Arsenal', null, 'http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic|Arsenal:300,400,400italic,600,700');
		$html2pdf->writeHTML($vue);
		$html2pdf->output();
	}

	private function uppercaseRoll($roll) {
		return str_replace('g', 'G', $roll);
	}

	function editerAvantageAction(Request $request, $id) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();

		$avantagePerso = $defaultEM->getRepository(AvantageXSamurai::class)->find($r->get('avantage_id'));
		$valeur = $r->get('valeur');
		if ($r->get('attribut') === 'resume') {
			$avantagePerso->setResume($valeur);
		} else {
			$avantagePerso->setEstCache($valeur);
		}
		$defaultEM->merge($avantagePerso);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function editerDesavantageAction(Request $request, $id) {
		$r = $request->request;
		$defaultEM = $this->getDoctrine()->getManager();
		$samurai = $this->getSamurai($id, $defaultEM);

		$desavantagePerso = $defaultEM->getRepository(DesavantageXSamurai::class)->find($r->get('desavantage_id'));
		$valeur = $r->get('valeur');
		if ($r->get('attribut') === 'resume') {
			$desavantagePerso->setResume($valeur);
		} else {
			$desavantagePerso->setEstCache($valeur);
		}
		$defaultEM->merge($desavantagePerso);
		$defaultEM->flush();

		return new JsonResponse(array(
			'success' => true,
		));
	}

	function majProvisionsAction(Request $request, Pusher $pusher) {
		$defaultEM = $this->getDoctrine()->getManager();
		$gameId = $this->getGameId();
		$game = $defaultEM->getRepository(Game::class)->find($gameId);
		$r = $request->request;
		$provisions = $r->get('provisions');

		$game->setProvisions($provisions);
		$defaultEM->merge($game);
		$defaultEM->flush();

		// Envoyer pusher
		/** @var \Pusher $pusher */
		//$pusher = $this->get('lopi_pusher.pusher');
		$logger = new MyLogger();
		//		$pusher->set_logger($logger);
		$pusher->trigger('game-'.$gameId, 'maj-provisions', $game->getProvisions());

		return new JsonResponse(
			array(
				'success' => true,
				'message' => $pusher->get_channel_info('game-'.$gameId),
			)
		);
	}

	function majNotesJoueursAction(Request $request, Pusher $pusher) {
		$defaultEM = $this->getDoctrine()->getManager();
		$gameId = $this->getGameId();
		$game = $defaultEM->getRepository(Game::class)->find($gameId);
		$r = $request->request;
		$notesJoueurs = $r->get('notes_joueurs');

		$game->setNotesJoueurs($notesJoueurs);
		$defaultEM->merge($game);
		$defaultEM->flush();

		// Envoyer pusher
		/** @var \Pusher $pusher */
//		$pusher = $this->get('lopi_pusher.pusher');
		$logger = new MyLogger();
		//		$pusher->set_logger($logger);
		$pusher->trigger('game-'.$gameId, 'maj-notes-joueurs', $game->getNotesJoueurs());

		return new JsonResponse(
			array(
				'success' => true,
				'message' => $pusher->get_channel_info('game-'.$gameId),
			)
		);
	}

	function getXPAction (Request $request, ?int $samuraiId) :JsonResponse {
    	try {
		    return new JsonResponse(
			    array(
				    'success' => true,
				    'xp' => $this->getDoctrine()->getManager()->getRepository(Samurai::class)->find(
					    $samuraiId
				    )->getXPRestant()
			    )
		    );
	    } catch (\Exception $e) {
    		throw new \Error($e);
	    }
	}
}