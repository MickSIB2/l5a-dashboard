<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\L5A\Samurai;
use Symfony\Component\Translation\Translator;

class FatherController extends Controller {
	
	/*
	 * This function gets the game id first from the character if it's defined,
	 * from the session next.
	 * If the gameId is not in the session, redirect to Mes Parties
	 */
	public function getGameId ($samuraiId = null) {
		try {
			if ($samuraiId !== null and is_int($samuraiId)) {
				$defaultEM = $this->getDoctrine()->getManager();
				$samurai = $defaultEM->getRepository(Samurai::class)->find($samuraiId);

				return $samurai->getGameId();
			} else {
				if ($this->get('session')->get('gameId')) {
					return $this->get('session')->get('gameId');
				}
			}

			// If there is no samurai and the gameID is not in the session, bring back to index
			//$this->redirectToRoute('homepage');
			header('Location: '.$this->generateUrl('homepage'));
			die();
		} catch (Exception $e) {
			header('Location: '.$this->generateUrl('homepage'));
			die();
		}
	}
	
	public function getTranslator() {
		$translator = new Translator('fr_FR');
		return $translator;
	}

	public function ajaxError(?string $message) : JsonResponse {
		return new JsonResponse(array(
			'success' => false,
			'message' => $message
		));
	}
}