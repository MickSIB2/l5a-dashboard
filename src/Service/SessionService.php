<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 04/06/2019
 * Time: 16:33
 */
 
 namespace App\Service;
 
 use Symfony\Component\HttpFoundation\Session\Session;
 use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Entity\L5A\AppUser;
use App\Entity\L5A\Game;
use Error;
 
 // Singleton
 class SessionService {
	 
	 private static $_instance = null;
	 private $session;
	 private $gameId;
	 private $samuraiId;
	 private $isGm = false;
	 
	 private function _construct() { } // Prevents any outside instantiation of this class
	 
	 private function _clone() { } // Prevents any object or instance of that class to be cloned
	 
	 public static function getInstance(SessionInterface $session = null) {
		 if (!is_object(self::$_instance)) {
			 self::$_instance = new SessionService();
			 self::$_instance->session = $session;
		 }
		 return self::$_instance;
	 }
	 
	 static function sessionStart() {
		 $this->session = new Session();
		 $this->session->start();
	 }
	 	 	
	public function isGM(AppUser $user, Game $game) : bool {
		return $user->hasRole('ROLE_GM') && $game->getGamemaster() === $user->getId();
	}
	
	public function setIsGm(AppUser $user, Game $game) : bool {
		//dnd(self::$_instance);
		if (self::$_instance->session->get('isGm') === null) {
			try {
				//self::$_instance->isGm = $this->isGm($user, $game);
				self::$_instance->session->set('isGm', $this->isGm($user, $game));
			} catch (\Exception $e) {
				throw new Error($e->getMessage());
			}
		}
		return true;
	}
	
	public function removeIsGm() : bool {
		try {
			//self::$_instance->isGm = null;
			self::$_instance->session->set('isGm', null);
		} catch (\Exception $e) {
			throw new Error($e->getMessage());
		}
		return true;
	}
	
	public function getIsGm() : bool {
		return self::$_instance->session->get('isGm') || false;
	}
	
	public function getSession() : SessionInterface {
		return self::$_instance->session;
	}
	
	public function setGameId(int $gameId) : bool {
		self::$_instance->gameId = $gameId;
		try {
			self::$_instance->session->set('gameId', $gameId);
		} catch (\Exception $e) {
			throw new Error($e->getMessage());
		}
		return true;
	}
	
	public function getGameId() : int {
		if (self::$_instance->session->gameId !== null)
			return self::$_instance->session->get('gameId');
		else return 0;
	}
 }