<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 29/01/2017
 * Time: 22:00
 */

namespace App\Service\L5A;

use App\Entity\L5A\PouvoirsOutremonde;
use App\Entity\L5A\SamuraiXrefCharacter;
use App\Entity\L5A\SamuraiXrefPlace;
use App\Entity\L5A\SamuraiXrefArrow;
use App\Entity\L5A\SamuraiXrefKata;
use App\Entity\L5A\SamuraiXrefSkill;
use App\Entity\L5A\SamuraiXrefSpell;
use App\Entity\L5A\ArmePerso;
use App\Entity\L5A\Armure;
use App\Entity\L5A\Avantage;
use App\Entity\L5A\AvantageXSamurai;
use App\Entity\L5A\Clan;
use App\Entity\L5A\Competence;
use App\Entity\L5A\Desavantage;
use App\Entity\L5A\DesavantageXSamurai;
use App\Entity\L5A\Ecole;
use App\Entity\L5A\Equipement;
use App\Entity\L5A\AppUser;
use App\Entity\L5A\Game;
use App\Entity\L5A\HistoriqueXp;
use App\Entity\L5A\Kata;
use App\Entity\L5A\SamuraiXrefNotes;
use App\Entity\L5A\Samurai;
use App\Entity\L5A\SamuraiXrefTechnique;
use App\Entity\L5A\Sort;
use App\Entity\L5A\Technique;
use App\Service\DatabaseUpdater;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Component\Translation\Translator;

define('SHUGENJA_TYPE', 'samurai.type.shugenja');
define('MONK_TYPE', 'samurai.type.monk');

class DetailsQuester {
	/** @var
	 * Samurai
	 */
	private $samurai;
	/** @var  EntityManager */
	private $em;
	private $gameId;
	/** @var Translator  */
	private $translator;

	/**
	 * DetailsQuester constructor.
	 * @param EntityManager $em
	 * @param Samurai $samurai
	 */
	public function __construct($em, $samurai = null, $gameId = null, $translator = null) {
		$this->samurai = $samurai;
		$this->em = $em;
		$this->gameId = $gameId;
		$this->translator = $translator;
	}

	/**
	 * @return array =>
		 * 'competence' => $thisCompetence,
			'rang' => $rang,
			'specialites' => $specialites,
			'jet' => $jet,
	 */
	public function getCompetences () :array {
		$samurai = $this->samurai;
		$em = $this->em;
		if (!empty($samurai->getCompetences())) {
			DatabaseUpdater::saveCompetences($em, $samurai); // Add them in the right format
		}
		$aSkills = $em->getRepository(SamuraiXrefSkill::class)->getSamuraiSkills($em, $samurai->getId());
		// Calculate the rolls
		foreach ($aSkills as $key => $oneSkill) {
			/** @var Competence $oSkill */
			$oSkill = $oneSkill[0];
			$aSkills[$key]['competence'] = $oSkill;
			$trait = $oSkill->getTrait();
			if ($trait === 'Variable' || strpos($trait, ' ') !== false) {
				$aSkills[$key]['jet'] = $trait;
			} else {
				$traitSamurai = $this->getValeurTrait(explode('.', $trait)[2], $samurai); // deconstruct the translation key
				$aSkills[$key]['jet'] = $traitSamurai + $oneSkill['rang'].'G'.$traitSamurai;
			}
		}

		return $aSkills;
	}

	// Do not flush
	public function setSkills(?string $string) : bool {
		$samurai = $this->samurai;
		$em = $this->em;

		$competences = explode(';', $string);
		foreach ($competences as $competence) {
			if (!is_null($competence)) {
				$competenceDetails = explode(',', $competence);
				$samuraiXSkills = new SamuraiXrefSkill();
				//showme($competenceDetails[1]);
				$samuraiXSkills
					->setSamuraiId($samurai->getId())
					->setSkillId((int)$competenceDetails[0])
					->setRank((int)$competenceDetails[1])
					->setSpecialities(isset($competenceDetails[2]) ? $competenceDetails[2] : '');
				try {
					$em->persist($samuraiXSkills);
				} catch (ORMException $e) {
					die($e);
				}
			}
		}
		return true;
	}

	public function getArmes() : array {
		$samurai = $this->samurai;
		/** @var EntityManager $em */
		$em = $this->em;

		$armes = $em->getRepository(ArmePerso::class)->getArmesPerso($em, $samurai->getId());
		$armesArray = array();

		foreach ($armes as $oneArme) {
			/** @var ArmePerso $armePerso */
			$armesArray[] = array(
				'armeGenerique' => $oneArme[0],
				'armePerso' => array(
					'id' => $oneArme['id'],
					'nom' => $oneArme['nom'],
					'filtreAttaque' => $oneArme['filtreAttaque'],
					'filtreDegats'  => $oneArme['filtreDegats'],
					'portee'        => $oneArme['portee'],
					'notes'         => $oneArme['notes'],
				),
			);
		}
		return $armesArray;
	}

	/**
	 *  $full Indique si le tableau est peuplé d'Avantages ou d'ids
	 */
	public function getAvantages(?bool $full = true) : array {
		$samurai = $this->samurai;
		$defaultEM = $this->em;
		$avantagesPerso = $defaultEM->getRepository(AvantageXSamurai::class)->getSamuraiAdvantages($defaultEM, $samurai->getId());
		$avantagesArray = array();

		if (sizeof($avantagesPerso) === 0) {
			return $avantagesArray;
		}
		/** @var AvantageXSamurai $avantagePerso */
		foreach ($avantagesPerso as $avantagePerso) {
			if ($avantagePerso === null) {
				continue;
			}
			if ($full) {
				$avantage = array(
					'perso' => array(
						'id'        => $avantagePerso['id'],
						'resume'    => $avantagePerso['resume'],
						'estCache'  => $avantagePerso['estCache'],
					),
					'vanilla' => $avantagePerso[0],
					'idVanilla' => $avantagePerso[0]->getId(),
				);
			}
			$avantagesArray[] =
				$full ?
					$avantage : $avantagePerso[0]->getId()
			;
		}

		return $avantagesArray;
	}

	/**
	 *  $full Indique si le tableau est peuplé de Desavantages ou d'ids
	 */
	public function getDesavantages(?bool $full = true) : array {
		$samurai = $this->samurai;
		$defaultEM = $this->em;
		$desavantagesPerso = $defaultEM->getRepository(DesavantageXSamurai::class)->getSamuraiDisadvantages($defaultEM, $samurai->getId());
		$desavantagesArray = array();

		if (sizeof($desavantagesPerso) === 0) {
			return $desavantagesArray;
		}
		/** @var DesavantageXSamurai $avantagePerso */
		foreach ($desavantagesPerso as $desavantagePerso) {
			if ($desavantagePerso === null) {
				continue;
			}
			if ($full) {
				$desavantage = array(
					'perso' => array(
						'id'        => $desavantagePerso['id'],
						'resume'    => $desavantagePerso['resume'],
						'estCache'  => $desavantagePerso['estCache'],
					),
					'vanilla' => $desavantagePerso[0],
					'idVanilla' => $desavantagePerso[0]->getId(),
				);
			}
			$avantagesArray[] =
				$full ?
					$desavantage : $desavantagePerso[0]->getId()
			;
		}

		return $avantagesArray;
	}
	
	public function getArmure() {
		$samurai = $this->samurai;
		$defaultEM = $this->em;
		if ($samurai->getArmure() != null) {
			$armure = $defaultEM->getRepository(Armure::class)->find($samurai->getArmure());
		} else {
			$armure = null;
		}

		return $armure;
	}

	public function getKatas() : array {
		$em = $this->em;
		/** Samurai $samurai */
		$samurai = $this->samurai;
		if($samurai->getType() !== SHUGENJA_TYPE && $samurai->getType() !== MONK_TYPE) {
			return $em->getRepository(SamuraiXrefKata::class)
			          ->getSamuraiKatas($em, $samurai->getId());
		} else {
			return array();
		}
	}

	public function getSorts() :array {
		$em = $this->em;
		/** Samurai $samurai */
		$samurai = $this->samurai;
		if($samurai->getType() === SHUGENJA_TYPE) {
			if (!empty($samurai->getSorts())) {
				DatabaseUpdater::saveSpells($em, $samurai, SHUGENJA_TYPE);
			}
			return $em->getRepository(SamuraiXrefSpell::class)
			          ->getSamuraiSpells($em, $samurai->getId());
		} else {
			return array();
		}
	}


	public function getCharacters(?SamuraiXrefNotes $notesCampagne = null) :array {
		$em = $this->em;
		if ($notesCampagne !== null && !empty($notesCampagne->getPersonnagesRencontres())) {
			DatabaseUpdater::saveCharacters($em, $notesCampagne, $this->samurai->getId());
		}
		return $em->getRepository(SamuraiXrefCharacter::class)
		          ->getSamuraiCharacters($em, $this->samurai->getId());
	}


	public function getPlaces(?SamuraiXrefNotes $notesCampagne = null) :array {
		$em = $this->em;
		if ($notesCampagne !== null && !empty($notesCampagne->getEndroitsVisites())) {
			DatabaseUpdater::savePlaces($em, $notesCampagne, $this->samurai->getId());
		}
		return $em->getRepository(SamuraiXrefPlace::class)
			->getSamuraiPlaces($em, $this->samurai->getId());
	}

	public function getFleches() {
		$samurai = $this->samurai;
		$defaultEM = $this->em;
		if ($samurai->getFlechesRestantes() != null || !empty($samurai->getFlechesRestantes())) {
			DatabaseUpdater::saveArrows($defaultEM, $samurai);
		}
		$arrows = $defaultEM->getRepository(SamuraiXrefArrow::class)->getArrows($defaultEM, $samurai->getId());

		$flechesArray = array();

		foreach ($arrows as $oneArrow) {
			$flechesArray[$oneArrow[0]->getId()] = array(
				'fleche' => $oneArrow[0],
				'qte' => $oneArrow['quantity'],
			);
		}
		return $flechesArray;
	}

	public function getTechniques() : array {
		$em = $this->em;
		$samurai = $this->samurai;

		if($samurai->getTechnique1() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,1, $samurai->getTechnique1());
		}
		if($samurai->getTechnique2() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,2, $samurai->getTechnique2());
		}
		if($samurai->getTechnique3() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,3, $samurai->getTechnique3());
		}
		if($samurai->getTechnique4() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,4, $samurai->getTechnique4());
		}
		if($samurai->getTechnique5() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,5, $samurai->getTechnique5());
		}
		if($samurai->getTechnique6() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,6, $samurai->getTechnique6());
		}
		if($samurai->getTechnique7() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,7, $samurai->getTechnique7());
		}
		if($samurai->getTechnique8() !== null) {
			DatabaseUpdater::saveTechnique($em, $samurai,8, $samurai->getTechnique8());
		}

		return $em->getRepository(SamuraiXrefTechnique::class)->getTechniques($em, $samurai->getId());
	}



	public function setFleche($flecheId, $nb) {
		/** @var Samurai $samurai */
		$samurai = $this->samurai;
		$em = $this->em;
		$samuraiXArrow = $em->getRepository(SamuraiXrefArrow::class)->findOneBy(
			array('samuraiId' => $samurai->getId(), 'arrowId' => $flecheId)
		);

		if ($samuraiXArrow === null) {
			$samuraiXArrow = new SamuraiXrefArrow();
			$samuraiXArrow->setSamuraiId($samurai->getId())
			              ->setArrowId($flecheId)
			              ->setQuantity(0);
		}
		$samuraiXArrow->setQuantity($samuraiXArrow->getQuantity() + $nb);
		$em->persist($samuraiXArrow);
	}

	public function majMonnaieAction(?int $sommeZeni) : array {

		$samurai = $this->samurai;
		$monnaieRestanteZeni = $samurai->getKoku()* 50 + $samurai->getBu()*10 + $samurai->getZeni();
		$sommeZeni = $monnaieRestanteZeni - (int)$sommeZeni;

		if ($sommeZeni < 0) {
			throw new \Exception('Le samurai n\' a pas assez d\'argent pour acheter cela.');
		}
		$zeni = 0;
		$bu = 0;
		$koku = 0;

		if (strlen($sommeZeni) >= 3) {
			$koku = (int)(substr($sommeZeni, 0, strlen($sommeZeni) - 2) * 2);
		}
		if (strlen($sommeZeni) >= 2) {
			$bu = (int)(substr($sommeZeni, -2, 1));
			$zeni = (int)(substr($sommeZeni, -1, 1));
		} else {
			$zeni = (int)$sommeZeni;
		}

		$samurai->setKoku($koku);
		$samurai->setBu($bu);
		$samurai->setZeni($zeni);
		// Doesn't merge

		return [$koku, $bu, $zeni];
	}

	public function majReputation() {
		$samurai = $this->samurai;
		$defaultEM = $this->em;

		$sommeAnneaux = ($samurai->getTerre() + $samurai->getAir() + $samurai->getEau() + $samurai->getFeu() + $samurai->getVide()) *10;
		$sommeCompetences = 0;
		foreach ($this->getCompetences() as $competence) {
			$sommeCompetences += $competence['rang'];
		}

		$totalReputation = $sommeAnneaux + $sommeCompetences;

		if ($totalReputation != $samurai->getReputation()) {
			$samurai->setReputation($totalReputation);

			switch ($totalReputation) {
				case (1 <= $totalReputation && $totalReputation <= 149):
					$samurai->setRang(1);
					break;
				case (150 <= $totalReputation && $totalReputation <= 174):
					$samurai->setRang(2);
					break;
				case (175 <= $totalReputation && $totalReputation <= 199):
					$samurai->setRang(3);
					break;
				case (200 <= $totalReputation && $totalReputation <= 224):
					$samurai->setRang(4);
					break;
				case (225 <= $totalReputation && $totalReputation <= 249):
					$samurai->setRang(5);
					break;
				case (250 <= $totalReputation && $totalReputation <= 274):
					$samurai->setRang(6);
					break;
				case (275 <= $totalReputation && $totalReputation <= 299):
					$samurai->setRang(7);
					break;
				case (300 <= $totalReputation && $totalReputation <= 324):
					$samurai->setRang(8);
					break;
				case (350 <= $totalReputation && $totalReputation <= 349):
					$samurai->setRang(9);
					break;
				case (375 <= $totalReputation && $totalReputation <= 399):
					$samurai->setRang(10);
					break;
			}

			$defaultEM->merge($samurai);
			$defaultEM->flush();
		}
	}
	
	public function creerItem($nomItem, $idArmure = null) {
		$nouvelItem = new Equipement();
	    $nouvelItem->setNom($nomItem);
	    $nouvelItem->setSamurai($this->samurai->getId());
		if ($idArmure !== null) {
			$nouvelItem->setIdArmure($idArmure);
		}
	    $this->em->persist($nouvelItem);
	    $this->em->flush();

//	    $this->samurai->setEquipement($this->samurai->getEquipement().','.$nouvelItem->getId());
//	    $this->em->merge($this->samurai);
//	    $this->em->flush();
	}
	
	public function creerArmePerso($nomArme, $idArme, $flushSamuraiAfterwards = true) {
//		$armesPossedees = explode(',', $this->samurai->getArmes());
		
		$nouvelleArmePerso = new ArmePerso();
		$nouvelleArmePerso->setNom($nomArme);
		$nouvelleArmePerso->setArmeId($idArme);
		$nouvelleArmePerso->setFiltreAttaque('0G0');
		$nouvelleArmePerso->setFiltreDegats('0G0');
		$nouvelleArmePerso->setSamurai($this->samurai->getId());

	    $this->em->persist($nouvelleArmePerso);
	    $this->em->flush();
		
//		array_push($armesPossedees, $nouvelleArmePerso->getId());
//		$this->samurai->setArmes(implode(',', $armesPossedees));
		
		if ($flushSamuraiAfterwards) {
			$this->em->merge($this->samurai);
			$this->em->flush();
		}
	}

	public function supprimerArmePerso($idArme, $flushSamuraiAfterwards = true) {
		$armePerso = $this->em->getRepository(ArmePerso::class)->find($idArme);
		$this->em->remove($armePerso);

		if ($flushSamuraiAfterwards) {
			$this->em->merge($this->samurai);
		}
		$this->em->flush();
	}

	public function getStuff($type, $valeur, $asHTML = false) {
		$sRepository = 'App\\Entity\\L5A\\';
		$translator = $this->translator;

		if ($asHTML === false) {
			$entities = $this->em->getRepository($sRepository)->findAll();
			return $entities;
		}

		$sHTML = '';
		$champ = '';
		switch ($type) {
			case 'Type':
				switch ($valeur) {
					case 'Technique':
						$sRepository .= 'Clan';
						$champ = 'Clan';
						$entities = $this->em->getRepository($sRepository)->findBy(array(), array('nom' => 'ASC'));
							/** @var Clan $clan */
						foreach($entities as $clan) {
							$sHTML .= '<option value="'.$clan->getId().'">'.
								$translator->trans($clan->getNom(), array(), 'clans').'</option>';
						}
						break;
					case 'Sort':
						$sRepository .= 'Sort';
						$champ = 'Sorts';
						$entities = $this->em->getRepository($sRepository)->findAll();
						/** @var Sort $sort */
						foreach($entities as $sort) {
							$sHTML .= '<option value="'.$sort->getId().'" data-rang="'. $sort->getMaitrise() .'" 
							data-anneau="'. $translator->trans($sort->getAnneau(), array(), 'spells') .'" 
							data-description="'. $translator->trans($sort->getDescription(), array(), 'spells') .'">'.
							$translator->trans($sort->getNom(), array(), 'spells').'</option>';
						}
						break;
					case 'Avantage':
						$sRepository .= 'Avantage';
						$champ = 'Nom';
						$query = $this->em->createQuery(
							'SELECT a
							FROM App\Entity\L5A\Avantage a
							INDEX BY a.id
							WHERE a.gameId = ' .$this->gameId. '
							OR a.gameId IS NULL'
						);
						$entities = $query->getResult();
						//$entities = $this->em->getRepository($sRepository)->findBy(array(), array('nom' => 'ASC'));
						/** @var Avantage $avantage */
						foreach($entities as $avantage) {
							$sHTML .= '<option value="'.$avantage->getId().'" 
							data-description="'.$translator->trans($avantage->getDescription(), array(), 'advantages').'">'
								.$translator->trans($avantage->getNom(), array(), 'advantages').'</option>';
						}
						break;
					case 'Desavantage':
						$sRepository .= 'Desavantage';
						$champ = 'Nom';
						$query = $this->em->createQuery(
							'SELECT a
							FROM App\Entity\L5A\Desavantage a
							INDEX BY a.id
							WHERE a.gameId = ' .$this->gameId. '
							OR a.gameId IS NULL'
						);
						$entities = $query->getResult();
						//$entities = $this->em->getRepository($sRepository)->findBy(array(), array('nom' => 'ASC'));
						/** @var Desavantage $desavantage */
						foreach($entities as $desavantage) {
							$sHTML .= '<option value="'.$desavantage->getId().'" 
							data-description="'.$translator->trans($desavantage->getDescription(), array(), 'disadvantages').'">'
								.$translator->trans($desavantage->getNom(), array(), 'disadvantages').'</option>';
						}
						break;
					case 'Kata':
						$sRepository .= 'Kata';
						$champ = 'Nom';
						$entities = $this->em->getRepository($sRepository)->findAll();
						/** @var Kata $kata */ 
						foreach ($entities as $kata) {
							$sHTML .= '<option value="' . $kata->getId() . '" 
							data-description="' . $translator->trans($kata->getEffet())
							. "\n" . $translator->trans($kata->getAnneau()) . '/' . $kata->getMaitrise() .' - ecole : '.
								$kata->getEcoles() . '">' . $translator->trans($kata->getNom()) . '</option>';
						}
						break;
					case 'PouvoirOutremonde':
						$sRepository .= 'Pouvoir';
						$champ = 'Nom';
						$entities = $this->em->getRepository($sRepository)->findAll();
						/** @var PouvoirsOutremonde $pouvoir */
						foreach ($entities as $pouvoir) {
							$sHTML .= '<option value="' . $pouvoir->getId() . '" 
							data-description="' . $translator->trans($pouvoir->getDescription()) .'">' .
							$translator->trans($pouvoir->getNom()) . '</option>';
						}
						break;
				}
				break;
			case 'Clan':
				$sRepository .= 'Ecole';
				$champ = 'Ecole';
				$entities = $this->em->getRepository($sRepository)->findByClanId($valeur);
				/** @var Ecole $ecole */
				foreach($entities as $ecole) {
					$sHTML .= '<option value="'.$ecole->getId().'">'.
						$translator->trans($ecole->getNom(), array(), 'schools').'</option>';
				}
				break;
			case 'Ecole':
				$champ = 'Nom';

				$query = $this->em->createQuery(
					'SELECT t.id, t.nom, t.description
					FROM App\Entity\L5A\Technique t, App\Entity\L5A\Ecole e
					WHERE e.id = ' . $valeur . '
					AND (
					 t.id = e.technique1Id
					 OR t.id = e.technique2Id
					 OR t.id = e.technique3Id
					 OR t.id = e.technique4Id
					 OR t.id = e.technique5Id
					)'
				);

				$entities = $query->getResult();
				/** @var Technique $technique */
				foreach($entities as $technique) {
					$sHTML .= '<option value="'.
						$translator->trans($technique['nom'], array(), 'techniques').'" 
						data-description="'.$translator->trans($technique['description'], array(), 'techniques').'">'
						.$translator->trans($technique['nom'], array(), 'techniques').'</option>';
				}
				break;
			/*case 'Anneau':
				$champ = 'Maitrise';
				$sRepository .= 'Sort';
				$entities = $this->em->getRepository($sRepository)->findBy(array('anneau' => $valeur), array('nom' => 'ASC'));
				/** @var Sort $sort *
				foreach($entities as $sort) {
					$sHTML .= '<option value="'.$sort->getNom().'" data-description="'.$sort->getDescription().'">'.$sort->getNom().'</option>';
				}
				break;				
			case 'Maitrise':
				$champ = 'Nom';
				$sRepository .= 'Sort';
				$entities = $this->em->getRepository($sRepository)->findBy(array('anneau' => $valeur), array('nom' => 'ASC'));
				/** @var Sort $sort *
				foreach($entities as $sort) {
					$sHTML .= '<option value="'.$sort->getNom().'" data-description="'.$sort->getDescription().'">'.$sort->getNom().'</option>';
				}
				break;*/
		}

		return array($champ, $sHTML);
	}

	/**
	* Permet de sauvegarder la modification d'xp.
	* @param $signe: + ou -, selon si le perso a gagn2 ou perdu de l'xp
	* @param $xp: La quantit2 d'xp ajout2e ou retir2e
	* @param $type: Avantage, Comp2tence...
	* @param $nom: Si $type est d2fini, alors $nom est le nom de l'Avantage, Comp2tence...
	**/
	public function setXPRestant($signe, $xp, $type = null, $nom = null) {
		$samurai = $this->samurai;
		$defaultEM = $this->em;
		$xpAnciens = $samurai->getXpRestant();
		$nouvelXPRestant = $signe === '+' ?
			$xpAnciens + $xp :
			$xpAnciens - $xp;

		$updateXP = new HistoriqueXp();
		$updateXP->setIdSamurai($samurai->getId());
		$updateXP->setSigne($signe);
		$updateXP->setValeur($xp);
		$updateXP->setDate(date('d-m-Y'));
		$updateXP->setType($type);
		$updateXP->setNom($nom);
		$defaultEM->persist($updateXP);

		$samurai->setXpRestant($nouvelXPRestant);
	}

	/**
	 * @param $trait est la dernière partie de la clé de traduction ie samurai.trait.perception
	 * @param Samurai $samurai
	 */
	private function getValeurTrait($trait, $samurai) {
		switch ($trait) {
			case 'perception':
				return $samurai->getPerception();
				break;
			case 'strength':
				return $samurai->getForce();
				break;
			case 'agility':
				return $samurai->getAgilite();
				break;
			case 'intelligence':
				return $samurai->getIntelligence();
				break;
			case 'will':
				return $samurai->getVolonte();
				break;
			case 'constitution':
				return $samurai->getConstitution();
				break;
			case 'reflexes':
				return $samurai->getReflexes();
				break;
			case 'awareness':
				return $samurai->getIntuition();
				break;
			case 'void':
				return $samurai->getVide();
				break;
		}
	}
				
}