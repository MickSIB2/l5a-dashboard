<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ecole
 *
 * @ORM\Table(name="ecole")
 * @ORM\Entity
 */
class Ecole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=false)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="technique_1_id", type="integer", nullable=false)
     */
    private $technique1Id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_2_id", type="integer", nullable=true)
     */
    private $technique2Id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_3_id", type="integer", nullable=true)
     */
    private $technique3Id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_4_id", type="integer", nullable=true)
     */
    private $technique4Id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="technique_5_id", type="integer", nullable=true)
     */
    private $technique5Id;

    /**
     * @var string
     *
     * @ORM\Column(name="trait_bonus", type="string", length=50, nullable=false)
     */
    private $traitBonus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var int|null
     *
     * @ORM\Column(name="clan_id", type="integer", nullable=true)
     */
    private $clanId;

    /**
     * @var float|null
     *
     * @ORM\Column(name="honneur_depart", type="float", precision=10, scale=0, nullable=true)
     */
    private $honneurDepart;

    /**
     * @var string|null
     *
     * @ORM\Column(name="competences", type="text", length=65535, nullable=true)
     */
    private $competences;

    /**
     * @var string|null
     *
     * @ORM\Column(name="competences_affichees", type="text", length=65535, nullable=true)
     */
    private $competencesAffichees;

    /**
     * @var string|null
     *
     * @ORM\Column(name="affinite", type="string", length=255, nullable=true)
     */
    private $affinite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deficience", type="string", length=255, nullable=true)
     */
    private $deficience;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sorts", type="string", length=255, nullable=true)
     */
    private $sorts;

    /**
     * @var string|null
     *
     * @ORM\Column(name="equipement", type="string", length=255, nullable=true)
     */
    private $equipement;

    /**
     * @var bool
     *
     * @ORM\Column(name="personnalisee", type="boolean", nullable=false)
     */
    private $personnalisee = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTechnique1Id(): ?int
    {
        return $this->technique1Id;
    }

    public function setTechnique1Id(int $technique1Id): self
    {
        $this->technique1Id = $technique1Id;

        return $this;
    }

    public function getTechnique2Id(): ?int
    {
        return $this->technique2Id;
    }

    public function setTechnique2Id(?int $technique2Id): self
    {
        $this->technique2Id = $technique2Id;

        return $this;
    }

    public function getTechnique3Id(): ?int
    {
        return $this->technique3Id;
    }

    public function setTechnique3Id(?int $technique3Id): self
    {
        $this->technique3Id = $technique3Id;

        return $this;
    }

    public function getTechnique4Id(): ?int
    {
        return $this->technique4Id;
    }

    public function setTechnique4Id(?int $technique4Id): self
    {
        $this->technique4Id = $technique4Id;

        return $this;
    }

    public function getTechnique5Id(): ?int
    {
        return $this->technique5Id;
    }

    public function setTechnique5Id(?int $technique5Id): self
    {
        $this->technique5Id = $technique5Id;

        return $this;
    }

    public function getTraitBonus(): ?string
    {
        return $this->traitBonus;
    }

    public function setTraitBonus(string $traitBonus): self
    {
        $this->traitBonus = $traitBonus;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getClanId(): ?int
    {
        return $this->clanId;
    }

    public function setClanId(?int $clanId): self
    {
        $this->clanId = $clanId;

        return $this;
    }

    public function getHonneurDepart(): ?float
    {
        return $this->honneurDepart;
    }

    public function setHonneurDepart(?float $honneurDepart): self
    {
        $this->honneurDepart = $honneurDepart;

        return $this;
    }

    public function getCompetences(): ?string
    {
        return $this->competences;
    }

    public function setCompetences(?string $competences): self
    {
        $this->competences = $competences;

        return $this;
    }

    public function getCompetencesAffichees(): ?string
    {
        return $this->competencesAffichees;
    }

    public function setCompetencesAffichees(?string $competencesAffichees): self
    {
        $this->competencesAffichees = $competencesAffichees;

        return $this;
    }

    public function getAffinite(): ?string
    {
        return $this->affinite;
    }

    public function setAffinite(?string $affinite): self
    {
        $this->affinite = $affinite;

        return $this;
    }

    public function getDeficience(): ?string
    {
        return $this->deficience;
    }

    public function setDeficience(?string $deficience): self
    {
        $this->deficience = $deficience;

        return $this;
    }

    public function getSorts(): ?string
    {
        return $this->sorts;
    }

    public function setSorts(?string $sorts): self
    {
        $this->sorts = $sorts;

        return $this;
    }

    public function getEquipement(): ?string
    {
        return $this->equipement;
    }

    public function setEquipement(?string $equipement): self
    {
        $this->equipement = $equipement;

        return $this;
    }

    public function getPersonnalisee(): ?bool
    {
        return $this->personnalisee;
    }

    public function setPersonnalisee(bool $personnalisee): self
    {
        $this->personnalisee = $personnalisee;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }


}
