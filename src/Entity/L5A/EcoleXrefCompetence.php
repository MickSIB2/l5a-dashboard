<?php

namespace App\Entity\L5A;

/**
 * EcoleXrefCompetence
 */
class EcoleXrefCompetence
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $ecoleId = '0';

    /**
     * @var integer
     */
    private $competenceId = '0';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ecoleId
     *
     * @param integer $ecoleId
     *
     * @return EcoleXrefCompetence
     */
    public function setEcoleId($ecoleId)
    {
        $this->ecoleId = $ecoleId;

        return $this;
    }

    /**
     * Get ecoleId
     *
     * @return integer
     */
    public function getEcoleId()
    {
        return $this->ecoleId;
    }

    /**
     * Set competenceId
     *
     * @param integer $competenceId
     *
     * @return EcoleXrefCompetence
     */
    public function setCompetenceId($competenceId)
    {
        $this->competenceId = $competenceId;

        return $this;
    }

    /**
     * Get competenceId
     *
     * @return integer
     */
    public function getCompetenceId()
    {
        return $this->competenceId;
    }
}
