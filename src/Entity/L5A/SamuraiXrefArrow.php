<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\L5A\SamuraiXrefArrowRepository")
 */
class SamuraiXrefArrow
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $samuraiId;

    /**
     * @ORM\Column(type="integer")
     */
    private $arrowId;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSamuraiId(): ?int
    {
        return $this->samuraiId;
    }

    public function setSamuraiId(int $samuraiId): self
    {
        $this->samuraiId = $samuraiId;

        return $this;
    }

    public function getArrowId(): ?int
    {
        return $this->arrowId;
    }

    public function setArrowId(int $arrowId): self
    {
        $this->arrowId = $arrowId;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}
