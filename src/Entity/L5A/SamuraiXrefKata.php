<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="samurai_xref_kata")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\SamuraiXrefKataRepository")
 */
class SamuraiXrefKata
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $samuraiId;

    /**
     * @ORM\Column(type="integer")
     */
    private $kataId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSamuraiId(): ?int
    {
        return $this->samuraiId;
    }

    public function setSamuraiId(int $samuraiId): self
    {
        $this->samuraiId = $samuraiId;

        return $this;
    }

    public function getKataId(): ?int
    {
        return $this->kataId;
    }

    public function setKataId(int $kataId): self
    {
        $this->kataId = $kataId;

        return $this;
    }
}
