<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Equipement
 *
 * @ORM\Table(name="equipement")
 * @ORM\Entity
 */
class Equipement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=250, nullable=false)
     */
    private $nom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_armure", type="integer", nullable=true)
     */
    private $idArmure;

    /**
     * @var int|null
     *
     * @ORM\Column(name="samurai", type="integer", nullable=true)
     */
    private $samurai;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

	    return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getIdArmure(): ?int
    {
        return $this->idArmure;
    }

    public function setIdArmure(?int $idArmure): self
    {
        $this->idArmure = $idArmure;

        return $this;
    }

    public function getSamurai(): ?int
    {
        return $this->samurai;
    }

    public function setSamurai(?int $samurai): self
    {
        $this->samurai = $samurai;

        return $this;
    }


}
