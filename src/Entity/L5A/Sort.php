<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sort
 *
 * @ORM\Table(name="sort")
 * @ORM\Entity
 */
class Sort
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="anneau", type="string", length=255, nullable=false)
     */
    private $anneau;

    /**
     * @var int
     *
     * @ORM\Column(name="maitrise", type="integer", nullable=false)
     */
    private $maitrise;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mots_cles", type="string", length=255, nullable=true)
     */
    private $motsCles;

    /**
     * @var string|null
     *
     * @ORM\Column(name="portee", type="string", length=255, nullable=true)
     */
    private $portee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="zone_effet", type="string", length=255, nullable=true)
     */
    private $zoneEffet;

    /**
     * @var string|null
     *
     * @ORM\Column(name="duree", type="string", length=255, nullable=true)
     */
    private $duree;

    /**
     * @var string|null
     *
     * @ORM\Column(name="augmentations", type="text", length=65535, nullable=true)
     */
    private $augmentations;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="maho", type="boolean", nullable=true)
     */
    private $maho = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="personnalise", type="boolean", nullable=true)
     */
    private $personnalise = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="officiel", type="boolean", nullable=true, options={"default"="1"})
     */
    private $officiel = '1';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAnneau(): ?string
    {
        return $this->anneau;
    }

    public function setAnneau(string $anneau): self
    {
        $this->anneau = $anneau;

        return $this;
    }

    public function getMaitrise(): ?int
    {
        return $this->maitrise;
    }

    public function setMaitrise(int $maitrise): self
    {
        $this->maitrise = $maitrise;

        return $this;
    }

    public function getMotsCles(): ?string
    {
        return $this->motsCles;
    }

    public function setMotsCles(?string $motsCles): self
    {
        $this->motsCles = $motsCles;

        return $this;
    }

    public function getPortee(): ?string
    {
        return $this->portee;
    }

    public function setPortee(?string $portee): self
    {
        $this->portee = $portee;

        return $this;
    }

    public function getZoneEffet(): ?string
    {
        return $this->zoneEffet;
    }

    public function setZoneEffet(?string $zoneEffet): self
    {
        $this->zoneEffet = $zoneEffet;

        return $this;
    }

    public function getDuree(): ?string
    {
        return $this->duree;
    }

    public function setDuree(?string $duree): self
    {
        $this->duree = $duree;

        return $this;
    }

    public function getAugmentations(): ?string
    {
        return $this->augmentations;
    }

    public function setAugmentations(?string $augmentations): self
    {
        $this->augmentations = $augmentations;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getMaho(): ?bool
    {
        return $this->maho;
    }

    public function setMaho(?bool $maho): self
    {
        $this->maho = $maho;

        return $this;
    }

    public function getPersonnalise(): ?bool
    {
        return $this->personnalise;
    }

    public function setPersonnalise(?bool $personnalise): self
    {
        $this->personnalise = $personnalise;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }

    public function getOfficiel(): ?bool
    {
        return $this->officiel;
    }

    public function setOfficiel(?bool $officiel): self
    {
        $this->officiel = $officiel;

        return $this;
    }


}
