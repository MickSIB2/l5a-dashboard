<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personnage
 *
 * @ORM\Table(name="personnage")
 * @ORM\Entity
 */
class Personnage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rapport", type="string", length=50, nullable=true, options={"default"="Neutre"})
     */
    private $rapport = 'Neutre';

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="genere_gm", type="boolean", nullable=true)
     */
    private $genereGm = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="air", type="integer", nullable=true)
     */
    private $air;

    /**
     * @var int|null
     *
     * @ORM\Column(name="reflexes", type="integer", nullable=true)
     */
    private $reflexes;

    /**
     * @var int|null
     *
     * @ORM\Column(name="intuition", type="integer", nullable=true)
     */
    private $intuition;

    /**
     * @var int|null
     *
     * @ORM\Column(name="eau", type="integer", nullable=true)
     */
    private $eau;

    /**
     * @var int|null
     *
     * @ORM\Column(name="_force", type="integer", nullable=true)
     */
    private $force;

    /**
     * @var int|null
     *
     * @ORM\Column(name="perception", type="integer", nullable=true)
     */
    private $perception;

    /**
     * @var int|null
     *
     * @ORM\Column(name="terre", type="integer", nullable=true)
     */
    private $terre;

    /**
     * @var int|null
     *
     * @ORM\Column(name="constitution", type="integer", nullable=true)
     */
    private $constitution;

    /**
     * @var int|null
     *
     * @ORM\Column(name="volonte", type="integer", nullable=true)
     */
    private $volonte;

    /**
     * @var int|null
     *
     * @ORM\Column(name="feu", type="integer", nullable=true)
     */
    private $feu;

    /**
     * @var int|null
     *
     * @ORM\Column(name="agilite", type="integer", nullable=true)
     */
    private $agilite;

    /**
     * @var int|null
     *
     * @ORM\Column(name="intelligence", type="integer", nullable=true)
     */
    private $intelligence;

    /**
     * @var int|null
     *
     * @ORM\Column(name="vide", type="integer", nullable=true)
     */
    private $vide;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_1", type="integer", nullable=true)
     */
    private $rangBlessure1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_2", type="integer", nullable=true)
     */
    private $rangBlessure2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_3", type="integer", nullable=true)
     */
    private $rangBlessure3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_4", type="integer", nullable=true)
     */
    private $rangBlessure4;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_5", type="integer", nullable=true)
     */
    private $rangBlessure5;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_6", type="integer", nullable=true)
     */
    private $rangBlessure6;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_7", type="integer", nullable=true)
     */
    private $rangBlessure7;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_blessure_8", type="integer", nullable=true)
     */
    private $rangBlessure8;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nd", type="integer", nullable=true)
     */
    private $nd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="reduction", type="integer", nullable=true)
     */
    private $reduction;

    /**
     * @var string|null
     *
     * @ORM\Column(name="jet_attaque", type="string", length=5, nullable=true)
     */
    private $jetAttaque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="jet_degats", type="string", length=5, nullable=true)
     */
    private $jetDegats;

    /**
     * @var string|null
     *
     * @ORM\Column(name="jet_initiative", type="string", length=5, nullable=true)
     */
    private $jetInitiative;

    /**
     * @var string|null
     *
     * @ORM\Column(name="arme", type="string", length=250, nullable=true)
     */
    private $arme;

    /**
     * @var string|null
     *
     * @ORM\Column(name="armure", type="string", length=250, nullable=true)
     */
    private $armure;

    /**
     * @var string|null
     *
     * @ORM\Column(name="techniques_sorts", type="text", length=65535, nullable=true)
     */
    private $techniquesSorts;

    /**
     * @var string|null
     *
     * @ORM\Column(name="clan", type="string", length=250, nullable=true)
     */
    private $clan;

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    /**
     * @var float|null
     *
     * @ORM\Column(name="honneur", type="float", precision=10, scale=0, nullable=true)
     */
    private $honneur;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_maitrise", type="integer", nullable=true, options={"default"="1"})
     */
    private $rangMaitrise = '1';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id) : self {
    	$this->id = $id;
    	return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getRapport(): ?string
    {
        return $this->rapport;
    }

    public function setRapport(?string $rapport): self
    {
        $this->rapport = $rapport;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getGenereGm(): ?bool
    {
        return $this->genereGm;
    }

    public function setGenereGm(?bool $genereGm): self
    {
        $this->genereGm = $genereGm;

        return $this;
    }

    public function getAir(): ?int
    {
        return $this->air;
    }

    public function setAir(?int $air): self
    {
        $this->air = $air;

        return $this;
    }

    public function getReflexes(): ?int
    {
        return $this->reflexes;
    }

    public function setReflexes(?int $reflexes): self
    {
        $this->reflexes = $reflexes;

        return $this;
    }

    public function getIntuition(): ?int
    {
        return $this->intuition;
    }

    public function setIntuition(?int $intuition): self
    {
        $this->intuition = $intuition;

        return $this;
    }

    public function getEau(): ?int
    {
        return $this->eau;
    }

    public function setEau(?int $eau): self
    {
        $this->eau = $eau;

        return $this;
    }

    public function getForce(): ?int
    {
        return $this->force;
    }

    public function setForce(?int $force): self
    {
        $this->force = $force;

        return $this;
    }

    public function getPerception(): ?int
    {
        return $this->perception;
    }

    public function setPerception(?int $perception): self
    {
        $this->perception = $perception;

        return $this;
    }

    public function getTerre(): ?int
    {
        return $this->terre;
    }

    public function setTerre(?int $terre): self
    {
        $this->terre = $terre;

        return $this;
    }

    public function getConstitution(): ?int
    {
        return $this->constitution;
    }

    public function setConstitution(?int $constitution): self
    {
        $this->constitution = $constitution;

        return $this;
    }

    public function getVolonte(): ?int
    {
        return $this->volonte;
    }

    public function setVolonte(?int $volonte): self
    {
        $this->volonte = $volonte;

        return $this;
    }

    public function getFeu(): ?int
    {
        return $this->feu;
    }

    public function setFeu(?int $feu): self
    {
        $this->feu = $feu;

        return $this;
    }

    public function getAgilite(): ?int
    {
        return $this->agilite;
    }

    public function setAgilite(?int $agilite): self
    {
        $this->agilite = $agilite;

        return $this;
    }

    public function getIntelligence(): ?int
    {
        return $this->intelligence;
    }

    public function setIntelligence(?int $intelligence): self
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    public function getVide(): ?int
    {
        return $this->vide;
    }

    public function setVide(?int $vide): self
    {
        $this->vide = $vide;

        return $this;
    }

    public function getRangBlessure1(): ?int
    {
        return $this->rangBlessure1;
    }

    public function setRangBlessure1(?int $rangBlessure1): self
    {
        $this->rangBlessure1 = $rangBlessure1;

        return $this;
    }

    public function getRangBlessure2(): ?int
    {
        return $this->rangBlessure2;
    }

    public function setRangBlessure2(?int $rangBlessure2): self
    {
        $this->rangBlessure2 = $rangBlessure2;

        return $this;
    }

    public function getRangBlessure3(): ?int
    {
        return $this->rangBlessure3;
    }

    public function setRangBlessure3(?int $rangBlessure3): self
    {
        $this->rangBlessure3 = $rangBlessure3;

        return $this;
    }

    public function getRangBlessure4(): ?int
    {
        return $this->rangBlessure4;
    }

    public function setRangBlessure4(?int $rangBlessure4): self
    {
        $this->rangBlessure4 = $rangBlessure4;

        return $this;
    }

    public function getRangBlessure5(): ?int
    {
        return $this->rangBlessure5;
    }

    public function setRangBlessure5(?int $rangBlessure5): self
    {
        $this->rangBlessure5 = $rangBlessure5;

        return $this;
    }

    public function getRangBlessure6(): ?int
    {
        return $this->rangBlessure6;
    }

    public function setRangBlessure6(?int $rangBlessure6): self
    {
        $this->rangBlessure6 = $rangBlessure6;

        return $this;
    }

    public function getRangBlessure7(): ?int
    {
        return $this->rangBlessure7;
    }

    public function setRangBlessure7(?int $rangBlessure7): self
    {
        $this->rangBlessure7 = $rangBlessure7;

        return $this;
    }

    public function getRangBlessure8(): ?int
    {
        return $this->rangBlessure8;
    }

    public function setRangBlessure8(?int $rangBlessure8): self
    {
        $this->rangBlessure8 = $rangBlessure8;

        return $this;
    }

    public function getNd(): ?int
    {
        return $this->nd;
    }

    public function setNd(?int $nd): self
    {
        $this->nd = $nd;

        return $this;
    }

    public function getReduction(): ?int
    {
        return $this->reduction;
    }

    public function setReduction(?int $reduction): self
    {
        $this->reduction = $reduction;

        return $this;
    }

    public function getJetAttaque(): ?string
    {
        return $this->jetAttaque;
    }

    public function setJetAttaque(?string $jetAttaque): self
    {
        $this->jetAttaque = $jetAttaque;

        return $this;
    }

    public function getJetDegats(): ?string
    {
        return $this->jetDegats;
    }

    public function setJetDegats(?string $jetDegats): self
    {
        $this->jetDegats = $jetDegats;

        return $this;
    }

    public function getJetInitiative(): ?string
    {
        return $this->jetInitiative;
    }

    public function setJetInitiative(?string $jetInitiative): self
    {
        $this->jetInitiative = $jetInitiative;

        return $this;
    }

    public function getArme(): ?string
    {
        return $this->arme;
    }

    public function setArme(?string $arme): self
    {
        $this->arme = $arme;

        return $this;
    }

    public function getArmure(): ?string
    {
        return $this->armure;
    }

    public function setArmure(?string $armure): self
    {
        $this->armure = $armure;

        return $this;
    }

    public function getTechniquesSorts(): ?string
    {
        return $this->techniquesSorts;
    }

    public function setTechniquesSorts(?string $techniquesSorts): self
    {
        $this->techniquesSorts = $techniquesSorts;

        return $this;
    }

    public function getClan(): ?string
    {
        return $this->clan;
    }

    public function setClan(?string $clan): self
    {
        $this->clan = $clan;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }

    public function getHonneur(): ?float
    {
        return $this->honneur;
    }

    public function setHonneur(?float $honneur): self
    {
        $this->honneur = $honneur;

        return $this;
    }

    public function getRangMaitrise(): ?int
    {
        return $this->rangMaitrise;
    }

    public function setRangMaitrise(?int $rangMaitrise): self
    {
        $this->rangMaitrise = $rangMaitrise;

        return $this;
    }


}
