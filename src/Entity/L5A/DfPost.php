<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * DfPost
 *
 * @ORM\Table(name="df_post", indexes={@ORM\Index(name="IDX_FDB5B0441F55203D", columns={"topic_id"}), @ORM\Index(name="IDX_FDB5B0445BB66C05", columns={"poster_id"}), @ORM\Index(name="IDX_FDB5B04416FE72E1", columns={"updated_by"})})
 * @ORM\Entity
 */
class DfPost
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=false)
     */
    private $content;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \AppUser
     *
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     * })
     */
    private $updatedBy;

    /**
     * @var \DfTopic
     *
     * @ORM\ManyToOne(targetEntity="DfTopic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     * })
     */
    private $topic;

    /**
     * @var \AppUser
     *
     * @ORM\ManyToOne(targetEntity="AppUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="poster_id", referencedColumnName="id")
     * })
     */
    private $poster;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?AppUser
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?AppUser $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getTopic(): ?DfTopic
    {
        return $this->topic;
    }

    public function setTopic(?DfTopic $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    public function getPoster(): ?AppUser
    {
        return $this->poster;
    }

    public function setPoster(?AppUser $poster): self
    {
        $this->poster = $poster;

        return $this;
    }


}
