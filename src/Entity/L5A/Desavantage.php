<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Desavantage
 *
 * @ORM\Table(name="desavantage")
 * @ORM\Entity
 */
class Desavantage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="effet", type="string", length=50, nullable=true)
     */
    private $effet;

    /**
     * @var int
     *
     * @ORM\Column(name="valeur", type="integer", nullable=false)
     */
    private $valeur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sous_type", type="string", length=50, nullable=true)
     */
    private $sousType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valeur_speciale", type="string", length=50, nullable=true)
     */
    private $valeurSpeciale;

    /**
     * @var bool
     *
     * @ORM\Column(name="personnalise", type="boolean", nullable=false)
     */
    private $personnalise = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEffet(): ?string
    {
        return $this->effet;
    }

    public function setEffet(?string $effet): self
    {
        $this->effet = $effet;

        return $this;
    }

    public function getValeur(): ?int
    {
        return $this->valeur;
    }

    public function setValeur(int $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getSousType(): ?string
    {
        return $this->sousType;
    }

    public function setSousType(?string $sousType): self
    {
        $this->sousType = $sousType;

        return $this;
    }

    public function getValeurSpeciale(): ?string
    {
        return $this->valeurSpeciale;
    }

    public function setValeurSpeciale(?string $valeurSpeciale): self
    {
        $this->valeurSpeciale = $valeurSpeciale;

        return $this;
    }

    public function getPersonnalise(): ?bool
    {
        return $this->personnalise;
    }

    public function setPersonnalise(bool $personnalise): self
    {
        $this->personnalise = $personnalise;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }


}
