<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Arme
 *
 * @ORM\Table(name="arme")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\ArmeRepository")
 */
class Arme
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="_type", type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mots_cles", type="string", length=50, nullable=true)
     */
    private $motsCles;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vd", type="string", length=50, nullable=true)
     */
    private $vd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="_force", type="integer", nullable=true)
     */
    private $force;

    /**
     * @var float|null
     *
     * @ORM\Column(name="portee", type="float", precision=10, scale=0, nullable=true, options={"comment"="en mètres"})
     */
    private $portee;

    /**
     * @var float|null
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=true, options={"comment"="en bu"})
     */
    private $prix;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="text", length=0, nullable=true)
     */
    private $note;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="personnalisee", type="boolean", nullable=true)
     */
    private $personnalisee = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMotsCles(): ?string
    {
        return $this->motsCles;
    }

    public function setMotsCles(?string $motsCles): self
    {
        $this->motsCles = $motsCles;

        return $this;
    }

    public function getVd(): ?string
    {
        return $this->vd;
    }

    public function setVd(?string $vd): self
    {
        $this->vd = $vd;

        return $this;
    }

    public function getForce(): ?int
    {
        return $this->force;
    }

    public function setForce(?int $force): self
    {
        $this->force = $force;

        return $this;
    }

    public function getPortee(): ?float
    {
        return $this->portee;
    }

    public function setPortee(?float $portee): self
    {
        $this->portee = $portee;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPersonnalisee(): ?bool
    {
        return $this->personnalisee;
    }

    public function setPersonnalisee(?bool $personnalisee): self
    {
        $this->personnalisee = $personnalisee;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }


}
