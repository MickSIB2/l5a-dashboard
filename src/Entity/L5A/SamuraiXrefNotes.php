<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotesCampagne
 *
 * @ORM\Table(name="samurai_xref_notes")
 * @ORM\Entity
 */
class SamuraiXrefNotes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="personnages_rencontres", type="string", length=250, nullable=true)
     */
    private $personnagesRencontres;

    /**
     * @var string|null
     *
     * @ORM\Column(name="objectifs", type="text", length=0, nullable=true)
     */
    private $objectifs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="endroits_visites", type="string", length=250, nullable=true)
     */
    private $endroitsVisites;

    /**
     * @var integer
     *
     * @ORM\Column(name="samurai_id", type="integer")
     */
    private $samuraiId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersonnagesRencontres(): ?string
    {
        return $this->personnagesRencontres;
    }

    public function setPersonnagesRencontres(?string $personnagesRencontres): self
    {
        $this->personnagesRencontres = $personnagesRencontres;

        return $this;
    }

    public function getObjectifs(): ?string
    {
        return $this->objectifs;
    }

    public function setObjectifs(?string $objectifs): self
    {
        $this->objectifs = $objectifs;

        return $this;
    }

    public function getEndroitsVisites(): ?string
    {
        return $this->endroitsVisites;
    }

    public function setEndroitsVisites(?string $endroitsVisites): self
    {
        $this->endroitsVisites = $endroitsVisites;

        return $this;
    }

	/**
	 * @return int
	 */
	public function getSamuraiId(): int {
		return $this->samuraiId;
	}

	/**
	 * @param int $samuraiId
	 */
	public function setSamuraiId(int $samuraiId): void {
		$this->samuraiId = $samuraiId;
	}


}
