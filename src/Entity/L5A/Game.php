<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mot_de_passe", type="string", length=255, nullable=true)
     */
    private $motDePasse;

    /**
     * @var int|null
     *
     * @ORM\Column(name="gamemaster", type="integer", nullable=true)
     */
    private $gamemaster;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes_joueurs", type="text", length=65535, nullable=true)
     */
    private $notesJoueurs;

    /**
     * @var int|null
     *
     * @ORM\Column(name="provisions", type="integer", nullable=true, options={"default"="10"})
     */
    private $provisions = '10';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getMotDePasse(): ?string
    {
        return $this->motDePasse;
    }

    public function setMotDePasse(?string $motDePasse): self
    {
        $this->motDePasse = $motDePasse;

        return $this;
    }

    public function getGamemaster(): ?int
    {
        return $this->gamemaster;
    }

    public function setGamemaster(?int $gamemaster): self
    {
        $this->gamemaster = $gamemaster;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getNotesJoueurs(): ?string
    {
        return $this->notesJoueurs;
    }

    public function setNotesJoueurs(?string $notesJoueurs): self
    {
        $this->notesJoueurs = $notesJoueurs;

        return $this;
    }

    public function getProvisions(): ?int
    {
        return $this->provisions;
    }

    public function setProvisions(?int $provisions): self
    {
        $this->provisions = $provisions;

        return $this;
    }


}
