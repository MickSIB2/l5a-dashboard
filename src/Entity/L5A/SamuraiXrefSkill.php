<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\L5A\SamuraiXrefSkillRepository")
 * @ORM\Table(name="samurai_xref_skill")
 *
 */
class SamuraiXrefSkill
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id",type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="samurai_id", type="integer")
     */
    private $samuraiId;

    /**
     * @ORM\Column(name="skill_id", type="integer")
     */
    private $skillId;

    /**
     * @ORM\Column(name="rank", type="integer")
     */
    private $rank = 1;

    /**
     * @ORM\Column(name="specialities", type="string", length=255, nullable=true)
     */
    private $specialities;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSamuraiId(): ?int
    {
        return $this->samuraiId;
    }

    public function setSamuraiId(int $samuraiId): self
    {
        $this->samuraiId = $samuraiId;

        return $this;
    }

    public function getSkillId(): ?int
    {
        return $this->skillId;
    }

    public function setSkillId(int $skillId): self
    {
        $this->skillId = $skillId;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getSpecialities(): ?string
    {
        return $this->specialities;
    }

    public function setSpecialities(?string $specialities): self
    {
        $this->specialities = $specialities;

        return $this;
    }
}
