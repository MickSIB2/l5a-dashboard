<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * Armure
 *
 * @ORM\Table(name="armure")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\ArmureRepository")
 */
class Armure
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var int
     *
     * @ORM\Column(name="nd", type="integer", nullable=false)
     */
    private $nd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="reduction", type="integer", nullable=true)
     */
    private $reduction;

    /**
     * @var string|null
     *
     * @ORM\Column(name="regles_speciales", type="text", length=0, nullable=true)
     */
    private $reglesSpeciales;

    /**
     * @var int|null
     *
     * @ORM\Column(name="prix", type="integer", nullable=true, options={"comment"="En bu"})
     */
    private $prix;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="personnalisee", type="boolean", nullable=true)
     */
    private $personnalisee = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNd(): ?int
    {
        return $this->nd;
    }

    public function setNd(int $nd): self
    {
        $this->nd = $nd;

        return $this;
    }

    public function getReduction(): ?int
    {
        return $this->reduction;
    }

    public function setReduction(?int $reduction): self
    {
        $this->reduction = $reduction;

        return $this;
    }

    public function getReglesSpeciales(): ?string
    {
        return $this->reglesSpeciales;
    }

    public function setReglesSpeciales(?string $reglesSpeciales): self
    {
        $this->reglesSpeciales = $reglesSpeciales;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getPersonnalisee(): ?bool
    {
        return $this->personnalisee;
    }

    public function setPersonnalisee(?bool $personnalisee): self
    {
        $this->personnalisee = $personnalisee;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


}
