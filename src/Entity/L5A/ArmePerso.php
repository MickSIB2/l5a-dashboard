<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArmePerso
 *
 * @ORM\Table(name="arme_perso")
 * @ORM\Entity(repositoryClass="App\Repository\L5A\ArmePersoRepository")
 */
class ArmePerso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var int
     *
     * @ORM\Column(name="arme_id", type="integer", nullable=false)
     */
    private $armeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="filtre_attaque", type="string", length=50, nullable=true, options={"default"="0G0"})
     */
    private $filtreAttaque = '0G0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="filtre_degats", type="string", length=50, nullable=true, options={"default"="0G0"})
     */
    private $filtreDegats = '0G0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="portee", type="integer", nullable=true)
     */
    private $portee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="notes", type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @var int|null
     *
     * @ORM\Column(name="samurai", type="integer", nullable=true)
     */
    private $samurai;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getArmeId(): ?int
    {
        return $this->armeId;
    }

    public function setArmeId(int $armeId): self
    {
        $this->armeId = $armeId;

        return $this;
    }

    public function getFiltreAttaque(): ?string
    {
        return $this->filtreAttaque;
    }

    public function setFiltreAttaque(?string $filtreAttaque): self
    {
        $this->filtreAttaque = $filtreAttaque;

        return $this;
    }

    public function getFiltreDegats(): ?string
    {
        return $this->filtreDegats;
    }

    public function setFiltreDegats(?string $filtreDegats): self
    {
        $this->filtreDegats = $filtreDegats;

        return $this;
    }

    public function getPortee(): ?int
    {
        return $this->portee;
    }

    public function setPortee(?int $portee): self
    {
        $this->portee = $portee;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getSamurai(): ?int
    {
        return $this->samurai;
    }

    public function setSamurai(?int $samurai): self
    {
        $this->samurai = $samurai;

        return $this;
    }


}
