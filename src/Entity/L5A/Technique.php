<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;
use stdClass;

/**
 * Technique
 *
 * @ORM\Table(name="technique")
 * @ORM\Entity
 */
class Technique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="effet", type="string", length=50, nullable=true)
     */
    private $effet;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rang_minimum", type="integer", nullable=true, options={"default"="1"})
     */
    private $rangMinimum = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="type_personnage", type="string", length=50, nullable=true)
     */
    private $typePersonnage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ecoles", type="string", length=250, nullable=true)
     */
    private $ecoles;

    /**
     * @var string|null
     *
     * @ORM\Column(name="requiert", type="string", length=255, nullable=true)
     */
    private $requiert;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ecole_voie", type="string", length=255, nullable=true)
     */
    private $ecoleVoie;

    /**
     * @var bool
     *
     * @ORM\Column(name="personnalisee", type="boolean", nullable=false)
     */
    private $personnalisee = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true)
     */
    private $gameId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="officielle", type="boolean", nullable=true, options={"default"="1"})
     */
    private $officielle = '1';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEffet(): ?string
    {
        return $this->effet;
    }

    public function setEffet(?string $effet): self
    {
        $this->effet = $effet;

        return $this;
    }

    public function getRangMinimum(): ?int
    {
        return $this->rangMinimum;
    }

    public function setRangMinimum(?int $rangMinimum): self
    {
        $this->rangMinimum = $rangMinimum;

        return $this;
    }

    public function getTypePersonnage(): ?string
    {
        return $this->typePersonnage;
    }

    public function setTypePersonnage(?string $typePersonnage): self
    {
        $this->typePersonnage = $typePersonnage;

        return $this;
    }

    public function getEcoles(): ?array
    {
	    return json_decode($this->ecoles, true);
    }

    public function setEcoles(?array $ecole): self
    {
	    $jsonEcoles = empty($this->ecoles) ? json_encode (new stdClass) : $this->ecoles;
	    $aEcoles = json_decode($jsonEcoles, true);
	    // If ArrayCollection
	    if ($ecoles = $ecole->toArray()) {
	    	/** @var Ecole $oneEcole */
		    foreach ($ecoles as $oneEcole) {
			    array_push($aEcoles, $oneEcole->getId());
		    }
	    } else {
		    array_push($aEcoles, $ecole);
	    }
	    $this->ecoles = json_encode($aEcoles, JSON_FORCE_OBJECT);

	    return $this;
    }

    public function getRequiert(): ?string
    {
        return $this->requiert;
    }

    public function setRequiert(?string $requiert): self
    {
        $this->requiert = $requiert;

        return $this;
    }

    public function getEcoleVoie(): ?string
    {
        return $this->ecoleVoie;
    }

    public function setEcoleVoie(?string $ecoleVoie): self
    {
        $this->ecoleVoie = $ecoleVoie;

        return $this;
    }

    public function getPersonnalisee(): ?bool
    {
        return $this->personnalisee;
    }

    public function setPersonnalisee(bool $personnalisee): self
    {
        $this->personnalisee = $personnalisee;

        return $this;
    }

    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }

    public function getOfficielle(): ?bool
    {
        return $this->officielle;
    }

    public function setOfficielle(?bool $officielle): self
    {
        $this->officielle = $officielle;

        return $this;
    }


}
