<?php

namespace App\Entity\L5A;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;


/**
 * AppUser
 *
 * @ORM\Table(name="app_user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_957A647992FC23A8")})
 * @ORM\Entity
 */
class AppUser extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;


    /**
     * @var int|null
     *
     * @ORM\Column(name="game_id", type="integer", nullable=true, options={"default"="1"})
     */
    private $gameId = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="locale", type="string", length=2, nullable=true, options={"default"="fr"})
     */
    private $locale = 'fr';

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getGameId(): ?int
    {
        return $this->gameId;
    }

    public function setGameId(?int $gameId): self
    {
        $this->gameId = $gameId;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }


}
