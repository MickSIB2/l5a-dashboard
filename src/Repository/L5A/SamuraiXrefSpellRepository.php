<?php

namespace App\Repository\L5A;

use App\Entity\L5A\SamuraiXrefSpell;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SamuraiXrefSpell|null find($id, $lockMode = null, $lockVersion = null)
 * @method SamuraiXrefSpell|null findOneBy(array $criteria, array $orderBy = null)
 * @method SamuraiXrefSpell[]    findAll()
 * @method SamuraiXrefSpell[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SamuraiXrefSpellRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SamuraiXrefSpell::class);
    }

	public function getSamuraiSpells(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT s
FROM App\Entity\L5A\SamuraiXrefSpell sXs
LEFT JOIN App\Entity\L5A\Sort s WITH s.id = sXs.spellId
WHERE sXs.samuraiId = :samurai_id')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}

//    /**
//     * @return SamuraiXrefSpell[] Returns an array of SamuraiXrefSpell objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SamuraiXrefSpell
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
