<?php

namespace App\Repository\L5A;

use App\Entity\L5A\DesavantageXSamurai;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DesavantageXSamurai|null find($id, $lockMode = null, $lockVersion = null)
 * @method DesavantageXSamurai|null findOneBy(array $criteria, array $orderBy = null)
 * @method DesavantageXSamurai[]    findAll()
 * @method DesavantageXSamurai[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DesavantageXSamuraiRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, DesavantageXSamurai::class);
	}

	public function getSamuraiDisadvantages(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT d, dXs.id, dXs.resume, dXs.estCache
FROM App\Entity\L5A\DesavantageXSamurai dXs
LEFT JOIN App\Entity\L5A\Desavantage d WITH d.id = dXs.desavantageId
WHERE dXs.samuraiId = :samurai_id')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}

	//    /**
	//     * @return SamuraiXrefCompetences[] Returns an array of SamuraiXrefCompetences objects
	//     */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('s')
			->andWhere('s.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('s.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?SamuraiXrefCompetences
	{
		return $this->createQueryBuilder('s')
			->andWhere('s.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
