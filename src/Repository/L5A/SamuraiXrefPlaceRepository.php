<?php

namespace App\Repository\L5A;

use App\Entity\L5A\SamuraiXrefPlace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SamuraiXrefPlace|null find($id, $lockMode = null, $lockVersion = null)
 * @method SamuraiXrefPlace|null findOneBy(array $criteria, array $orderBy = null)
 * @method SamuraiXrefPlace[]    findAll()
 * @method SamuraiXrefPlace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SamuraiXrefPlaceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SamuraiXrefPlace::class);
    }

	public function getSamuraiPlaces(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT p
FROM App\Entity\L5A\SamuraiXrefPlace sXp
LEFT JOIN App\Entity\L5A\Lieu p WITH p.id = sXp.placeId
WHERE sXp.samuraiId = :samurai_id')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}
//    /**
//     * @return NotesXrefPlace[] Returns an array of NotesXrefPlace objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotesXrefPlace
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
