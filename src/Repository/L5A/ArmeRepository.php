<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 25/02/2017
 * Time: 15:19
 */

namespace App\Repository\L5A;

use Doctrine\ORM\EntityRepository;
use App\Entity\L5A\Arme;

class ArmeRepository extends EntityRepository {

	public function findAllBuyable() {
		return $this->getEntityManager()
			->createQuery(
				'SELECT a FROM L5ABundle:Arme a WHERE a.prix IS NOT NULL'
			)
			->getResult();
	}
	public function findByGameIdCustom($gameId) {
		$qb = $this->createQueryBuilder('a');
		$qb->select('a')
		   ->where(
			   $qb->expr()->orX(
				   $qb->expr()->eq('a.gameId', $gameId),
				   $qb->expr()->isNull('a.gameId')
			   )
		   );
		$data = $qb->getQuery()->getResult();
		return $data;
	}
}