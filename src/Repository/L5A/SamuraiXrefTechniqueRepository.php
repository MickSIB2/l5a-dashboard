<?php

namespace App\Repository\L5A;

use App\Entity\L5A\SamuraiXrefTechnique;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SamuraiXrefTechnique|null find($id, $lockMode = null, $lockVersion = null)
 * @method SamuraiXrefTechnique|null findOneBy(array $criteria, array $orderBy = null)
 * @method SamuraiXrefTechnique[]    findAll()
 * @method SamuraiXrefTechnique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SamuraiXrefTechniqueRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SamuraiXrefTechnique::class);
    }

	public function getTechniques(?EntityManager $em, ?int $samuraiId) : array {
		$query = $em
			->createQuery(
				'SELECT t
FROM App\Entity\L5A\SamuraiXrefTechnique sXt
LEFT JOIN App\Entity\L5A\Technique t WITH t.id = sXt.techniqueId
WHERE sXt.samuraiId = :samurai_id
ORDER BY sXt.rank')
			->setParameter(':samurai_id', $samuraiId)
		;
		return $query->getResult();
	}
//    /**
//     * @return SamuraiXrefTechnique[] Returns an array of SamuraiXrefTechnique objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SamuraiXrefTechnique
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
