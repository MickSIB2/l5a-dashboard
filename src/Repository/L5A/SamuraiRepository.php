<?php
/**
 * Created by PhpStorm.
 * User: Mickaël
 * Date: 25/02/2017
 * Time: 15:19
 */

namespace App\Repository\L5A;

use Doctrine\ORM\EntityRepository;
use App\Entity\L5A\Samurai;

class SamuraiRepository extends EntityRepository {

	public function findAllBuyable() {
		return $this->getEntityManager()
			->createQuery(
				'SELECT a FROM L5ABundle:Samurai a WHERE a.prix IS NOT NULL'
			)
			->getResult();
	}
}