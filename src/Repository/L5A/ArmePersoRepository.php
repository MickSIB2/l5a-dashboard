<?php

namespace App\Repository\L5A;

use App\Entity\L5A\ArmePerso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ArmePerso|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArmePerso|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArmePerso[]    findAll()
 * @method ArmePerso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArmePersoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ArmePerso::class);
    }

    public function getArmesPerso(?EntityManager $em, ?int $samuraiId) : array {
	    $query = $em
		    ->createQuery(
			    'SELECT a, aP.id, aP.nom, aP.filtreAttaque, aP.filtreDegats, aP.portee, aP.notes
FROM App\Entity\L5A\ArmePerso aP
LEFT JOIN App\Entity\L5A\Arme a WITH a.id = aP.armeId
WHERE aP.samurai = :samurai_id')
		    ->setParameter(':samurai_id', $samuraiId)
	    ;
	    return $query->getResult();
    }

//    /**
//     * @return SamuraiXrefCompetences[] Returns an array of SamuraiXrefCompetences objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SamuraiXrefCompetences
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
