<?php

namespace App\Repository\L5A;

use App\Entity\L5A\SamuraiXrefSkill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SamuraiXrefSkill|null find($id, $lockMode = null, $lockVersion = null)
 * @method SamuraiXrefSkill|null findOneBy(array $criteria, array $orderBy = null)
 * @method SamuraiXrefSkill[]    findAll()
 * @method SamuraiXrefSkill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SamuraiXrefSkillRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SamuraiXrefSkill::class);
    }

    public function getSamuraiSkills(?EntityManager $em, ?int $samuraiId) : array {
	    $query = $em
		    ->createQuery(
			    'SELECT s, sXs.rank rang, sXs.specialities specialites
FROM App\Entity\L5A\SamuraiXrefSkill sXs
LEFT JOIN App\Entity\L5A\Competence s WITH s.id = sXs.skillId
WHERE sXs.samuraiId = :samurai_id')
		    ->setParameter(':samurai_id', $samuraiId)
	    ;
	    return $query->getResult();
    }

//    /**
//     * @return SamuraiXrefCompetences[] Returns an array of SamuraiXrefCompetences objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SamuraiXrefCompetences
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
